/**
 * @creation 1998-12-23
 * @modification $Date: 2006-09-19 14:45:47 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.hiswa;

import org.fudaa.dodico.corba.hiswa.*;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.objet.IPersonne;

import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.ServeurPersonne;

/**
 * Classe client de Hiswa.
 * 
 * @version $Revision: 1.1 $ $Date: 2006-09-19 14:45:47 $ by $Author: deniger $
 * @author Axel Guerrand
 */
public final class HiswaClient {

  private HiswaClient() {

  }

  public static void main(final String[] _argv) {
    ICalculHiswa hs = null;
    // try {
    hs = ICalculHiswaHelper.narrow(CDodico.findServerByName("un-serveur-Hiswa"));
    // }
    // catch(Exception ex) { ex.printStackTrace(); }
    if (hs == null) {
      System.out.println("connexion au serveur echouee...");
      return;
    }
    System.out.println("connexion etablie...");
    System.out.println("Creation de la connexion");
    // IUsine usine=CDodico.creeUsineLocale();
    final IPersonne sp = ServeurPersonne.createPersonne("test-personne-hiswa", "test-organisme-hiswa");
    System.out.println("Connexion au serveur Hiswa : " + hs);
    final IConnexion c = hs.connexion(sp);
    final IParametresHiswa paramsH = IParametresHiswaHelper.narrow(hs.parametres(c));
    if (paramsH == null) {
      System.out.println(" paramsH null...");
    }
    final SParametresHiswaFichier fichierP = new SParametresHiswaFichier();
    final SParametresHiswaProject projectP = new SParametresHiswaProject();
    final SParametresHiswaTest testP = new SParametresHiswaTest();
    final SParametresHiswaResult resultP = new SParametresHiswaResult();
    final SParametresHiswaSet setP = new SParametresHiswaSet();
    final SParametresHiswaInputGrid inputgridP = new SParametresHiswaInputGrid();
    final SParametresHiswaRead readP = new SParametresHiswaRead();
    final SParametresHiswaGrid gridP = new SParametresHiswaGrid();
    final SParametresHiswaInc incP = new SParametresHiswaInc();
    final SParametresHiswaBoundary boundP = new SParametresHiswaBoundary();
    final SParametresHiswaWind windP = new SParametresHiswaWind();
    final SParametresHiswaBreaking breakP = new SParametresHiswaBreaking();
    final SParametresHiswaFriction fricP = new SParametresHiswaFriction();
    final SParametresHiswaBlk blkP = new SParametresHiswaBlk();
    final SParametresHiswaNumeric numP = new SParametresHiswaNumeric();
    final SParametresHiswaOff offP = new SParametresHiswaOff();
    final SParametresHiswaFrame frameP = new SParametresHiswaFrame();
    final SParametresHiswaCurve curveP = new SParametresHiswaCurve();
    final SParametresHiswaPoints pointsP = new SParametresHiswaPoints();
    final SParametresHiswaRay rayP = new SParametresHiswaRay();
    final SParametresHiswaIso isoP = new SParametresHiswaIso();
    final SParametresHiswaNgrid ngridP = new SParametresHiswaNgrid();
    final SParametresHiswaBlock blockP = new SParametresHiswaBlock();
    final SParametresHiswaTable tableP = new SParametresHiswaTable();
    final SParametresHiswaSpec specP = new SParametresHiswaSpec();
    final SParametresHiswaNest nestP = new SParametresHiswaNest();
    final SParametresHiswaShow showP = new SParametresHiswaShow();
    final SParametresHiswaPlot plotP = new SParametresHiswaPlot();
    final SParametresHiswaLine lineP = new SParametresHiswaLine();
    final SParametresHiswaPlace placeP = new SParametresHiswaPlace();
    final SParametresHIS paramsHIS = new SParametresHIS();
    fichierP.filename = "example";
    projectP.ecriture = true;
    projectP.name = "STANDARD TESTS FOR THE HISWA MODEL";
    projectP.nr = "RF1";
    projectP.title1 = "TEST: REFRACTION ON A PLANE BEACH";
    projectP.title2 = "HS = 1.0  DIR = 30.0 DEG  WIND: ABSENT";
    projectP.title3 = "T = 5.0 S  MS = 64                    ";
    testP.ecriture = false;
    testP.ix_present = false;
    testP.iy_present = false;
    resultP.ecriture = false;
    resultP.hres = "";
    setP.ecriture = true;
    setP.level = 0.;
    setP.cfae = 1.0;
    setP.depmin = 0.05;
    setP.negmes = 200;
    setP.interp = 1;
    setP.maxerr = 1;
    setP.grav = 9.81;
    setP.rho = 1025.;
    setP.inrhog = 1;
    setP.leakf = 1.;
    setP.umin = 1.;
    inputgridP.ecriture = true;
    inputgridP.grilles = new SParametresHiswaInputGridUnit[1];
    inputgridP.grilles[0] = new SParametresHiswaInputGridUnit();
    inputgridP.grilles[0].option = "BOTTOM";
    inputgridP.grilles[0].xp = 0.;
    inputgridP.grilles[0].yp = 0.;
    inputgridP.grilles[0].alp = 0.;
    inputgridP.grilles[0].mx = 3;
    inputgridP.grilles[0].my = 1;
    inputgridP.grilles[0].dx = 480.;
    inputgridP.grilles[0].dy = 4000.;
    readP.ecriture = true;
    readP.grilles = new SParametresHiswaReadUnit[1];
    readP.grilles[0] = new SParametresHiswaReadUnit();
    readP.grilles[0].option = "BOTTOM";
    readP.grilles[0].fname = "refbot";
    readP.grilles[0].fac = 1.0;
    readP.grilles[0].idla = 1;
    readP.grilles[0].nhed = 0;
    readP.grilles[0].optionfin = "";
    readP.grilles[0].format = false;
    readP.grilles[0].form = "";
    gridP.ecriture = true;
    gridP.xclen = 480.;
    gridP.yclen = 2000.;
    gridP.sector = 90.;
    gridP.mxc = 120;
    gridP.myc = 10;
    gridP.mdc = 15;
    gridP.option = "FIXED";
    gridP.xpc = 480.;
    gridP.ypc = 1000.;
    gridP.alpc = 0.;
    incP.ecriture = true;
    incP.option = "PARAM";
    incP.hsig = 1.0;
    incP.per = 5.0;
    incP.dir = 30.0;
    incP.ms = 64.;
    boundP.ecriture = false;
    boundP.option = "";
    boundP.option2 = "";
    boundP.bcf = "";
    windP.ecriture = false;
    breakP.ecriture = false;
    breakP.freq = false;
    fricP.ecriture = false;
    fricP.freq = false;
    blkP.ecriture = false;
    numP.ecriture = true;
    numP.cdn = 0.1;
    numP.cdd = 1.0;
    offP.ecriture = true;
    offP.option = new String[2];
    offP.option[0] = "BREA";
    offP.option[1] = "BLK";
    frameP.ecriture = false;
    frameP.sname = "";
    frameP.option = false;
    frameP.scale_present = false;
    curveP.ecriture = true;
    curveP.sname = "CT1C1";
    curveP.xp1 = 480.;
    curveP.yp1 = 2000.;
    curveP.nb_pts = 3;
    curveP.cur = new double[3][curveP.nb_pts];
    curveP.cur[0][0] = 12;
    curveP.cur[0][1] = 30;
    curveP.cur[0][2] = 6;
    curveP.cur[1][0] = 720.;
    curveP.cur[1][1] = 840.;
    curveP.cur[1][2] = 960.;
    curveP.cur[2][0] = 2000.;
    curveP.cur[2][1] = 2000.;
    curveP.cur[2][2] = 2000.;
    pointsP.ecriture = false;
    pointsP.sname = "";
    pointsP.fname = "";
    pointsP.coords = new double[0][0];
    rayP.ecriture = false;
    rayP.rname = "";
    rayP.coords = new double[0][0];
    isoP.ecriture = false;
    isoP.sname = "";
    isoP.rname = "";
    isoP.dep = new double[0];
    ngridP.ecriture = false;
    ngridP.sname = "";
    blockP.ecriture = false;
    blockP.sname = "";
    blockP.fname = "";
    blockP.options = new String[0];
    blockP.unit_present = false;
    blockP.units = new double[0];
    tableP.ecriture = true;
    tableP.sname = "CT1C1";
    tableP.format = "PAPER";
    tableP.fname = "resu_example.dat";
    tableP.options = new String[4];
    tableP.options[0] = "HS";
    tableP.options[1] = "DIR";
    tableP.options[2] = "DEP";
    tableP.options[3] = "VEL";
    // tableP.options[4] = "FOR";
    // tableP.options[5] = "TRA";
    // tableP.options[6] = "DSPR";
    // tableP.options[7] = "XP";
    specP.ecriture = false;
    specP.sname = "";
    specP.fname = "";
    nestP.ecriture = false;
    nestP.sname = "";
    nestP.fname = "";
    showP.ecriture = false;
    showP.sname = "";
    showP.fname = "";
    showP.title_present = false;
    showP.title = "";
    showP.option = "";
    showP.min_max_present = false;
    showP.min_max = new double[0];
    showP.scale_present = false;
    plotP.ecriture = false;
    plotP.sname = "";
    plotP.fname = "";
    plotP.title_present = false;
    plotP.title = "";
    plotP.iso_op = "";
    plotP.min_max_present = false;
    plotP.min_max = new double[0];
    plotP.vec_op = "";
    plotP.scale_present = false;
    plotP.dist_present = false;
    plotP.star_op = "";
    plotP.prob_op = "";
    lineP.ecriture = false;
    lineP.coords = new double[0][0];
    placeP.ecriture = false;
    placeP.pname = new String[0];
    placeP.xp = new double[0];
    placeP.yp = new double[0];
    placeP.size = new double[0];
    placeP.sit = new int[0];
    // on affecte a notre IParametreHiswa tous les parametres initiaux
    paramsHIS.parametresFichier = fichierP;
    paramsHIS.parametresProject = projectP;
    paramsHIS.parametresTest = testP;
    paramsHIS.parametresResult = resultP;
    paramsHIS.parametresSet = setP;
    paramsHIS.parametresInputGrid = inputgridP;
    paramsHIS.parametresRead = readP;
    paramsHIS.parametresGrid = gridP;
    paramsHIS.parametresInc = incP;
    paramsHIS.parametresBoundary = boundP;
    paramsHIS.parametresWind = windP;
    paramsHIS.parametresBreaking = breakP;
    paramsHIS.parametresFriction = fricP;
    paramsHIS.parametresBlk = blkP;
    paramsHIS.parametresNumeric = numP;
    paramsHIS.parametresOff = offP;
    paramsHIS.parametresFrame = frameP;
    paramsHIS.parametresCurve = curveP;
    paramsHIS.parametresPoints = pointsP;
    paramsHIS.parametresRay = rayP;
    paramsHIS.parametresIso = isoP;
    paramsHIS.parametresNgrid = ngridP;
    paramsHIS.parametresBlock = blockP;
    paramsHIS.parametresTable = tableP;
    paramsHIS.parametresSpec = specP;
    paramsHIS.parametresNest = nestP;
    paramsHIS.parametresShow = showP;
    paramsHIS.parametresPlot = plotP;
    paramsHIS.parametresLine = lineP;
    paramsHIS.parametresPlace = placeP;
    if (paramsH != null) {
      paramsH.parametresHIS(paramsHIS);
    }
    hs.calcul(c);
    final IResultatsHiswa resultsH = IResultatsHiswaHelper.narrow(hs.resultats(c));
    if (resultsH == null) {
      System.out.println("resultsH : null ");
    } else {
      System.out.println("OK, reference recuperee ");
    }
    if (resultsH == null) {
      return;
    }
    final SResultatsHiswaTable res = resultsH.resultatsHiswaTable();
    for (int i = 0; i < res.lignes.length; i++) {
      System.out.println("Hsign de la ligne " + i + " : " + res.lignes[i].hsign);
      System.out.println("Depth de la ligne " + i + " : " + res.lignes[i].depth);
      // System.out.println("Force de la ligne "+i+" : "+res.lignes[i].force);
    }
  }
}