/*
 * @file HiswaTestParser.java @creation 1999-11-24 @modification $Date: 2006-09-19 14:45:46 $
 * @license GNU General Public License 2 @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231
 * Compiegne @mail devel@fudaa.org
 */
package org.fudaa.dodico.hiswa;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.fudaa.dodico.corba.hiswa.ICalculHiswa;
import org.fudaa.dodico.corba.hiswa.IParametresHiswa;
import org.fudaa.dodico.corba.hiswa.IParametresHiswaHelper;
import org.fudaa.dodico.corba.hiswa.IResultatsHiswa;
import org.fudaa.dodico.corba.hiswa.IResultatsHiswaHelper;
import org.fudaa.dodico.corba.hiswa.SParametresHIS;
import org.fudaa.dodico.corba.hiswa.SParametresHiswaTable;
import org.fudaa.dodico.corba.hiswa.SResultatsHiswaTable;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.objet.IPersonne;

import org.fudaa.dodico.hiswa.DParametresHiswa;
import org.fudaa.dodico.hiswa.HiswaWriter;
import org.fudaa.dodico.hiswa.parser.ParseException;
import org.fudaa.dodico.objet.ServeurPersonne;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Programme-test du parser Hiswa.
 * @version $Revision: 1.1 $ $Date: 2006-09-19 14:45:46 $ by $Author: deniger $
 * @author Claudio Toni BRANCO
 */
public final class HiswaTestParser {
  private HiswaTestParser(){
      
  }

  public static void main(final String[] _args){
    UsineLib.setAllLocal(true);
    ICalculHiswa hs = null;
    hs = UsineLib.findUsine().creeHiswaCalculHiswa();
    System.out.println("Creation de la connexion");
    //    IUsine usine=CDodico.creeUsineLocale();
    final IPersonne sp = ServeurPersonne.createPersonne("test-personne-hiswa", "test-organisme-hiswa");
    System.out.println("Connexion au serveur Hiswa : " + hs);
    final IConnexion c = hs.connexion(sp);
    File fich;
    final IParametresHiswa paramsH = IParametresHiswaHelper.narrow(hs.parametres(c));
    SParametresHIS paramsHIS = null;
    if (_args.length != 1) {
      System.out.println("");
      System.out.print("Erreur : Il faut un seul et un argument : ");
      System.out.println(" Le nom du fichier a parser");
      return;
    }
    fich = new File(_args[0]);
    try {
      paramsHIS = DParametresHiswa.litParametresHIS(fich);
    }
    catch (final ParseException e) {
      System.err.println("Erreur de syntaxe :");
      e.printStackTrace();
    }
    catch (final IOException e) {
      System.err.println("Erreur : IOException  :");
      e.printStackTrace();
    }
    paramsH.parametresHIS(paramsHIS);
    hs.calcul(c);
    final IResultatsHiswa resultsH = IResultatsHiswaHelper.narrow(hs.resultats(c));
    //pour le calcul en local
    if (resultsH == null) {
      System.out.println("resultsH : null ");
    } else {
      final SResultatsHiswaTable res = resultsH.resultatsHiswaTable();
      final SParametresHiswaTable tab = paramsH.parametresHIS().parametresTable;
      boolean hsign = false;
      boolean dir = false;
      boolean per = false;
      boolean dept = false;
      boolean vel = false;
      boolean force = false;
      boolean transp = false;
      boolean dirspr = false;
      boolean dissip = false;
      boolean leak = false;
      boolean qb = false;
      boolean xp = false;
      boolean yp = false;
      boolean distan = false;
      boolean ubottom = false;
      boolean steepn = false;
      boolean wlength = false;
      for (int i = 0; i < tab.options.length; i++) {
        if (tab.options[i].equals("HS")) {
          hsign = true;
        }
        if (tab.options[i].equals("DIR")) {
          dir = true;
        }
        if (tab.options[i].equals("PER")) {
          per = true;
        }
        if (tab.options[i].equals("DEP")) {
          dept = true;
        }
        if (tab.options[i].equals("VEL")) {
          vel = true;
        }
        if (tab.options[i].equals("FOR")) {
          force = true;
        }
        if (tab.options[i].equals("TRA")) {
          transp = true;
        }
        if (tab.options[i].equals("DSPR")) {
          dirspr = true;
        }
        if (tab.options[i].equals("DISS")) {
          dissip = true;
        }
        if (tab.options[i].equals("LEA")) {
          leak = true;
        }
        if (tab.options[i].equals("QB")) {
          qb = true;
        }
        if (tab.options[i].equals("XP")) {
          xp = true;
        }
        if (tab.options[i].equals("YP")) {
          yp = true;
        }
        if (tab.options[i].equals("DIST")) {
          distan = true;
        }
        if (tab.options[i].equals("UBOT")) {
          ubottom = true;
        }
        if (tab.options[i].equals("STEEP")) {
          steepn = true;
        }
        if (tab.options[i].equals("WLEN")) {
          wlength = true;
        }
      }
      try {
        final HiswaWriter fic = new HiswaWriter(new FileWriter("HiswaResults.dat"));
        fic
            .keywordField(0,
                "##########       Fichier R�sultat pour tester la fonction litResultatsHiswa     ##########");
        fic.writeFields();
        fic.writeFields();
        for (int i = 0; i < tab.options.length; i++) {
          fic.keywordField(i, tab.options[i]);
        }
        fic.writeFields();
        for (int j = 0; j < res.lignes.length; j++) {
          int k = 0;
          while (k < (tab.options.length)) {
            if (hsign) {
              fic.doubleField(k++, res.lignes[j].hsign);
            }
            if (dir) {
              fic.doubleField(k++, res.lignes[j].dir);
            }
            if (per) {
              fic.doubleField(k++, res.lignes[j].period);
            }
            if (dept) {
              fic.doubleField(k++, res.lignes[j].depth);
            }
            if (vel) {
              fic.doubleField(k++, res.lignes[j].velX);
              fic.doubleField(k++, res.lignes[j].velY);
            }
            if (force) {
              fic.doubleField(k++, res.lignes[j].forceX);
              fic.doubleField(k++, res.lignes[j].forceY);
            }
            if (transp) {
              fic.doubleField(k++, res.lignes[j].transpX);
              fic.doubleField(k++, res.lignes[j].transpY);
            }
            if (dirspr) {
              fic.doubleField(k++, res.lignes[j].dspr);
            }
            if (dissip) {
              fic.doubleField(k++, res.lignes[j].dissip);
            }
            if (leak) {
              fic.doubleField(k++, res.lignes[j].leak);
            }
            if (qb) {
              fic.doubleField(k++, res.lignes[j].qb);
            }
            if (xp) {
              fic.doubleField(k++, res.lignes[j].xp);
            }
            if (yp) {
              fic.doubleField(k++, res.lignes[j].yp);
            }
            if (distan) {
              fic.doubleField(k++, res.lignes[j].dist);
            }
            if (ubottom) {
              fic.doubleField(k++, res.lignes[j].ubot);
            }
            if (steepn) {
              fic.doubleField(k++, res.lignes[j].steepn);
            }
            if (wlength) {
              fic.doubleField(k++, res.lignes[j].wlength);
            }
          }
          fic.writeFields();
        }
        fic.close();
      }
      catch (final Exception e) {
        e.printStackTrace();
      }
    }
  }
}