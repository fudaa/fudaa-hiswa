/*
 * @creation 1998-12-06
 * @modification $Date: 2006-09-19 14:43:23 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.dodico.hiswa;

import java.io.File;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.hiswa.ICalculHiswa;
import org.fudaa.dodico.corba.hiswa.ICalculHiswaOperations;
import org.fudaa.dodico.corba.hiswa.IParametresHiswa;
import org.fudaa.dodico.corba.hiswa.IParametresHiswaHelper;
import org.fudaa.dodico.corba.hiswa.IResultatsHiswa;
import org.fudaa.dodico.corba.hiswa.IResultatsHiswaHelper;
import org.fudaa.dodico.corba.hiswa.SParametresHIS;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.objet.ITransfertFichierASCII;

import org.fudaa.dodico.calcul.DCalcul;
import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.CExec;
import org.fudaa.dodico.objet.UsineLib;

/**
 * Classe Calcul de Hiswa.
 *
 * @version $Revision: 1.14 $ $Date: 2006-09-19 14:43:23 $ by $Author: deniger $
 * @author Axel Guerrand , Claudio Toni Branco
 */
public class DCalculHiswa extends DCalcul implements ICalculHiswa, ICalculHiswaOperations {

  private String strOperation_;

  public DCalculHiswa() {
    super();
    strOperation_ = null;
    setFichiersExtensions(new String[] { ".60", ".cal", ".lig", ".clm", ".pro", ".rzo", ".sng", ".ern", ".rsn" });
  }

  public String toString() {
    return "DCalculHiswa()";
  }

  public String description() {
    return "Hiswa, serveur de calcul pour la houle: " + super.description();
  }

  public ITransfertFichierASCII copieFichiers() {
    final ITransfertFichierASCII flux = UsineLib.findUsine().creeObjetTransfertFichierASCII();
    final String path = cheminServeur();
    flux.setCheminServeur(path);
    return flux;
  }

  // retourne l'operation en cours et un pourcentage d'achevement
  // "operation":%%
  public String getMsgOperationEnCours() {
    return strOperation_;
  }

  public void calcul(final IConnexion _c) {
    if (!verifieConnexion(_c)) {
      return;
    }
    final IParametresHiswa params = IParametresHiswaHelper.narrow(parametres(_c));
    if (params == null){
      CDodico.exceptionAxel(this, new Exception("params non definis (null)"));
     return;
    }
    final IResultatsHiswa results = IResultatsHiswaHelper.narrow(resultats(_c));
    if (results == null) {
      CDodico.exceptionAxel(this, new Exception("results non definis (null)"));
    }
    log(_c, "lancement du calcul");
    final String os = System.getProperty("os.name");
    final String path = cheminServeur();
    try {
      final SParametresHIS paramsHIS = params.parametresHIS();
      String basefic = paramsHIS.parametresFichier.filename;
      try {
        basefic = (basefic).substring(0, basefic.lastIndexOf('.'));
      } catch (final StringIndexOutOfBoundsException ex) {} // filename ne contient pas de "."
      if (basefic.length() > 15) {
        basefic = basefic.substring(0, 15);
      }
      final int etude = _c.numero();
      // On affecte un nouveau nom de fichier r�sultat
      paramsHIS.parametresTable.fname = basefic + etude + "ResuTable.dat";
      paramsHIS.parametresTable.fname_present = true;
      // On affecte un nouveau nom de fichier
      paramsHIS.parametresFichier.filename = basefic + etude;
      // Ecriture
      strOperation_ = "Ecriture des fichiers param�tres:00";
      DParametresHiswa.ecritParametresHIS(new File(path + paramsHIS.parametresFichier.filename + ".his"), paramsHIS);
      System.out.println("Appel de l'executable lido");
      String[] cmd;
      if (os.startsWith("Windows")) {
        cmd = new String[4];
        cmd[0] = path + "hiswa.bat";
        if (path.indexOf(':') != -1) {
          // lettre de l'unite (ex: "C:")
          cmd[1] = path.substring(0, path.indexOf(':') + 1);
          // chemin du serveur
          cmd[2] = path.substring(path.indexOf(':') + 1);
          cmd[3] = paramsHIS.parametresFichier.filename;
        } else {
          // si pas de lettre dans le chemin
          cmd[1] = "fake_cmd";
          cmd[2] = path;
          cmd[3] = paramsHIS.parametresFichier.filename;
        }
        System.out.println(cmd[0] + CtuluLibString.ESPACE + cmd[1] + CtuluLibString.ESPACE + cmd[2] + CtuluLibString.ESPACE + cmd[3]);
      } else {
        cmd = new String[3];
        cmd[0] = path + "hiswa.sh";
        cmd[1] = path;
        cmd[2] = "" + paramsHIS.parametresFichier.filename;
        System.out.println(cmd[0] + CtuluLibString.ESPACE + cmd[1] + CtuluLibString.ESPACE + cmd[2]);
      }
      final CExec ex = new CExec();
      ex.setCommand(cmd);
      ex.setOutStream(System.out);
      ex.setErrStream(System.err);
      strOperation_ = "Lancement du Calcul:50";
      ex.exec();
      strOperation_ = "Fin du Calcul:98";
      System.out.println("Fin du calcul");
      if(results!=null) {
        results.nomFichier(path + basefic + etude);
      }
      log(_c, "calcul termin�");
    } catch (final Exception ex) {
      log(_c, "erreur du calcul");
      CDodico.exceptionAxel(this, ex);
    }
  }
}