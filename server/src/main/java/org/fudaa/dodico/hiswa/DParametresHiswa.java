/*
 * @creation 1998-10-26
 * @modification $Date: 2006-09-19 14:43:23 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.dodico.hiswa;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;

import org.fudaa.dodico.calcul.DParametres;
import org.fudaa.dodico.corba.hiswa.*;
import org.fudaa.dodico.hiswa.parser.HiswaParser;
import org.fudaa.dodico.hiswa.parser.ParseException;

/**
 * Les parametres Hiswa.
 *
 * @version $Revision: 1.10 $ $Date: 2006-09-19 14:43:23 $ by $Author: deniger $
 * @author Axel Guerrand , Claudio Branco
 */
public class DParametresHiswa extends DParametres implements IParametresHiswa, IParametresHiswaOperations {

  private  String fichier_;
  private SParametresHIS paramsHIS_;

  public DParametresHiswa() {
    super();
  }

  public final Object clone() throws CloneNotSupportedException {
    return new DParametresHiswa();
  }

  public String toString() {
    return "DParametresHiswa(" + fichier_ + ")";
  }

  // Ecriture de l'entete du fichier
  private static void ecritParametresHiswaFichier(final HiswaWriter _fhis, final SParametresHiswaFichier _params)
      throws IOException {
    _fhis
        .keywordField(0, "$************************** Fichier " + _params.filename + ".his **************************");
    _fhis.writeFields();
    _fhis.writeFields();
  }

  // Parametres pour la commande Project
  private static void ecritParametresHiswaProject(final HiswaWriter _fhis, final SParametresHiswaProject _params)
      throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "PROJECT");
      _fhis.stringField(1, _params.name);
      _fhis.stringField(2, _params.nr);
      _fhis.writeFields();
      _fhis.stringField(1, _params.title1);
      _fhis.writeFields();
      _fhis.stringField(1, _params.title2);
      _fhis.writeFields();
      _fhis.stringField(1, _params.title3);
      _fhis.writeFields();
      _fhis.writeFields();
      _fhis.keywordField(0, getRien());
      _fhis.writeFields();
      _fhis.keywordField(0, "$************************** INPUT DATA **************************");
      _fhis.writeFields();
      _fhis.keywordField(0, getRien());
      _fhis.writeFields();
      _fhis.writeFields();
    }
  }

  private static String getRien() {
    return "$";
  }

  // Parametres pour la commande Test
  private static void ecritParametresHiswaTest(final HiswaWriter _fhis, final SParametresHiswaTest _params) throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "TEST");
      _fhis.intField(1, _params.itest);
      _fhis.intField(2, _params.itrace);
      if (_params.ix_present) {
        _fhis.intField(3, _params.ix);
      }
      if (_params.iy_present) {
        _fhis.intField(4, _params.iy);
      }
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande Result
  private static void ecritParametresHiswaResult(final HiswaWriter _fhis, final SParametresHiswaResult _params) throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "RESULT");
      _fhis.stringField(1, _params.hres);
      _fhis.intField(2, _params.ix1);
      _fhis.intField(3, _params.ix2);
      _fhis.intField(4, _params.iy1);
      _fhis.intField(5, _params.iy2);
      _fhis.intField(6, _params.istx);
      _fhis.intField(7, _params.isty);
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande Set
  private static void ecritParametresHiswaSet(final HiswaWriter _fhis, final SParametresHiswaSet _params) throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "SET");
      _fhis.floatField(1, _params.level);
      _fhis.floatField(2, _params.cfae);
      _fhis.floatField(3, _params.depmin);
      _fhis.intField(4, _params.negmes);
      _fhis.intField(5, _params.interp);
      _fhis.intField(6, _params.maxerr);
      _fhis.floatField(7, _params.grav);
      _fhis.floatField(8, _params.rho);
      _fhis.intField(9, _params.inrhog);
      _fhis.floatField(10, _params.leakf);
      _fhis.floatField(11, _params.umin);
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande InputGrid
  private static void ecritParametresHiswaInputGrid(final HiswaWriter _fhis, final SParametresHiswaInputGrid _params)
      throws IOException {
    if (_params.ecriture) {
      for (int i = 0; i < _params.grilles.length; i++) {
        _fhis.keywordField(0, "INPUT GRID");
        _fhis.keywordField(1, _params.grilles[i].option);
        _fhis.floatField(2, _params.grilles[i].xp);
        _fhis.floatField(3, _params.grilles[i].yp);
        _fhis.floatField(4, _params.grilles[i].alp);
        _fhis.intField(5, _params.grilles[i].mx);
        _fhis.intField(6, _params.grilles[i].my);
        _fhis.floatField(7, _params.grilles[i].dx);
        _fhis.floatField(8, _params.grilles[i].dy);
        _fhis.writeFields();
      }
    }
  }

  // Parametres pour la commande Read
  private static void ecritParametresHiswaRead(final HiswaWriter _fhis, final SParametresHiswaRead _params) throws IOException {
    if (_params.ecriture) {
      for (int i = 0; i < _params.grilles.length; i++) {
        _fhis.keywordField(0, "READ");
        _fhis.keywordField(1, _params.grilles[i].option);
        _fhis.stringField(2, _params.grilles[i].fname);
        _fhis.floatField(3, _params.grilles[i].fac);
        _fhis.intField(4, _params.grilles[i].idla);
        _fhis.intField(5, _params.grilles[i].nhed);
        _fhis.keywordField(6, _params.grilles[i].optionfin);
        if (_params.grilles[i].optionfin.startsWith("FOR")) {
          if (_params.grilles[i].format) {
            _fhis.stringField(7, _params.grilles[i].form);
          } else {
            _fhis.intField(7, _params.grilles[i].idfm);
          }
        }
        _fhis.writeFields();
      }
    }
  }

  // Parametres pour la commande Grid
  private static void ecritParametresHiswaGrid(final HiswaWriter _fhis, final SParametresHiswaGrid _params) throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "GRID");
      _fhis.floatField(1, _params.xclen);
      _fhis.floatField(2, _params.yclen);
      _fhis.floatField(3, _params.sector);
      _fhis.intField(4, _params.mxc);
      _fhis.intField(5, _params.myc);
      _fhis.intField(6, _params.mdc);
      _fhis.keywordField(7, _params.option);
      if (_params.option.startsWith("F")) {
        _fhis.floatField(8, _params.xpc);
        _fhis.floatField(9, _params.ypc);
        _fhis.floatField(10, _params.alpc);
      } else {
        _fhis.floatField(8, _params.xpr);
        _fhis.floatField(9, _params.ypr);
        _fhis.floatField(10, _params.xcr);
        _fhis.floatField(11, _params.ycr);
      }
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande Inc
  private static void ecritParametresHiswaInc(final HiswaWriter _fhis, final SParametresHiswaInc _params) throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "INC");
      _fhis.keywordField(1, _params.option);
      if (_params.option.startsWith("PAR")) {
        _fhis.floatField(2, _params.hsig);
        _fhis.floatField(3, _params.per);
        _fhis.floatField(4, _params.dir);
        _fhis.floatField(5, _params.ms);
      }
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande Boundary
  private static void ecritParametresHiswaBoundary(final HiswaWriter _fhis, final SParametresHiswaBoundary _params)
      throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "BOUNDARY");
      _fhis.keywordField(1, _params.option);
      if (_params.option.startsWith("NE")) {
        _fhis.stringField(2, _params.bcf);
      } else {
        _fhis.keywordField(2, _params.option2);
        if (_params.option2.startsWith("REFL")) {
          _fhis.floatField(3, _params.coef);
        }
        if (_params.option2.equals(getStrFile())) {
          _fhis.stringField(3, _params.bcf);
        }
      }
      _fhis.writeFields();
    }
  }

  private static String getStrFile() {
    return "FILE";
  }

  // Parametres pour la commande Wind
  private static void ecritParametresHiswaWind(final HiswaWriter _fhis, final SParametresHiswaWind _params) throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "WIND");
      _fhis.floatField(1, _params.vel);
      _fhis.floatField(2, _params.dir);
      _fhis.floatField(3, _params.a);
      _fhis.floatField(4, _params.b);
      _fhis.floatField(5, _params.p);
      _fhis.floatField(6, _params.q);
      _fhis.floatField(7, _params.tmax);
      _fhis.intField(8, _params.ms);
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande Breaking
  private static void ecritParametresHiswaBreaking(final HiswaWriter _fhis, final SParametresHiswaBreaking _params)
      throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "BREAKING");
      _fhis.floatField(1, _params.gams);
      _fhis.floatField(2, _params.gamd);
      _fhis.floatField(3, _params.alpha);
      if (_params.freq) {
        _fhis.keywordField(4, "FREQ");
        _fhis.floatField(5, _params.bf);
      } else {
        _fhis.floatField(4, _params.bf);
      }
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande Friction
  private static void ecritParametresHiswaFriction(final HiswaWriter _fhis, final SParametresHiswaFriction _params)
      throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "FRICTION");
      _fhis.floatField(1, _params.cfw);
      _fhis.floatField(2, _params.cfc);
      if (_params.freq) {
        _fhis.keywordField(3, "FREQ");
        _fhis.floatField(4, _params.bf);
      } else {
        _fhis.floatField(3, _params.bf);
      }
      // (claudio) : est ce que dans ce cas(freq=false), on utilise bf ???
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande Blk
  private static void ecritParametresHiswaBlk(final HiswaWriter _fhis, final SParametresHiswaBlk _params) throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "BLK");
      _fhis.floatField(1, _params.power);
      _fhis.floatField(2, _params.pca);
      _fhis.floatField(3, _params.dis);
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande Numeric
  private static void ecritParametresHiswaNumeric(final HiswaWriter _fhis, final SParametresHiswaNumeric _params)
      throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "NUMERIC");
      _fhis.floatField(1, _params.cdn);
      _fhis.floatField(2, _params.cdd);
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande Off
  private static void ecritParametresHiswaOff(final HiswaWriter _fhis, final SParametresHiswaOff _params) throws IOException {
    if (_params.ecriture) {
      for (int i = 0; i < _params.option.length; i++) {
        if (!(_params.option[i]).equals("")) {
          _fhis.keywordField(0, "OFF");
          _fhis.keywordField(1, _params.option[i]);
          _fhis.writeFields();
        }
      }
    }
  }

  // Parametres pour la commande Frame
  private static void ecritParametresHiswaFrame(final HiswaWriter _fhis, final SParametresHiswaFrame _params) throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "FRAME");
      _fhis.stringField(1, _params.sname);
      if (_params.option) {
        _fhis.keywordField(2, "SUBGRID");
        _fhis.intField(3, _params.ix1);
        _fhis.intField(4, _params.ix2);
        _fhis.intField(5, _params.iy1);
        _fhis.intField(6, _params.iy2);
      } else {
        _fhis.floatField(2, _params.xflen);
        _fhis.floatField(3, _params.yflen);
        _fhis.floatField(4, _params.xpf);
        _fhis.floatField(5, _params.ypf);
        _fhis.floatField(6, _params.alpf);
        _fhis.intField(7, _params.mxf);
        _fhis.intField(8, _params.myf);
        if (_params.scale_present) {
          _fhis.floatField(9, _params.scale);
        }
      }
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande Curve
  private static void ecritParametresHiswaCurve(final HiswaWriter _fhis, final SParametresHiswaCurve _params) throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "CURVE");
      _fhis.stringField(1, _params.sname);
      _fhis.floatField(2, _params.xp1);
      _fhis.floatField(3, _params.yp1);
      for (int i = 0; i < _params.nb_pts; i++) {
        _fhis.intField(4 + 3 * i, (int) _params.cur[0][i]);
        _fhis.floatField(5 + 3 * i, _params.cur[1][i]);
        _fhis.floatField(6 + 3 * i, _params.cur[2][i]);
      }
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande Points
  private static void ecritParametresHiswaPoints(final HiswaWriter _fhis, final SParametresHiswaPoints _params) throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "POINTS");
      _fhis.stringField(1, _params.sname);
      if (_params.file) {
        _fhis.keywordField(2, getStrFile());
        _fhis.stringField(3, _params.fname);
      } else {
        for (int i = 0; i < _params.coords[0].length; i++) {
          _fhis.floatField(2 + 2 * i, _params.coords[0][i]);
          _fhis.floatField(3 + 2 * i, _params.coords[1][i]);
        }
      }
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande Ray
  private static void ecritParametresHiswaRay(final HiswaWriter _fhis, final SParametresHiswaRay _params) throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "RAY");
      _fhis.stringField(1, _params.rname);
      _fhis.floatField(2, _params.xp1);
      _fhis.floatField(3, _params.yp1);
      _fhis.floatField(4, _params.xq1);
      _fhis.floatField(5, _params.yq1);
      for (int i = 0; i < _params.coords[0].length; i++) {
        _fhis.intField(6 + 5 * i, (int) _params.coords[0][i]);
        _fhis.floatField(7 + 5 * i, _params.coords[1][i]);
        _fhis.floatField(8 + 5 * i, _params.coords[2][i]);
        _fhis.floatField(9 + 5 * i, _params.coords[3][i]);
        _fhis.floatField(10 + 5 * i, _params.coords[4][i]);
      }
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande Iso
  private static void ecritParametresHiswaIso(final HiswaWriter _fhis, final SParametresHiswaIso _params) throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "ISOLINE");
      _fhis.stringField(1, _params.sname);
      _fhis.stringField(2, _params.rname);
      if (_params.depth) {
        _fhis.keywordField(3, "DEPTH");
      } else {
        _fhis.keywordField(3, "BOTTOM");
      }
      for (int i = 0; i < _params.dep.length; i++) {
        _fhis.floatField(4 + i, _params.dep[i]);
      }
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande Ngrid
  private static void ecritParametresHiswaNgrid(final HiswaWriter _fhis, final SParametresHiswaNgrid _params) throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "NGRID");
      _fhis.stringField(1, _params.sname);
      if (_params.option) {
        _fhis.keywordField(2, "SUBGRID");
        _fhis.intField(3, _params.ix1);
        _fhis.intField(4, _params.ix2);
        _fhis.intField(5, _params.iy1);
        _fhis.intField(6, _params.iy2);
      } else {
        _fhis.floatField(2, _params.xnlen);
        _fhis.floatField(3, _params.ynlen);
        _fhis.floatField(4, _params.sector);
        _fhis.intField(5, _params.mxn);
        _fhis.intField(6, _params.myn);
        _fhis.intField(7, _params.mdn);
        _fhis.floatField(8, _params.xpn);
        _fhis.floatField(9, _params.ypn);
        _fhis.floatField(10, _params.alpn);
      }
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande Block
  private static void ecritParametresHiswaBlock(final HiswaWriter _fhis, final SParametresHiswaBlock _params) throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "BLOCK");
      _fhis.stringField(1, _params.sname);
      if (_params.file) {
        _fhis.keywordField(2, getStrFile());
        _fhis.stringField(3, _params.fname);
        if (_params.lay_out) {
          _fhis.intField(4, _params.idla);
        }
      } else {
        _fhis.keywordField(2, "PAPER");
      }
      for (int i = 0; i < _params.options.length; i++) {
        _fhis.keywordField(5 + 2 * i, _params.options[i]);
        if (_params.unit_present) {
          _fhis.floatField(6 + 2 * i, _params.units[i]);
        }
      }
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande Table
  private static void ecritParametresHiswaTable(final HiswaWriter _fhis, final SParametresHiswaTable _params) throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "TABLE");
      _fhis.stringField(1, _params.sname);
      _fhis.keywordField(2, _params.format);
      if (_params.fname_present) {
        _fhis.stringField(3, _params.fname);
      }
      for (int i = 0; i < _params.options.length; i++) {
        _fhis.keywordField(4 + i, _params.options[i]);
      }
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande Spec
  private static void ecritParametresHiswaSpec(final HiswaWriter _fhis, final SParametresHiswaSpec _params) throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "SPEC");
      _fhis.stringField(1, _params.sname);
      _fhis.stringField(2, _params.fname);
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande Nest
  private static void ecritParametresHiswaNest(final HiswaWriter _fhis, final SParametresHiswaNest _params) throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "NEST");
      _fhis.stringField(1, _params.sname);
      _fhis.stringField(2, _params.fname);
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande Show
  private static void ecritParametresHiswaShow(final HiswaWriter _fhis, final SParametresHiswaShow _params) throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "SHOW");
      _fhis.stringField(1, _params.sname);
      if (_params.file) {
        _fhis.keywordField(2, getStrFile());
        _fhis.stringField(3, _params.fname);
      }
      if (_params.title_present) {
        _fhis.stringField(4, _params.title);
      }
      _fhis.keywordField(5, _params.option);
      if (_params.location) {
        _fhis.floatField(6, _params.symsiz);
      }
      if (_params.min_max_present) {
        for (int i = 0; i < _params.min_max.length; i++) {
          _fhis.floatField(6 + i, _params.min_max[i]);
        }
      }
      if (_params.scale_present) {
        _fhis.floatField(6, _params.scale);
      }
      if (_params.places) {
        _fhis.keywordField(9, "PLACES");
      }
      if (_params.lines) {
        _fhis.keywordField(10, "LINES");
      }
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande Plot
  private static void ecritParametresHiswaPlot(final HiswaWriter _fhis, final SParametresHiswaPlot _params) throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "PLOT");
      _fhis.stringField(1, _params.sname);
      if (_params.file) {
        _fhis.keywordField(2, getStrFile());
        _fhis.stringField(3, _params.fname);
      }
      if (_params.title_present) {
        _fhis.stringField(4, _params.title);
      }
      if (_params.iso) {
        _fhis.keywordField(5, "ISO");
        _fhis.keywordField(6, _params.iso_op);
        if (_params.min_max_present) {
          for (int i = 0; i < _params.min_max.length; i++) {
            _fhis.floatField(7 + i, _params.min_max[i]);
          }
        }
      }
      if (_params.vec) {
        _fhis.keywordField(10, "VEC");
        _fhis.keywordField(11, _params.vec_op);
        if (_params.scale_present) {
          _fhis.floatField(12, _params.scale);
        }
        if (_params.dist_present) {
          _fhis.intField(13, _params.dist);
        }
      }
      if (_params.star) {
        _fhis.keywordField(10, "STAR");
        _fhis.keywordField(11, _params.star_op);
        if (_params.scale_present) {
          _fhis.floatField(12, _params.scale);
        }
        if (_params.dist_present) {
          _fhis.intField(13, _params.dist);
        }
      }
      if (_params.problem) {
        _fhis.keywordField(5, "PROBLEM");
        _fhis.keywordField(6, _params.prob_op);
        _fhis.floatField(7, _params.symsiz);
      }
      if (_params.places) {
        _fhis.keywordField(14, "PLACES");
      }
      if (_params.lines) {
        _fhis.keywordField(15, "LINES");
      }
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande Line
  private static void ecritParametresHiswaLine(final HiswaWriter _fhis, final SParametresHiswaLine _params) throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "LINE");
      for (int i = 0; i < _params.coords[0].length; i++) {
        _fhis.floatField(1 + 2 * i, _params.coords[0][i]);
        _fhis.floatField(2 + 2 * i, _params.coords[1][i]);
      }
      _fhis.writeFields();
    }
  }

  // Parametres pour la commande Place
  private static void ecritParametresHiswaPlace(final HiswaWriter _fhis, final SParametresHiswaPlace _params) throws IOException {
    if (_params.ecriture) {
      _fhis.keywordField(0, "PLACE");
      for (int i = 0; i < _params.pname.length; i++) {
        _fhis.stringField(1 + 5 * i, _params.pname[i]);
        _fhis.floatField(2 + 5 * i, _params.xp[i]);
        _fhis.floatField(3 + 5 * i, _params.yp[i]);
        _fhis.floatField(4 + 5 * i, _params.size[i]);
        _fhis.intField(5 + 5 * i, _params.sit[i]);
        _fhis.writeFields();
      }
    }
  }

  public SParametresHIS parametresHIS() {
    return paramsHIS_;
  }

  public void parametresHIS(final SParametresHIS _params) {
    paramsHIS_ = _params;
  }

  public static void ecritParametresHIS(final File _fichier, final SParametresHIS _params) throws IOException {
    ecritParametresHIS(new FileOutputStream(_fichier), _params);
  }

  public static void ecritParametresHIS(final OutputStream _fichier, final SParametresHIS _params) throws IOException {
    if (!(_params == null)) {
      // System.out.println("Ecriture du fichier d'entree.") ;
      final HiswaWriter fhis = new HiswaWriter(new OutputStreamWriter(_fichier));
      ecritParametresHiswaFichier(fhis, _params.parametresFichier);
      ecritParametresHiswaProject(fhis, _params.parametresProject);
      ecritParametresHiswaTest(fhis, _params.parametresTest);
      ecritParametresHiswaResult(fhis, _params.parametresResult);
      ecritParametresHiswaSet(fhis, _params.parametresSet);
      ecritParametresHiswaInputGrid(fhis, _params.parametresInputGrid);
      ecritParametresHiswaRead(fhis, _params.parametresRead);
      ecritParametresHiswaGrid(fhis, _params.parametresGrid);
      ecritParametresHiswaInc(fhis, _params.parametresInc);
      ecritParametresHiswaBoundary(fhis, _params.parametresBoundary);
      ecritParametresHiswaWind(fhis, _params.parametresWind);
      ecritParametresHiswaBreaking(fhis, _params.parametresBreaking);
      ecritParametresHiswaFriction(fhis, _params.parametresFriction);
      ecritParametresHiswaBlk(fhis, _params.parametresBlk);
      ecritParametresHiswaNumeric(fhis, _params.parametresNumeric);
      ecritParametresHiswaOff(fhis, _params.parametresOff);
      fhis.writeFields();
      fhis.writeFields();
      fhis.keywordField(0, getRien());
      fhis.writeFields();
      fhis.keywordField(0, "$************************** OUTPUT REQUESTS **************************");
      fhis.writeFields();
      fhis.keywordField(0, getRien());
      fhis.writeFields();
      fhis.writeFields();
      ecritParametresHiswaFrame(fhis, _params.parametresFrame);
      ecritParametresHiswaCurve(fhis, _params.parametresCurve);
      ecritParametresHiswaPoints(fhis, _params.parametresPoints);
      ecritParametresHiswaRay(fhis, _params.parametresRay);
      ecritParametresHiswaIso(fhis, _params.parametresIso);
      ecritParametresHiswaNgrid(fhis, _params.parametresNgrid);
      ecritParametresHiswaBlock(fhis, _params.parametresBlock);
      ecritParametresHiswaTable(fhis, _params.parametresTable);
      ecritParametresHiswaSpec(fhis, _params.parametresSpec);
      ecritParametresHiswaNest(fhis, _params.parametresNest);
      ecritParametresHiswaShow(fhis, _params.parametresShow);
      ecritParametresHiswaPlot(fhis, _params.parametresPlot);
      ecritParametresHiswaLine(fhis, _params.parametresLine);
      ecritParametresHiswaPlace(fhis, _params.parametresPlace);
      fhis.writeFields();
      fhis.writeFields();
      fhis.writeFields();
      fhis.keywordField(0, "STOP");
      fhis.writeFields();
      fhis.close();
    }
  }

  public static SParametresHIS litParametresHIS(final File _fic) throws IOException, ParseException {
    SParametresHIS paramsH;
    paramsH = litParametresHIS(new FileReader(_fic));
    paramsH.parametresFichier.filename = (_fic.getName()).substring(0, (_fic.getName()).lastIndexOf('.'));
    return (paramsH);
  }

  public static SParametresHIS litParametresHIS(final Reader _fic) throws IOException, ParseException {
    final SParametresHIS paramsH = new SParametresHIS();
    HiswaParser parser;
    parser = new HiswaParser(_fic);
    parser.parametresHiswa(paramsH);
    parser.init();
    parser.hiswaParse();
    return (paramsH);
  }
}