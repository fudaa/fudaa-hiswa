/*
 * @file HiswaWriter.java @creation 1998-10-20 @modification $Date: 2007-04-16 16:34:22 $ @license
 * GNU General Public License 2 @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne @mail
 * devel@fudaa.org
 */
package org.fudaa.dodico.hiswa;

import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Iterator;

import org.fudaa.ctulu.CtuluLib;

import org.fudaa.dodico.commun.DodicoArrayList;
/**
 * Une classe inspir�e (tres largement) de FortranWriter qu permet d'ecrire dans un fichier avec des
 * colonnes libres. A noter la difference faite entre les chaines de caracteres avec ou sans 'quote'
 * @version $Revision: 1.9 $ $Date: 2007-04-16 16:34:22 $ by $Author: deniger $
 * @author Guillaume Desnoix , Bertrand Marchand, Axel Guerrand
 */
public class HiswaWriter extends PrintWriter {

  protected DodicoArrayList fields;

  public HiswaWriter() {
    this(new OutputStreamWriter(System.out));
  }

  public HiswaWriter(final Writer _writer) {
    super(_writer);
    fields = new DodicoArrayList();
  }

  /**
   * Ecrit une ligne en format libre contenant les champs definis.
   */
  public void writeFields() throws java.io.IOException{
    for (final Iterator e = fields.iterator(); e.hasNext();) {
      final Object o = e.next();
      if (o != null) {
        String s = "";
        if (o instanceof String) {
          s = o.toString();
          if (s.indexOf('\'') >= 0) {
            for (int i = s.length() - 1; i >= 0; i--) {
              if (s.charAt(i) == '\'') {
                s = s.substring(0, i) + "''" + s.substring(i + 1);
              }
            }
          }
          if (s.charAt(0) != '-') {
            s = "'" + s + "'";
          } else {
            s = s.substring(1, s.length());
          }
        } else {
          s = o.toString();
        }
        write(s + " ");
      }
    }
    write('\n');
    fields.clear();
  }

  /**
   * Fixe la valeur du champ numero i avec un reel.
   * @param _i le numero du champ
   * @param _v la valeur du champ de type double
   */
  public void doubleField(final int _i,final double _v){
    if (fields.size() <= _i) {
      fields.setSize(_i + 1);
    }
    fields.set(_i,CtuluLib.getDouble(_v));
  }

  /**
   * Fixe la valeur du champ numero i avec un reel.
   * @param _i le numero du champ
   * @param _v la valeur du champ de type float
   */
  public void floatField(final int _i,final double _v){
    if (fields.size() <= _i) {
      fields.setSize(_i + 1);
    }
    fields.set(_i,new Float(_v));
  }

  /**
   * Fixe la valeur du champ numero i avec un entier.
   * @param _i le numero du champ
   * @param _v la valeur du champ de type int
   */
  public void intField(final int _i,final int _v){
    if (fields.size() <= _i) {
      fields.setSize(_i + 1);
    }
    fields.set(_i,new Integer(_v));
  }

  /**
   * Fixe la valeur du champ numero i avec une chaine de caracteres (entour�e de 'quote').
   * @param _i le numero du champ
   * @param _v la valeur du champ de type string
   */
  public void stringField(final int _i,final String _v){
    if (fields.size() <= _i) {
      fields.setSize(_i + 1);
    }
    fields.set(_i,_v);
  }

  /**
   * Fixe la valeur du champ numero i avec une chaine de caracteres (sans 'quote').
   * @param _i le numero du champ
   * @param _v la valeur du champ de type string
   */
  public void keywordField(final int _i,final String _v){
    if (fields.size() <= _i) {
      fields.setSize(_i + 1);
    }
    fields.set(_i,"-" + _v );
  }
}