/*
 * @file         HiswaParserConstants.java
 * @creation     
 * @modification $Date: 2003-11-25 10:11:38 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
/* Generated By:JavaCC: Do not edit this line. HiswaParserConstants.java */
package org.fudaa.dodico.hiswa.parser;
/**
 * @version      $Revision: 1.4 $ $Date: 2003-11-25 10:11:38 $ by $Author: deniger $
 * @author        
 */
public interface HiswaParserConstants {
  int EOF= 0;
  int SINGLE_LINE_COMMENT= 7;
  int TEST= 9;
  int STOP= 10;
  int SET= 11;
  int GRID= 12;
  int VX= 13;
  int VY= 14;
  int WX= 15;
  int WY= 16;
  int READ= 17;
  int INC= 18;
  int VAR= 19;
  int RAD= 20;
  int FILE= 21;
  int DAM= 22;
  int BLK= 23;
  int OFF= 24;
  int REF= 25;
  int TRY= 26;
  int RAY= 27;
  int DIR= 28;
  int DSPR= 29;
  int QB= 30;
  int UBOT= 31;
  int XP= 32;
  int YP= 33;
  int DIST= 34;
  int VEC= 35;
  int TDI= 36;
  int STAR= 37;
  int CY= 38;
  int CONT= 39;
  int FREE= 40;
  int CFAE= 41;
  int ESSAI= 42;
  int NOMBRE= 43;
  int ENTIER= 44;
  int REEL= 45;
  int EXPONENT= 46;
  int STRING= 47;
  int PROJECT= 48;
  int RESULT= 49;
  int INPUT= 50;
  int BOTTOM= 51;
  int CUR= 52;
  int WLEVEL= 53;
  int VEGET= 54;
  int FOR= 55;
  int UNFORMATTED= 56;
  int PARAM= 57;
  int SPECTR= 58;
  int DEGR= 59;
  int UNIFORM= 60;
  int BOUNDARY= 61;
  int NEST= 62;
  int RIGHT= 63;
  int REFLECTING= 64;
  int EXTENDED= 65;
  int OBSTACLE= 66;
  int SCREEN= 67;
  int LINE= 68;
  int BREAKING= 69;
  int NUMERIC= 70;
  int SUBGRID= 71;
  int NGRID= 72;
  int POINTS= 73;
  int ISOLINE= 74;
  int DEPTH= 75;
  int BLOCK= 76;
  int PAPER= 77;
  int LAY_OUT= 78;
  int HSIGN= 79;
  int PERIOD= 80;
  int TRANSP= 81;
  int DISSIP= 82;
  int LEAK= 83;
  int STEEPN= 84;
  int WLENGTH= 85;
  int INDEXED= 86;
  int SHOW= 87;
  int LOCATIONS= 88;
  int VELOCITY= 89;
  int PLACES= 90;
  int PLOT= 91;
  int ACTION= 92;
  int PROBLEM= 93;
  int WIND= 94;
  int LEFT= 95;
  int FREQUENCY= 96;
  int FRAME= 97;
  int FRICTION_INPUT= 98;
  int TABLE= 99;
  int FIXED= 100;
  int ROTATING= 101;
  int ID= 102;
  int LETTER= 103;
  int DIGIT= 104;
  int DEFAULT= 0;
  int IN_SINGLE_LINE_COMMENT= 1;
  String[] tokenImage=
    {
      "<EOF>",
      "\" \"",
      "\"\\t\"",
      "\"\\n\"",
      "\"\\r\"",
      "\"\\f\"",
      "\"$\"",
      "<SINGLE_LINE_COMMENT>",
      "<token of kind 8>",
      "\"TEST\"",
      "\"STOP\"",
      "\"SET\"",
      "\"GRID\"",
      "\"VX\"",
      "\"VY\"",
      "\"WX\"",
      "\"WY\"",
      "\"READ\"",
      "\"INC\"",
      "\"VAR\"",
      "\"RAD\"",
      "\"FILE\"",
      "\"DAM\"",
      "\"BLK\"",
      "\"OFF\"",
      "\"REF\"",
      "\"TRY\"",
      "\"RAY\"",
      "\"DIR\"",
      "\"DSPR\"",
      "\"QB\"",
      "\"UBOT\"",
      "\"XP\"",
      "\"YP\"",
      "\"DIST\"",
      "\"VEC\"",
      "\"TDI\"",
      "\"STAR\"",
      "\"CY\"",
      "\"CONT\"",
      "\"FREE\"",
      "\"CFAE\"",
      "\"ESSAI\"",
      "<NOMBRE>",
      "<ENTIER>",
      "<REEL>",
      "<EXPONENT>",
      "<STRING>",
      "<PROJECT>",
      "<RESULT>",
      "<INPUT>",
      "<BOTTOM>",
      "<CUR>",
      "<WLEVEL>",
      "<VEGET>",
      "<FOR>",
      "<UNFORMATTED>",
      "<PARAM>",
      "<SPECTR>",
      "<DEGR>",
      "<UNIFORM>",
      "<BOUNDARY>",
      "<NEST>",
      "<RIGHT>",
      "<REFLECTING>",
      "<EXTENDED>",
      "<OBSTACLE>",
      "<SCREEN>",
      "<LINE>",
      "<BREAKING>",
      "<NUMERIC>",
      "<SUBGRID>",
      "<NGRID>",
      "<POINTS>",
      "<ISOLINE>",
      "<DEPTH>",
      "<BLOCK>",
      "<PAPER>",
      "<LAY_OUT>",
      "<HSIGN>",
      "<PERIOD>",
      "<TRANSP>",
      "<DISSIP>",
      "<LEAK>",
      "<STEEPN>",
      "<WLENGTH>",
      "<INDEXED>",
      "<SHOW>",
      "<LOCATIONS>",
      "<VELOCITY>",
      "<PLACES>",
      "<PLOT>",
      "<ACTION>",
      "<PROBLEM>",
      "<WIND>",
      "<LEFT>",
      "<FREQUENCY>",
      "<FRAME>",
      "<FRICTION_INPUT>",
      "<TABLE>",
      "<FIXED>",
      "<ROTATING>",
      "<ID>",
      "<LETTER>",
      "<DIGIT>",
      "\",\"",
      "\"=\"",
      };
}
