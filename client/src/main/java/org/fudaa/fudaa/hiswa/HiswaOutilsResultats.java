/*
 * @file         HiswaOutilsResultats.java
 * @creation     1999-12-16
 * @modification $Date: 2006-09-19 15:02:04 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.hiswa;
import java.util.Enumeration;

import com.memoire.yapod.YapodAccessQuery;
import com.memoire.yapod.YapodQuery;
import com.memoire.yapod.YapodStaticArrayQuery;

import org.fudaa.dodico.corba.hiswa.SParametresHIS;
import org.fudaa.dodico.corba.hiswa.SResultatsHiswaTable;

import org.fudaa.ebli.calque.BCalqueVecteur;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrVecteur;
/**
 * Simple classe pour recuperer , sous forme de double[][]
 * directement exploitable par une BCalqueGrilleReguliere, des champs specifiques
 * d'un resultatsHiswa.
 *
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:02:04 $ by $Author: deniger $
 * @author       Claudio Toni Branco
 */
public class HiswaOutilsResultats {
  // base de toutes les requetes Yapod
  private YapodQuery baseResultats_;
  // nb de resultats contenus dans chaque parametre resultat (hs, vel...)
  private int tailleResultats_;
  // parametres de la grille de calcul
  private int nbMailleX_, nbMailleY_;
  private double largeur_, hauteur_;
  private double origX_, origY_;
  private double pasX_;
  private double pasY_;
  public HiswaOutilsResultats() {
    baseResultats_= null;
    tailleResultats_= 0;
  }
  public YapodQuery getBaseResultats() {
    return baseResultats_;
  }
  //construit la base pour les requetes
  //et recupere les parametres de la grille
  public void setBaseResultats(
    final SResultatsHiswaTable _resultatsHIS,
    final SParametresHIS _paramsHIS) {
    if (_resultatsHIS != null) {
      baseResultats_= new YapodStaticArrayQuery(_resultatsHIS.lignes);
      tailleResultats_= _resultatsHIS.lignes.length;
      if (_paramsHIS.parametresFrame.option) {
        //System.out.println("FRAME AVEC SUBGRID");
        nbMailleX_=
          (_paramsHIS.parametresFrame.ix2 - _paramsHIS.parametresFrame.ix1);
        nbMailleY_=
          (_paramsHIS.parametresFrame.iy2 - _paramsHIS.parametresFrame.iy1);
        if (_paramsHIS.parametresGrid.option.startsWith("F")) {
          //System.out.println("GRID AVEC FIXED");
          origX_= _paramsHIS.parametresGrid.xpc;
          origY_= _paramsHIS.parametresGrid.ypc;
          largeur_= _paramsHIS.parametresGrid.xclen;
          hauteur_= _paramsHIS.parametresGrid.yclen;
        }
      } else {
        //System.out.println("FRAME SANS SUBGRID");
        nbMailleX_= _paramsHIS.parametresFrame.mxf;
        nbMailleY_= _paramsHIS.parametresFrame.myf;
        largeur_= _paramsHIS.parametresFrame.xflen;
        hauteur_= _paramsHIS.parametresFrame.yflen;
        origX_= _paramsHIS.parametresFrame.xpf;
        origY_= _paramsHIS.parametresFrame.ypf;
      }
      pasX_= (largeur_) / nbMailleX_;
      pasY_= (hauteur_) / nbMailleY_;
      System.out.println("Nombre de Mailles en  X = " + nbMailleX_);
      System.out.println("Nombre de Mailles en  Y = " + nbMailleY_);
      System.out.println("Pas en X = " + pasX_);
      System.out.println("Pas en Y = " + pasY_);
      System.out.println("Largeur de la grille = " + largeur_);
      System.out.println("Hauteur de la grille = " + hauteur_);
      System.out.println("Abscisse de l'Origine = " + origX_);
      System.out.println("Ordonn�e de l'Origine = " + origY_);
    }
  }
  //construit et renvoie un rectangle adapte a  la grille de calcul
  public GrPolygone getRectangle() {
    final GrPolygone rect= new GrPolygone();
    rect.sommets_.ajoute(new GrPoint(origX_, origY_, 0.));
    rect.sommets_.ajoute(new GrPoint(largeur_ + origX_, origY_, 0.));
    rect.sommets_.ajoute(new GrPoint(largeur_ + origX_, hauteur_ + origY_, 0.));
    rect.sommets_.ajoute(new GrPoint(origX_, hauteur_ + origY_, 0.));
    return rect;
  }
  public GrPoint getOrigine() {
    return (new GrPoint(origX_, origY_, 0.));
  }
  public GrPoint getExtremite() {
    return (new GrPoint(largeur_, hauteur_, 0.));
  }
  public boolean hasToChangeRepere() {
    return ((origX_ != 0.) || (origY_ != 0.));
  }
  public double getPasX() {
    return pasX_;
  }
  public double getPasY() {
    return pasY_;
  }
  public double getPasOptimal() {
    double pasOptimal;
    pasOptimal= Math.max(largeur_, hauteur_);
    pasOptimal= Math.log(pasOptimal) / Math.log(10.);
    pasOptimal= Math.floor(pasOptimal) - 1;
    pasOptimal= Math.pow(10., pasOptimal);
    System.err.println("Pas Optimal = " + pasOptimal);
    return pasOptimal;
  }
  public int getNbMailleX() {
    return nbMailleX_;
  }
  public int getNbMailleY() {
    return nbMailleY_;
  }
  public int getMaxDimension() {
    return (int)Math.max(largeur_, hauteur_);
  }
  //recupere, a partir d'une requete sur un resultat, les valeurs correspondantes
  // sous forme d'un double[] et retourne , par le double[][] associe qui sera
  // directement exploitable par une BCalqueGrilleReguliere.
  public double[][] queryToDouble(final YapodQuery _query) {
    final double[] res= new double[tailleResultats_];
    int indice= 0;
    final Enumeration e= _query.getResult();
    try {
      while (e.hasMoreElements()) {
        res[indice++]= ((Double) (e.nextElement())).doubleValue();
      }
    } catch (final ArrayIndexOutOfBoundsException exc) {
      exc.printStackTrace();
    }
    return (construitPoints(res));
  }
  //construit, a partir d'un double[] le double[][] associe pour etre exploite dans un
  //BCalqueGrilleReguliere
  private double[][] construitPoints(final double[] _resu) {
    if (_resu != null) {
      double[][] vvz;
      vvz= new double[nbMailleX_ + 1][nbMailleY_ + 1];
      for (int j= 0; j < (nbMailleY_ + 1); j++) {
        for (int i= 0; i < (nbMailleX_ + 1); i++) {
          //System.out.println(" i = "+i+" j = "+j);
          vvz[i][j]= _resu[j * (nbMailleX_ + 1) + i];
        }
      }
      return (vvz);
    }
    System.err.println(
      "Le tableau de parametres a transformer en points est null!");
    return null;
  }
  // Renvoie, sous forme d'un YapodQuery, le resultat d'une requete
  // specifique sur un champ pasee sous forme d'un string
  public YapodQuery getQuerySpecificResult(final String _champ) {
    if ((_champ == null) || (_champ.equals(""))) {
      return null;
    }
    return new YapodAccessQuery(_champ, baseResultats_);
  }
  // Renvoie le resultat d'une requete
  // specifique sur un champ pasee sous forme d'un string
  public double[][] getSpecificResult(final String _champ) {
    if ((_champ == null) || (_champ.equals(""))) {
      return null;
    }
      return (queryToDouble(new YapodAccessQuery(_champ, baseResultats_)));
  }
  public BCalqueVecteur getSpecificCalqueVecteur(final String _calque) {
    BCalqueVecteur calque;
    double[] vecteurX;
    double[] vecteurY;
    double[] abs;
    double[] ord;
    double[] dir;
    int nbLogX= 0;
    int nbLogY= 0;
    double maxVectX= 0.;
    double maxVectY= 0.;
    Enumeration eVectX= null;
    Enumeration eVectY= null;
    Enumeration eAbs= null;
    Enumeration eOrd= null;
    Enumeration eDir= null;
    if ((_calque == null) || (_calque.equals(""))) {
      return null;
    }
    calque= new BCalqueVecteur();
    vecteurX= new double[tailleResultats_];
    vecteurY= new double[tailleResultats_];
    abs= new double[tailleResultats_];
    ord= new double[tailleResultats_];
    eAbs= (new YapodAccessQuery("xp", baseResultats_)).getResult();
    eOrd= (new YapodAccessQuery("yp", baseResultats_)).getResult();
    if (_calque.equals("dir")) {
      eDir= (new YapodAccessQuery("dir", baseResultats_)).getResult();
    } else {
      eVectX=
        (new YapodAccessQuery(_calque + "X", baseResultats_)).getResult();
      eVectY=
        (new YapodAccessQuery(_calque + "Y", baseResultats_)).getResult();
    }
    try {
      int indice= 0;
      while (eAbs.hasMoreElements()) {
        abs[indice]= ((Double) (eAbs.nextElement())).doubleValue();
        ord[indice]= ((Double) (eOrd.nextElement())).doubleValue();
        indice++;
      }
      indice= 0;
      if (_calque.equals("dir")) {
        dir= new double[tailleResultats_];
        while (eDir.hasMoreElements()) {
          dir[indice]= ((Double) (eDir.nextElement())).doubleValue();
          vecteurX[indice]= (Math.cos(Math.toRadians((dir[indice]))));
          vecteurY[indice]= (Math.sin(Math.toRadians((dir[indice]))));
          indice++;
        }
      } else {
        while (eVectX.hasMoreElements()) {
          vecteurX[indice]= ((Double) (eVectX.nextElement())).doubleValue();
          /*
           *                   if (Math.abs(vecteurX[indice])>maxVectX)
           *                   	maxVectX=Math.abs(vecteurX[indice]);
           */
          if (!(vecteurX[indice] == 0)) {
            maxVectX += Math.log(Math.abs(vecteurX[indice]));
            nbLogX++;
          }
          vecteurY[indice]= ((Double) (eVectY.nextElement())).doubleValue();
          if (!(vecteurY[indice] == 0)) {
            maxVectY += Math.log(Math.abs(vecteurY[indice]));
            nbLogY++;
          }
          /*
           *                   if (Math.abs(vecteurY[indice])>maxVectY)
           *                   	maxVectY=Math.abs(vecteurY[indice]);
           */
          indice++;
        }
      }
    } catch (final ArrayIndexOutOfBoundsException exc) {
      exc.printStackTrace();
    }
    final double minPas= Math.min(pasX_, pasY_);
    //System.err.println("maxVectX = "+maxVectX);
    //System.err.println("maxVectY = "+maxVectX);
    //System.err.println("minPas = "+minPas);
    /*double factX= Math.exp(2. * maxVectX / nbLogX);
    double factY= Math.exp(2. * maxVectY / nbLogY);*/
    for (int i= 0; i < tailleResultats_; i++) {
      if (_calque.equals("dir")) {
        vecteurX[i]= vecteurX[i] * minPas;
        vecteurY[i]= vecteurY[i] * minPas;
      } else {
        //vecteurX[i]=vecteurX[i]*(minPas/Math.max(maxVectX,maxVectY));
        /*
         *          		vecteurX[i]=vecteurX[i]*(minPas*fact);
         *          		vecteurY[i]=vecteurY[i]*(minPas*fact);
         */
        //vecteurY[i]=vecteurY[i]*(minPas/Math.max(maxVectX,maxVectY));
      }
      calque.ajoute(
        new GrPoint(abs[i], ord[i], 0),
        new GrVecteur(vecteurX[i], vecteurY[i], 0));
    }
    return calque;
  }
  public static void print(final YapodQuery q) {
    System.out.println(q);
  }
  public static void print(final String s, final Enumeration e) {
    System.out.print(s);
    print(e);
  }
  public static void print(final Enumeration e) {
    final StringBuffer r= new StringBuffer();
    boolean p= true;
    r.append('[');
    while (e.hasMoreElements()) {
      if (p) {
        p= false;
      } else {
        r.append(", ");
      }
      r.append(e.nextElement());
    }
    r.append(']');
    System.out.println(r);
  }
}
