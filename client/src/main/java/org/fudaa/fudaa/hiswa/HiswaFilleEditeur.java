/*
 * @file         HiswaFilleEditeur.java
 * @creation     2000-01-12
 * @modification $Date: 2006-09-19 15:02:04 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hiswa;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.PrintJob;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.StringReader;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

import com.memoire.bu.*;
import com.memoire.jedit.JEditTextArea;
import com.memoire.jedit.JEditTokenMarker;

import org.fudaa.dodico.corba.hiswa.SParametresHIS;

import org.fudaa.dodico.hiswa.DParametresHiswa;
import org.fudaa.dodico.hiswa.parser.ParseException;
import org.fudaa.dodico.hiswa.parser.TokenMgrError;

import org.fudaa.ebli.impression.EbliFilleImprimable;
import org.fudaa.ebli.impression.EbliPrinter;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
/**
 * Une fenetre fille pour l'edition du fichier d'entr�e hiswa.
 *
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:02:04 $ by $Author: deniger $
 * @author       Claudio Branco
 */
public class HiswaFilleEditeur
  extends EbliFilleImprimable
  implements  BuCutCopyPasteInterface, BuUndoRedoInterface {
  HiswaImplementation hiswa_;
  private FudaaProjet projet_;
  private JComponent[] tools_;
  private JPanel pnEdition_;
  private JPanel pnBoutons_;
  JEditTextArea editeur_;
  private BuButton bValider_;
  private BuButton bAnnuler_;
  private BuButton bCheckSyntax_;
  private BuPopupMenu popMenu_;
  JMenuItem undoMenuItem_;
  JMenuItem redoMenuItem_;
  private JMenuItem cutMenuItem_;
  private JMenuItem copyMenuItem_;
  private JMenuItem pasteMenuItem_;
  private JMenuItem checkMenuItem_;
  String text_;
  private String nomFichier_;
  //on doit garder une reference sur le nom du fichier de parametres
  boolean initialise_;
  boolean error_;
  protected UndoAction undoAction;
  protected RedoAction redoAction;
  protected UndoManager undo;
  protected MyUndoableEditListener undoListener_;
  public SParametresHIS paramsHiswa_;
  final static Color normalSelectionColor= new Color(117, 178, 185);
  final static Color errorSelectionColor= new Color(255, 0, 0);
  public HiswaFilleEditeur(final HiswaImplementation _hiswa, final InputStream _in) {
    super("", true, true, true, true);
    setName("ifEditeur");
    setTitle("Fichier d'Entr�e");
    setResizable(true);
    setFrameIcon(BuResource.BU.getIcon("texte"));
    setLocation(30, 30);
    hiswa_= _hiswa;
    text_= "";
    initialise_= false;
    error_= false;
    undoAction= new UndoAction();
    redoAction= new RedoAction();
    undo= null;
    undoListener_= null;
    paramsHiswa_= null;
    tools_= null;
    bValider_= new BuButton(BuResource.BU.getString("Valider"));
    bValider_.setIcon(BuResource.BU.getIcon("valider"));
    bValider_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent evt) {
        valider();
      }
    });
    bAnnuler_= new BuButton(BuResource.BU.getString("Annuler"));
    bAnnuler_.setIcon(BuResource.BU.getIcon("annuler"));
    bAnnuler_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent evt) {
        editeur_.setText(text_); //hack
        try {
          setClosed(true);
        } catch (final java.beans.PropertyVetoException ex) {}
      }
    });
    bCheckSyntax_= new BuButton("V�rifier la syntaxe");
    bCheckSyntax_.setIcon(BuResource.BU.getIcon("controler"));
    bCheckSyntax_.setToolTipText("V�rifier la syntaxe");
    bCheckSyntax_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent evt) {
        checkSyntax();
      }
    });
    popMenu_= new BuPopupMenu();
    popMenu_.addSeparator("Hiswa");
    checkMenuItem_=
      popMenu_.addMenuItem(
        "V�rifier la syntaxe",
        "VERIFIER",
        BuResource.BU.getIcon("controler"),
        true);
    popMenu_.addSeparator("Edition");
    undoMenuItem_= popMenu_.addMenuItem("Defaire", "DEFAIRE", false);
    redoMenuItem_= popMenu_.addMenuItem("Refaire", "REFAIRE", false);
    cutMenuItem_= popMenu_.addMenuItem("Couper", "COUPER", true);
    copyMenuItem_= popMenu_.addMenuItem("Copier", "COPIER", true);
    pasteMenuItem_= popMenu_.addMenuItem("Coller", "COLLER", true);
    checkMenuItem_.addActionListener(this);
    undoMenuItem_.addActionListener(this);
    redoMenuItem_.addActionListener(this);
    cutMenuItem_.addActionListener(this);
    copyMenuItem_.addActionListener(this);
    pasteMenuItem_.addActionListener(this);
    editeur_= new JEditTextArea();
    editeur_.setRightClickPopup(popMenu_);
    editeur_.getPainter().setFont(new Font("Monospaced", Font.PLAIN, 12));
    editeur_.getPainter().setBlockCaretEnabled(false);
    editeur_.getPainter().setLineHighlightColor(new Color(255, 255, 224));
    editeur_.getPainter().setBracketHighlightColor(new Color(128, 255, 0));
    editeur_.getPainter().setEOLMarkerColor(new Color(64, 192, 128));
    editeur_.getPainter().setEOLMarkerEnabled(false);
    editeur_.getPainter().setSelectionColor(normalSelectionColor);
    //editeur_.getPainter().setInvalidLinesPainted(false);
    setText(_in);
    editeur_.getDocument().addDocumentListener(new MyDocumentListener());
    editeur_.addCaretListener(new CaretListener() {
      public void caretUpdate(final CaretEvent e) {
        if (error_) {
          editeur_.getPainter().setSelectionColor(normalSelectionColor);
          error_= false;
        }
      }
    });
    pnEdition_= new JPanel();
    pnEdition_.setLayout(new BoxLayout(pnEdition_, BoxLayout.Y_AXIS));
    pnEdition_.add(editeur_);
    pnEdition_.add(Box.createRigidArea(new Dimension(0, 10)));
    pnBoutons_= new JPanel();
    pnBoutons_.setLayout(new BoxLayout(pnBoutons_, BoxLayout.X_AXIS));
    pnBoutons_.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
    pnBoutons_.add(Box.createRigidArea(new Dimension(190, 0)));
    pnBoutons_.add(bAnnuler_);
    pnBoutons_.add(Box.createRigidArea(new Dimension(10, 0)));
    pnBoutons_.add(bValider_);
    getContentPane().add(pnEdition_, BuBorderLayout.CENTER);
    getContentPane().add(pnBoutons_, BuBorderLayout.SOUTH);
    addInternalFrameListener(new InternalFrameListener() {
      public void internalFrameClosing(final InternalFrameEvent e) {
        int reponse_;
        boolean correct= false;
        if (hasChanged()) {
          reponse_=
            new BuDialogConfirmation(
              hiswa_,
              hiswa_.getInformationsSoftware(),
              "Les Parametres ont chang�\nVoulez-vous enregistrer vos modifications?")
              .activate();
          if (reponse_ == JOptionPane.YES_OPTION) {
            correct= valider();
            if (!correct) {
              new BuDialogError(
                hiswa_,
                hiswa_.getInformationsSoftware(),
                "Les param�tres n'ont pas �t� enregistr�s")
                .activate();
            }
          } 
        }
        text_= "";
        initialise_= false;
        if (error_) {
          editeur_.getPainter().setSelectionColor(normalSelectionColor);
          error_= false; //precaution
        }
        setTitle("Fichier d'Entr�e");
        editeur_.getDocument().removeUndoableEditListener(undoListener_);
        undoListener_= null;
        undo= null;
      }
      public void internalFrameActivated(final InternalFrameEvent e) {
        //bug Jedit ???
        //force les scrollBars a s'adapter au texte
        editeur_.recalculateVisibleLines();
      }
      public void internalFrameClosed(final InternalFrameEvent e) {}
      public void internalFrameDeactivated(final InternalFrameEvent e) {}
      public void internalFrameDeiconified(final InternalFrameEvent e) {}
      public void internalFrameIconified(final InternalFrameEvent e) {}
      public void internalFrameOpened(final InternalFrameEvent e) {}
    });
    pack();
    setVisible(true);
  }
  public void actionPerformed(final ActionEvent _evt) {
    final String action= _evt.getActionCommand();
    if (action.equals("VERIFIER")) {
      checkSyntax();
    } else if (action.equals("DEFAIRE")) {
      undo();
    } else if (action.equals("REFAIRE")) {
      redo();
    } else if (action.equals("COUPER")) {
      cut();
    } else if (action.equals("COPIER")) {
      copy();
    } else if (action.equals("COLLER")) {
      paste();
    } else if (action.equals("DUPLIQUER")) {
      duplicate();
    } else {
      hiswa_.actionPerformed(_evt);
    }
  }
  public SParametresHIS checkSyntax() {
    SParametresHIS params;
    try {
      params=
        DParametresHiswa.litParametresHIS(new StringReader(editeur_.getText()));
    } catch (final IOException e) {
      System.err.println("Erreur IOException: " + e.getMessage());
      e.printStackTrace();
      return null;
    } catch (final ParseException e) {
      final String message= e.getMessage();
      int errorLine;
      errorLine= (e.currentToken.next.beginLine) - 1;
      System.err.println("Erreur de syntaxe ");
      editeur_.getPainter().setSelectionColor(errorSelectionColor);
      editeur_.select(
        editeur_.getLineStartOffset(errorLine),
        editeur_.getLineEndOffset(errorLine));
      new BuDialogError(
        hiswa_,
        hiswa_.getInformationsSoftware(),
        "Erreur de Syntaxe : \n" + message + "\n")
        .activate();
      error_= true;
      return null;
    } catch (final TokenMgrError err) {
      final String message= err.getMessage();
      final int errorLine=
        (Integer
          .parseInt(
            message.substring(
              message.indexOf("line") + 5,
              message.indexOf(','))))
          - 1;
      System.err.println("Erreur Lexicale ");
      editeur_.select(
        editeur_.getLineStartOffset(errorLine),
        editeur_.getLineEndOffset(errorLine));
      editeur_.getPainter().setSelectionColor(errorSelectionColor);
      new BuDialogError(
        hiswa_,
        hiswa_.getInformationsSoftware(),
        "Erreur Lexicale : \n" + message + "\n")
        .activate();
      error_= true;
      return null;
    }
    String mesgError= "";
    boolean errorFrame= false;
    boolean errorTable= false;
    boolean errorPaper= false;
    boolean errorFixed= false;
    if (!(params.parametresFrame.ecriture)) {
      errorFrame= true;
      mesgError += "\nLa commande Frame n'est pas mentionn�e";
    }
    if (!(params.parametresTable.ecriture)) {
      errorTable= true;
      mesgError += "\nLa commande Table n'est pas mentionn�e";
    }
    if ((!errorTable) && !(params.parametresTable.format).equals("PAPER")) {
      errorPaper= true;
      mesgError += "\nLe format du Tableau de resultats n'est pas \"PAPER\"";
    }
    if (!(params.parametresGrid.option.startsWith("F"))) {
      errorFixed= true;
      mesgError
        += "\nL'option \"FIXED\" de la commande\"GRID\" n'est pas mentionn�e";
    }
    if (errorFrame || errorTable || errorPaper || errorFixed) {
      new BuDialogError(
        hiswa_,
        hiswa_.getInformationsSoftware(),
        "Le fichier n'est pas valide : " + mesgError)
        .activate();
      return null;
    }
    System.err.println("Syntaxe correcte ");
    new BuDialogMessage(
      hiswa_,
      hiswa_.getInformationsSoftware(),
      "Syntaxe Correcte")
      .activate();
    return params;
  }
  public boolean valider() {
    if (hasChanged()) {
      paramsHiswa_= checkSyntax();
      if (!(paramsHiswa_ == null)) {
        paramsHiswa_.parametresFichier.filename= nomFichier_;
        new BuDialogMessage(
          hiswa_,
          hiswa_.getInformationsSoftware(),
          "Param�tres enregistr�s")
          .activate();
        text_= editeur_.getText();
      } else {
        return false;
      }
    }
    setTitle("Fichier d'Entr�e");
    return true;
  }
  public void setProjet(final FudaaProjet _projet) {
    if (!(_projet == null)) {
      projet_= _projet;
    }
    nomFichier_=
      (
        (SParametresHIS)projet_.getParam(
          HiswaResource.HIS)).parametresFichier.filename;
  }
  public FudaaProjet getProjet() {
    return projet_;
  }
  public SParametresHIS getParams() {
    return paramsHiswa_;
  }
  public boolean hasNewParams() {
    return !(paramsHiswa_ == null);
  }
  public void resetParams() {
    paramsHiswa_= null;
  }
  public void setText(final InputStream _in) {
    if (!(_in == null)) {
      final LineNumberReader reader= new LineNumberReader(new InputStreamReader(_in));
      try {
        String ligne;
        while (!((ligne= reader.readLine()) == null)) {
          text_ += ligne + "\n";
        }
        _in.close(); //indispensable ???
        reader.close();
      } catch (final IOException e) {
        System.err.println(
          "Erreur lors de la lecture du fichier : " + e.getMessage());
        return;
      }
    }
    editeur_.setText(text_);
    editeur_.setCaretPosition(0);
    initialise_= true;
    undoListener_= new MyUndoableEditListener();
    editeur_.getDocument().addUndoableEditListener(undoListener_);
    undo= new UndoManager();
  }
  public String getText() {
    return (editeur_.getText());
  }
  public JEditTokenMarker getTokenMarker() {
    return (editeur_.getTokenMarker());
  }
  public void setTokenMarker(final JEditTokenMarker tkm) {
    editeur_.setTokenMarker(tkm);
  }
  public boolean hasChanged() {
    return (!(text_.equals(editeur_.getText())));
  }
  public void clearTextArea() {
    if (!(editeur_ == null)) {
      editeur_.setText("");
      hiswa_.setEnabledForAction("REFAIRE", false);
      hiswa_.setEnabledForAction("DEFAIRE", false);
      BuActionEnabler.setEnabledForAction(redoMenuItem_, "REFAIRE", false);
      BuActionEnabler.setEnabledForAction(undoMenuItem_, "DEFAIRE", false);
    }
  }
  class MyDocumentListener implements DocumentListener {
    public void insertUpdate(final DocumentEvent e) {
      updateTitle();
    }
    public void removeUpdate(final DocumentEvent e) {
      updateTitle();
    }
    public void changedUpdate(final DocumentEvent e) {}
    public void updateTitle() {
      if (initialise_) {
        if (hasChanged()) {
          setTitle("Fichier d'Entr�e (modifi�) ");
        } else {
          setTitle("Fichier d'Entr�e ");
        }
      }
    }
  }
  class MyUndoableEditListener implements UndoableEditListener {
    public void undoableEditHappened(final UndoableEditEvent e) {
      //Remember the edit and update the menus.
      undo.addEdit(e.getEdit());
      undoAction.updateUndoState();
      redoAction.updateRedoState();
    }
  }
  class UndoAction {
    public void action() {
      try {
        if (initialise_) {
          undo.undo();
        }
      } catch (final CannotUndoException ex) {
        System.out.println("Impossible de d�faire: " + ex);
        //ex.printStackTrace();
      }
      updateUndoState();
      redoAction.updateRedoState();
    }
    protected void updateUndoState() {
      if (undo.canUndo()) {
        hiswa_.setEnabledForAction("DEFAIRE", true);
        BuActionEnabler.setEnabledForAction(undoMenuItem_, "DEFAIRE", true);
      } else {
        hiswa_.setEnabledForAction("DEFAIRE", false);
        BuActionEnabler.setEnabledForAction(undoMenuItem_, "DEFAIRE", false);
      }
    }
  }
  class RedoAction {
    public void action() {
      try {
        if (initialise_) {
          undo.redo();
        }
      } catch (final CannotRedoException ex) {
        System.out.println("Impossible de refaire: " + ex);
        //ex.printStackTrace();
      }
      updateRedoState();
      undoAction.updateUndoState();
    }
    protected void updateRedoState() {
      if (undo.canRedo()) {
        hiswa_.setEnabledForAction("REFAIRE", true);
        BuActionEnabler.setEnabledForAction(redoMenuItem_, "REFAIRE", true);
      } else {
        hiswa_.setEnabledForAction("REFAIRE", false);
        BuActionEnabler.setEnabledForAction(redoMenuItem_, "REFAIRE", false);
      }
    }
  }
  public String[] getEnabledActions() {
    final String[] r=
      new String[] {
        "IMPRIMER",
        "PREVISUALISER",
        "COUPER",
        "COPIER",
        "COLLER",
        "DUPLIQUER",
        "DEFAIRE",
        "REFAIRE" };
    return r;
  }
  // BuCutCopyPasteInterface
  public void cut() {
    editeur_.cut();
  }
  public void copy() {
    editeur_.copy();
  }
  public void duplicate() {
    cut();
    copy();
    copy();
  }
  public void paste() {
    editeur_.paste();
  }
  // BuUndoRedoInterface
  public void undo() {
    undoAction.action();
  }
  public void redo() {
    redoAction.action();
  }
  public JComponent[] getSpecificTools() {
    tools_= new JComponent[1];
    tools_[0]= bCheckSyntax_;
    return tools_;
  }
  public int getNumberOfPages() {
    return 1;
  }
  // BuPrintable
  public void print(final PrintJob _job, final Graphics _g) {
    BuPrinter.INFO_DOC= new BuInformationsDocument();
    BuPrinter.INFO_DOC.name= getTitle();
    BuPrinter.INFO_DOC.logo= BuResource.BU.getIcon("texte");
    BuPrinter.printString(_job, _g, editeur_.getText());
  }
  public int print(final Graphics _g, final PageFormat _format, final int _numPage) {
    if (_numPage != 0) {
      return Printable.NO_SUCH_PAGE;
    }
    EbliPrinter.printString(_g, _format, getText());
    return Printable.PAGE_EXISTS;
  }
}
