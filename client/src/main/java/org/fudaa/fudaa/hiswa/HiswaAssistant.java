/*
 * @file         HiswaAssistant.java
 * @creation     1999-01-18
 * @modification $Date: 2003-11-25 10:13:43 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hiswa;
import com.memoire.bu.BuAssistant;
/**
 * L'assistant du client Hiswa.
 *
 * @version      $Revision: 1.4 $ $Date: 2003-11-25 10:13:43 $ by $Author: deniger $
 * @author       Axel Guerrand 
 */
public class HiswaAssistant extends BuAssistant {}
