/**
 * @file         Hiswa.java
 * @creation     1999-01-18
 * @modification $Date: 2007-01-19 13:14:38 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hiswa;
import org.fudaa.fudaa.commun.impl.Fudaa;
/**
 * L'application cliente Hiswa.
 *
 * @version      $Revision: 1.8 $ $Date: 2007-01-19 13:14:38 $ by $Author: deniger $
 * @author       Axel Guerrand , Claudio Toni Branco
 */
public class Hiswa {
  /**
   * @param args les arguments
   */
  public static void main(final String[] args) {
    final Fudaa f=new Fudaa();
    f.launch(args, HiswaImplementation.informationsSoftware(),false);
    f.startApp(new HiswaImplementation());
  }
}
