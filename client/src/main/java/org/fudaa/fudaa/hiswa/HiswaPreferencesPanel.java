/*
 * @file         HiswaPreferencesPanel.java
 * @creation     2000-01-05
 * @modification $Date: 2006-09-19 15:02:04 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hiswa;
import java.awt.Dimension;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.AbstractBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import com.memoire.bu.BuAbstractPreferencesPanel;
import com.memoire.bu.BuGridLayout;
/**
 * Panneau de preferences pour Hiswa.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:02:04 $ by $Author: deniger $
 * @author       Claudio Branco
 */
public class HiswaPreferencesPanel extends BuAbstractPreferencesPanel {
  HiswaPreferences options_;
  HiswaImplementation hiswa_;
  BuGridLayout loHiswa_;
  JPanel pnHiswaResults_;
  AbstractBorder boHiswaResults_;
  BuGridLayout loHiswaResults_;
  HiswaSelectionResultats pnResults_;
  JScrollPane scrollPane_;
  JLabel labelHiswa_;
  JCheckBox[] grCheckBox_;
  public String getTitle() {
    return "Hiswa";
  }
  // Constructeur
  public HiswaPreferencesPanel(final HiswaImplementation _hiswa) {
    super();
    options_= HiswaPreferences.HISWA;
    hiswa_= _hiswa;
    // Panneau Hiswa
    loHiswa_= new BuGridLayout();
    loHiswa_.setColumns(1);
    this.setLayout(loHiswa_);
    // Resultats
    pnHiswaResults_= new JPanel();
    pnResults_= new HiswaSelectionResultats();
    grCheckBox_= pnResults_.getGrBox();
    boHiswaResults_=
      new CompoundBorder(
        new TitledBorder("Resultats"),
        new EmptyBorder(5, 5, 5, 5));
    pnHiswaResults_.setBorder(boHiswaResults_);
    loHiswaResults_= new BuGridLayout();
    loHiswaResults_.setColumns(1);
    loHiswaResults_.setVgap(5);
    pnHiswaResults_.setLayout(loHiswaResults_);
    labelHiswa_= new JLabel("S�lectionnez les R�sultats � Afficher");
    pnHiswaResults_.add(labelHiswa_);
    scrollPane_= new JScrollPane(pnResults_);
    scrollPane_.setPreferredSize(new Dimension(430, 130));
    pnHiswaResults_.add(scrollPane_);
    this.add(pnHiswaResults_);
    updateComponents();
  }
  public boolean isPreferencesValidable() {
    return true;
  }
  public void validatePreferences() {
    fillTable();
    options_.writeIniFile();
  }
  public boolean isPreferencesApplyable() {
    return true;
  }
  public void applyPreferences() {
    fillTable();
    options_.applyOn(hiswa_);
  }
  public boolean isPreferencesCancelable() {
    return true;
  }
  public void cancelPreferences() {
    options_.readIniFile();
    updateComponents();
  }
  // Methodes privees
  private void fillTable() {
    for (int i= 0; i < grCheckBox_.length; i++) {
      options_.putStringProperty(
        grCheckBox_[i].getText(),
        "" + grCheckBox_[i].isSelected());
    }
  }
  private void updateComponents() {
    for (int i= 0; i < grCheckBox_.length; i++) {
      grCheckBox_[i].setSelected(
        options_.getBooleanProperty(grCheckBox_[i].getText()));
    }
  }
}
