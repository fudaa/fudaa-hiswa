/*
 * @file         HiswaResource.java
 * @creation     1999-01-18
 * @modification $Date: 2006-09-08 16:04:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hiswa;
import com.memoire.bu.BuResource;
/**
 * Le gestionnaire de ressources de Hiswa.
 *
 * @version      $Revision: 1.5 $ $Date: 2006-09-08 16:04:26 $ by $Author: opasteur $
 * @author       Axel Guerrand 
 */
public class HiswaResource extends BuResource {
  public final static String HIS= "HIS";
  public final static String DAT= "dat";
  public final static String HISWAFichier= "HiswaFichier";
  public final static String HISWAProject= "HiswaProject";
  public final static String HISWATest= "HiswaTest";
  public final static String HISWAResult= "HiswaResult";
  public final static String HISWASet= "HiswaSet";
  public final static String HISWAInputGrid= "HiswaInputGrid";
  public final static String HISWARead= "HiswaRead";
  public final static String HISWAGrid= "HiswaGrid";
  public final static String HISWAInc= "HiswaInc";
  public final static String HISWABoundary= "HiswaBoundary";
  public final static String HISWAWind= "HiswaWind";
  public final static String HISWABreaking= "HiswaBreaking";
  public final static String HISWAFriction= "HiswaFriction";
  public final static String HISWABlk= "HiswaBlk";
  public final static String HISWANumeric= "HiswaNumeric";
  public final static String HISWAOff= "HiswaOff";
  public final static String HISWAFrame= "HiswaFrame";
  public final static String HISWACurve= "HiswaCurve";
  public final static String HISWAPoints= "HiswaPoints";
  public final static String HISWARay= "HiswaRay";
  public final static String HISWAIso= "HiswaIso";
  public final static String HISWANgrid= "HiswaNgrid";
  public final static String HISWABlock= "HiswaBlock";
  public final static String HISWATable= "HiswaTable";
  public final static String HISWASpec= "HiswaSpec";
  public final static String HISWANest= "HiswaNest";
  public final static String HISWAShow= "HiswaShow";
  public final static String HISWAPlot= "HiswaPlot";
  public final static String HISWALine= "HiswaLine";
  public final static String HISWAPlace= "HiswaPlace";
  public final static HiswaResource HISWA= new HiswaResource();
}
