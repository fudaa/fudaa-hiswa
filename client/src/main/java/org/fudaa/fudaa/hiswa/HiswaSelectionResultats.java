/*
 * @file         HiswaSelectionResultats.java
 * @creation     2000-01-05
 * @modification $Date: 2006-09-19 15:02:04 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hiswa;
import javax.swing.JCheckBox;

import com.memoire.bu.BuPanel;

import cz.autel.dmi.HIGConstraints;
import cz.autel.dmi.HIGLayout;
/**
 * Une Boite de Dialogue pour selectionner quels r�sultats Hiswa Sp�cifiques on veut afficher.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:02:04 $ by $Author: deniger $
 * @author       Claudio Branco
 */
public class HiswaSelectionResultats extends BuPanel {
  JCheckBox[] grCheckBoxResults_;
  public HiswaSelectionResultats() {
    super();
    int indice= 0;
    // Cr�ation du layout manager
    final int[] width= { 10, 170, 10, 170, 10 }; // Largeur des colonnes.
    final int[] height= { 5, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 5 };
    // Hauteur des lignes.
    final HIGLayout lm= new HIGLayout(width, height);
    final HIGConstraints c= new HIGConstraints();
    this.setLayout(lm);
    grCheckBoxResults_= new JCheckBox[HiswaTableauResultats.RESULTATS.length];
    for (int i= 0; i < HiswaTableauResultats.RESULTATS.length; i++) {
      grCheckBoxResults_[i]= new JCheckBox(HiswaTableauResultats.RESULTATS[i]);
    }
    for (indice= 0; indice < 10; indice++) {
      this.add(grCheckBoxResults_[indice], c.rc(indice + 2, 2, "l"));
      //grCheckBoxResults_[indice].setHorizontalTextPosition(SwingConstants.LEFT);
    }
    for (int i= 0; i < 10; i++) {
      this.add(grCheckBoxResults_[indice + i], c.rc(i + 2, 4, "l"));
    }
  }
  public JCheckBox[] getGrBox() {
    return grCheckBoxResults_;
  }
}
