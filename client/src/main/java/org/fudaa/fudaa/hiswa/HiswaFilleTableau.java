/*
 * @file         HiswaFilleTableau.java
 * @creation     2000-01-06
 * @modification $Date: 2006-09-19 15:02:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hiswa;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuPopupButton;
import com.memoire.bu.BuResource;

import cz.autel.dmi.HIGConstraints;
import cz.autel.dmi.HIGLayout;

import org.fudaa.ebli.tableau.EbliFilleTableau;
/**
 * Une fenetre fille pour le Tableau de Resultats.
 *
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:02:03 $ by $Author: deniger $
 * @author       Claudio Branco
 */
public class HiswaFilleTableau extends EbliFilleTableau {
  JComponent[] tools_;
  BuPopupButton pbSelectionResultats_;
  MyPanelTable pnSelectionResultats_;
  public HiswaFilleTableau() {
    super();
    tools_= null;
    pnSelectionResultats_= new MyPanelTable();
    pbSelectionResultats_=
      new BuPopupButton("Selection des Resultats", pnSelectionResultats_);
    pbSelectionResultats_.setToolTipText("Selection des Resultats");
    pbSelectionResultats_.setIcon(BuResource.BU.getIcon("tableau"));
  }
  public JComponent[] getSpecificTools() {
    pbSelectionResultats_.setDesktop((BuDesktop)getDesktopPane());
    tools_= new JComponent[1];
    tools_[0]= pbSelectionResultats_;
    return tools_;
  }
  public void setTable(final HiswaTableauResultats _table) {
    pnSelectionResultats_.addPropertyChangeListener(_table);
    super.setTable(_table);
  }
}
class MyPanelTable extends JPanel {
  JScrollPane scrollPane_;
  JButton bAppliquer_;
  JLabel lSelection_;
  HiswaSelectionResultats pnResults_;
  String[] resultatsSelectionnes_;
  HiswaPreferences options_;
  public MyPanelTable() {
    String[] resuDemandesTmp;
    int nbResultatsSelectionnes= 0;
    resuDemandesTmp= new String[20];
    options_= HiswaPreferences.HISWA;
    //On recupere les valeurs par defaut definies dans les Preferences
    for (int i= 0; i < 20; i++) {
      if (options_.getBooleanProperty(HiswaTableauResultats.RESULTATS[i])) {
        resuDemandesTmp[nbResultatsSelectionnes++]=
          HiswaTableauResultats.RESULTATS[i];
      }
    }
    //On fabrique un tableau a la bonne taille
    resultatsSelectionnes_= new String[nbResultatsSelectionnes];
    System.arraycopy(
      resuDemandesTmp,
      0,
      resultatsSelectionnes_,
      0,
      nbResultatsSelectionnes);
    resuDemandesTmp= null;
    //resultatsSelectionnes_=new String[20];
    //System.arraycopy(HiswaTableauResultats.RESULTATS,0,resultatsSelectionnes_,0,20);
    pnResults_= new HiswaSelectionResultats();
    final JCheckBox[] grCheckBox_= pnResults_.getGrBox();
    for (int i= 0; i < grCheckBox_.length; i++) {
      grCheckBox_[i].setSelected(
        options_.getBooleanProperty(HiswaTableauResultats.RESULTATS[i]));
    }
    bAppliquer_= new BuButton(BuResource.BU.getString("appliquer"));
    bAppliquer_.setIcon(BuResource.BU.getIcon("appliquer"));
    bAppliquer_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent evt) {
        envoieResultats(pnResults_.getGrBox());
      }
    });
    // Cr�ation du layout manager
    final int[] width= { 10, 150, 130, 150, 10 }; // Largeur des colonnes.
    final int[] height= { 5, 20, 5, 130, 15, 25, 10 }; // Hauteur des lignes.
    final HIGLayout lm= new HIGLayout(width, height);
    final HIGConstraints c= new HIGConstraints();
    setLayout(lm);
    lSelection_= new JLabel("S�lectionnez les R�sultats � Afficher");
    scrollPane_= new JScrollPane(pnResults_);
    scrollPane_.setPreferredSize(new Dimension(430, 430));
    add(lSelection_, c.rcwh(2, 2, 2, 1));
    add(scrollPane_, c.rcwh(4, 2, 3, 1));
    add(bAppliquer_, c.rc(6, 3));
  }
  public void envoieResultats(final JCheckBox[] grBox) {
    String[] resultatsTmp= new String[20];
    String[] oldResultats= null;
    int nbElementsSelectionnes= 0;
    for (int i= 0; i < 20; i++) {
      if (grBox[i].isSelected()) {
        resultatsTmp[nbElementsSelectionnes++]= grBox[i].getText();
      }
    }
    /*
     *        System.out.println("nbElementsSelectionnes : "+nbElementsSelectionnes);
     *        for (int i=0;i<nbElementsSelectionnes;i++)
     *           System.out.println("resultatsTmp["+i+"] = "+resultatsTmp[i]);
     *        System.out.println("Avant copie ");
     *        for (int i=0;i<resultatsSelectionnes_.length;i++)
     *           System.out.println("resultatsSelectionnes_["+i+"] = "+resultatsSelectionnes_[i]);
     */
    oldResultats= new String[resultatsSelectionnes_.length];
    System.arraycopy(
      resultatsSelectionnes_,
      0,
      oldResultats,
      0,
      resultatsSelectionnes_.length);
    resultatsSelectionnes_= new String[nbElementsSelectionnes];
    System.arraycopy(
      resultatsTmp,
      0,
      resultatsSelectionnes_,
      0,
      nbElementsSelectionnes);
    resultatsTmp= null;
    /*
     *        System.out.println("Apres copie ");
     *        for (int i=0;i<resultatsSelectionnes_.length;i++)
     *           System.out.println("resultatsSelectionnes_["+i+"] = "+resultatsSelectionnes_[i]);
     */
    firePropertyChange(
      "resultatsDemandes",
      oldResultats,
      resultatsSelectionnes_);
  }
}
