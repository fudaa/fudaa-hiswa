/*
 * @file         HiswaTokenMarker.java
 * @creation     2000-01-12
 * @modification $Date: 2006-09-19 15:02:04 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hiswa;
import javax.swing.text.Segment;

import com.memoire.jedit.JEditKeywordMap;
import com.memoire.jedit.JEditToken;
import com.memoire.jedit.JEditTokenMarker;
/**
 * Un Token Marker pour Hiswa , largement inspiree de HtmlTokenMarker.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:02:04 $ by $Author: deniger $
 * @author       Slava Pestov , Claudio Branco
 */
public class HiswaTokenMarker extends JEditTokenMarker {
  public static final byte HISWA= JEditToken.INTERNAL_FIRST;
  private static JEditKeywordMap hiswaKeywords_;
  private int lastOffset;
  private int lastKeyword;
  public HiswaTokenMarker() {
    super();
    initKeywords();
  }
  public byte markTokensImpl(byte token, final Segment line, final int lineIndex) {
    final char[] array= line.array;
    final int offset= line.offset;
    lastOffset= offset;
    lastKeyword= offset;
    /*
     *        Seulement 2 types de Tokens:
     *        	* soit HISWA ( Commande Hiswa )
     *       	* soit Literal2 ( '...' )
     *        Comme il y a toujours une commande HIswa avant un '...' le
     *        Token par defaut est toujours HISWA
     */
    token= HISWA;
    loop : for (int i= offset; i < length; i++) {
      final int i1= (i + 1);
      final char c= array[i];
      switch (token) {
        case HISWA : // Inside a HISWA
          switch (c) {
            case '\'' :
              {
                doKeyword(line, i);
                addToken(i - lastOffset, JEditToken.NULL);
                lastOffset= lastKeyword= i;
                token= JEditToken.LITERAL2;
              }
              break;
            case '$' :
              doKeyword(line, i);
              if (length - i > 1) {
                addToken(i - lastOffset, JEditToken.NULL);
                lastOffset= lastKeyword= i;
                addToken(length - i, JEditToken.COMMENT2);
                lastOffset= lastKeyword= length;
                break loop;
              }
              break;
            default :
              if (!Character.isLetterOrDigit(c) && c != '_') {
                doKeyword(line, i);
              }
              break;
          }
          break;
        case JEditToken.LITERAL2 : // Hiswa '...'
          if (c == '\'') {
            addToken(i1 - lastOffset, JEditToken.LITERAL2);
            lastOffset= lastKeyword= i1;
            token= HISWA;
          }
          break;
        default :
          throw new InternalError("Invalid state: " + token);
      }
    }
    switch (token) {
      case HISWA :
        doKeyword(line, length);
        addToken(length - lastOffset, JEditToken.NULL);
        break;
      default :
        addToken(length - lastOffset, token);
        break;
    }
    return token;
  }
  private boolean doKeyword(final Segment line, final int i) {
    final int i1= i + 1;
    final int len= i - lastKeyword;
    final byte id= hiswaKeywords_.lookup(line, lastKeyword, len);
    if (id != JEditToken.NULL) {
      if (lastKeyword != lastOffset) {
        addToken(lastKeyword - lastOffset, JEditToken.NULL);
      }
      addToken(len, id);
      lastOffset= i;
    }
    lastKeyword= i1;
    return false;
  }
  private void initKeywords() {
    hiswaKeywords_= new JEditKeywordMap(true);
    hiswaKeywords_.add("PROJECT", JEditToken.KEYWORD3);
    hiswaKeywords_.add("TEST", JEditToken.KEYWORD3);
    hiswaKeywords_.add("RESULT", JEditToken.KEYWORD3);
    hiswaKeywords_.add("STOP", JEditToken.KEYWORD3);
    hiswaKeywords_.add("SET", JEditToken.KEYWORD3);
    hiswaKeywords_.add("INPUT", JEditToken.KEYWORD3);
    hiswaKeywords_.add("GRID", JEditToken.KEYWORD3);
    hiswaKeywords_.add("BOTTOM", JEditToken.KEYWORD3);
    hiswaKeywords_.add("CURRENT", JEditToken.KEYWORD3);
    hiswaKeywords_.add("VX", JEditToken.KEYWORD3);
    hiswaKeywords_.add("VY", JEditToken.KEYWORD3);
    hiswaKeywords_.add("FRICTION", JEditToken.KEYWORD3);
    hiswaKeywords_.add("WIND", JEditToken.KEYWORD3);
    hiswaKeywords_.add("WX", JEditToken.KEYWORD3);
    hiswaKeywords_.add("WY", JEditToken.KEYWORD3);
    hiswaKeywords_.add("WLEVEL", JEditToken.KEYWORD3);
    hiswaKeywords_.add("VEGET", JEditToken.KEYWORD3);
    hiswaKeywords_.add("READ", JEditToken.KEYWORD3);
    hiswaKeywords_.add("FREE", JEditToken.KEYWORD3);
    hiswaKeywords_.add("FORMAT", JEditToken.KEYWORD3);
    hiswaKeywords_.add("UNFORMATTED", JEditToken.KEYWORD3);
    hiswaKeywords_.add("FIXED", JEditToken.KEYWORD3);
    hiswaKeywords_.add("ROTATING", JEditToken.KEYWORD3);
    hiswaKeywords_.add("INC", JEditToken.KEYWORD3);
    hiswaKeywords_.add("PARAM", JEditToken.KEYWORD3);
    hiswaKeywords_.add("VAR", JEditToken.KEYWORD3);
    hiswaKeywords_.add("SPECTR", JEditToken.KEYWORD3);
    hiswaKeywords_.add("RAD", JEditToken.KEYWORD3);
    hiswaKeywords_.add("DEGR", JEditToken.KEYWORD3);
    hiswaKeywords_.add("FILE", JEditToken.KEYWORD3);
    hiswaKeywords_.add("UNIFORM", JEditToken.KEYWORD3);
    hiswaKeywords_.add("BOUNDARY", JEditToken.KEYWORD3);
    hiswaKeywords_.add("NEST", JEditToken.KEYWORD3);
    hiswaKeywords_.add("LEFT", JEditToken.KEYWORD3);
    hiswaKeywords_.add("RIGHT", JEditToken.KEYWORD3);
    hiswaKeywords_.add("REFLECTING", JEditToken.KEYWORD3);
    hiswaKeywords_.add("EXTENDED", JEditToken.KEYWORD3);
    hiswaKeywords_.add("OBSTACLE", JEditToken.KEYWORD3);
    hiswaKeywords_.add("SCREEN", JEditToken.KEYWORD3);
    hiswaKeywords_.add("DAM", JEditToken.KEYWORD3);
    hiswaKeywords_.add("LINE", JEditToken.KEYWORD3);
    hiswaKeywords_.add("VEGETATION", JEditToken.KEYWORD3);
    hiswaKeywords_.add("BREAKING", JEditToken.KEYWORD3);
    hiswaKeywords_.add("FREQ", JEditToken.KEYWORD3);
    hiswaKeywords_.add("BLK", JEditToken.KEYWORD3);
    hiswaKeywords_.add("NUMERIC", JEditToken.KEYWORD3);
    hiswaKeywords_.add("OFF", JEditToken.KEYWORD3);
    hiswaKeywords_.add("REF ", JEditToken.KEYWORD3);
    hiswaKeywords_.add("TRY", JEditToken.KEYWORD3);
    hiswaKeywords_.add("BRE", JEditToken.KEYWORD3);
    hiswaKeywords_.add("FRAME", JEditToken.KEYWORD3);
    hiswaKeywords_.add("SUBGRID", JEditToken.KEYWORD3);
    hiswaKeywords_.add("NGRID", JEditToken.KEYWORD3);
    hiswaKeywords_.add("CURVE", JEditToken.KEYWORD3);
    hiswaKeywords_.add("POINTS", JEditToken.KEYWORD3);
    hiswaKeywords_.add("RAY", JEditToken.KEYWORD3);
    hiswaKeywords_.add("ISOLINE", JEditToken.KEYWORD3);
    hiswaKeywords_.add("DEPTH", JEditToken.KEYWORD3);
    hiswaKeywords_.add("BLOCK", JEditToken.KEYWORD3);
    hiswaKeywords_.add("PAPER", JEditToken.KEYWORD3);
    hiswaKeywords_.add("HSIGN", JEditToken.KEYWORD3);
    hiswaKeywords_.add("DIR", JEditToken.KEYWORD3);
    hiswaKeywords_.add("PERIOD", JEditToken.KEYWORD3);
    hiswaKeywords_.add("VEL", JEditToken.KEYWORD3);
    hiswaKeywords_.add("FORCE", JEditToken.KEYWORD3);
    hiswaKeywords_.add("TRANSP", JEditToken.KEYWORD3);
    hiswaKeywords_.add("DSPR", JEditToken.KEYWORD3);
    hiswaKeywords_.add("DISSIP", JEditToken.KEYWORD3);
    hiswaKeywords_.add("LEAK", JEditToken.KEYWORD3);
    hiswaKeywords_.add("QB", JEditToken.KEYWORD3);
    hiswaKeywords_.add("UBOT", JEditToken.KEYWORD3);
    hiswaKeywords_.add("STEEPN", JEditToken.KEYWORD3);
    hiswaKeywords_.add("WLENGTH", JEditToken.KEYWORD3);
    hiswaKeywords_.add("INDEXED", JEditToken.KEYWORD3);
    hiswaKeywords_.add("TABLE", JEditToken.KEYWORD3);
    hiswaKeywords_.add("XP", JEditToken.KEYWORD3);
    hiswaKeywords_.add("YP", JEditToken.KEYWORD3);
    hiswaKeywords_.add("DIST", JEditToken.KEYWORD3);
    hiswaKeywords_.add("SPEC", JEditToken.KEYWORD3);
    hiswaKeywords_.add("SHOW", JEditToken.KEYWORD3);
    hiswaKeywords_.add("LOCATIONS", JEditToken.KEYWORD3);
    hiswaKeywords_.add("FRIC", JEditToken.KEYWORD3);
    hiswaKeywords_.add("VELOCITY", JEditToken.KEYWORD3);
    hiswaKeywords_.add("PLOT", JEditToken.KEYWORD3);
    hiswaKeywords_.add("ISO", JEditToken.KEYWORD3);
    hiswaKeywords_.add("VEC", JEditToken.KEYWORD3);
    hiswaKeywords_.add("TDI", JEditToken.KEYWORD3);
    hiswaKeywords_.add("STAR", JEditToken.KEYWORD3);
    hiswaKeywords_.add("ACTION", JEditToken.KEYWORD3);
    hiswaKeywords_.add("PROBLEM", JEditToken.KEYWORD3);
    hiswaKeywords_.add("FREQUENCY", JEditToken.KEYWORD3);
    hiswaKeywords_.add("PLACE", JEditToken.KEYWORD3);
  }
  public static JEditKeywordMap getKeywords() {
    return hiswaKeywords_;
  }
}
