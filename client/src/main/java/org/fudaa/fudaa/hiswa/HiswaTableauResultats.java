/*
 * @file         HiswaTableauResultats.java
 * @creation     1999-12-23
 * @modification $Date: 2006-09-19 15:02:04 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hiswa;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.NumberFormat;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import com.memoire.bu.BuTable;
import com.memoire.bu.BuTableCellRenderer;

import org.fudaa.dodico.corba.hiswa.SResultatsHiswaTable;
/**
 * Tableau des resultats hiswa.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:02:04 $ by $Author: deniger $
 * @author       Axel von Arnim , Claudio Branco
 */
public class HiswaTableauResultats
  extends BuTable
  implements PropertyChangeListener {
  public HiswaTableauResultats() {
    super();
    setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
  }
  private SResultatsHiswaTable resultats_;
  private String[] resultatsDemandes_;
  private String[] resultatsValides_;
  public final static String[] RESULTATS=
    {
      "Hauteur de houle",
      "Direction",
      "Periode",
      "Profondeur d'eau",
      "Vitesse X",
      "Vitesse Y",
      "Force X",
      "Force Y",
      "Transport D'energie X",
      "Transport D'energie Y",
      "Etalement Directionnel",
      "Dissipation",
      "Perte D'Energie (theta)",
      "Fraction of breaking Waves",
      "Abscisse",
      "Ordonn�e",
      "Distance (% courbe)",
      "Vitesse Orbitale Maximale",
      "Cambrure",
      "Longueur de la vague",
      };
  public void propertyChange(final PropertyChangeEvent evt) {
    String[] oldValue_;
    String[] newValue_;
    boolean change_= false;
    if ((evt.getPropertyName()).equals("resultatsDemandes")) {
      oldValue_= (String[])evt.getOldValue();
      newValue_= (String[])evt.getNewValue();
      if (!(oldValue_.length == newValue_.length)) {
        change_= true;
      } else {
        for (int i= 0; i < oldValue_.length; i++) {
          if (!(oldValue_[i].equals(newValue_[i]))) {
            change_= true;
            break;
          }
        }
      }
      if (change_) {
        setResultatsDemandes((String[])evt.getNewValue());
      }
    }
  }
  public SResultatsHiswaTable getResultats() {
    return resultats_;
  }
  public void setResultats(final SResultatsHiswaTable _resultats) {
    final SResultatsHiswaTable vp= resultats_;
    resultats_= _resultats;
    reinitialise();
    repaint();
    firePropertyChange("resultats", vp, resultats_);
  }
  public void setResultatsValides(final String[] _resultatsValides) {
    final String[] vp= resultatsValides_;
    resultatsValides_= _resultatsValides;
    firePropertyChange("resultatsValides", vp, resultatsValides_);
  }
  String[] getResultatsDemandes() {
    return resultatsDemandes_;
  }
  public void setResultatsDemandes(final String[] _resultatsDemandes) {
    int nbResultatsValides= 0;
    final String[] vp= resultatsDemandes_;
    for (nbResultatsValides= 0;
      nbResultatsValides < _resultatsDemandes.length;
      nbResultatsValides++) {
      if (_resultatsDemandes[nbResultatsValides].equals("")) {
        break;
      }
    }
    resultatsDemandes_= new String[nbResultatsValides];
    System.arraycopy(
      _resultatsDemandes,
      0,
      resultatsDemandes_,
      0,
      nbResultatsValides);
    reinitialise();
    repaint();
    firePropertyChange("resultats", vp, resultats_);
  }
  public void reinitialise() {
    final Vector resultatsNonDemandes= new Vector();
    if (resultats_ == null) {
      return;
    }
    setModel(new HiswaTableauResultatsModel(resultats_, resultatsValides_));
    final BuTableCellRenderer tcr= new BuTableCellRenderer();
    final NumberFormat nf= NumberFormat.getInstance();
    nf.setMaximumFractionDigits(2);
    tcr.setNumberFormat(nf);
    for (int i= 0; i < getModel().getColumnCount(); i++) {
      getColumn(getColumnName(i)).setCellRenderer(tcr);
      getColumn(getColumnName(i)).setWidth(100);
    }
    for (int i= 0; i < RESULTATS.length; i++) {
      resultatsNonDemandes.addElement(RESULTATS[i]);
    }
    for (int i= 0; i < resultatsDemandes_.length; i++) {
      resultatsNonDemandes.remove(resultatsDemandes_[i]);
    }
    for (int i= 0; i < resultatsNonDemandes.size(); i++) {
      removeColumn(getColumn(resultatsNonDemandes.elementAt(i)));
    }
    sizeColumnsToFit(-1);
  }
}
class HiswaTableauResultatsModel implements TableModel {
  Vector listeners_;
  SResultatsHiswaTable resultats_;
  String[] resultatsValides_;
  public HiswaTableauResultatsModel(
    final SResultatsHiswaTable _resultats,
    final String[] _resultatsValides) {
    listeners_= new Vector();
    resultats_= _resultats;
    resultatsValides_= _resultatsValides;
  }
  public Class getColumnClass(final int column) {
    return (Double.class);
  }
  public int getColumnCount() {
    return 20;
  }
  public String getColumnName(final int column) {
    return HiswaTableauResultats.RESULTATS[column];
  }
  public int getRowCount() {
    return resultats_.lignes.length;
  }
  public Object getValueAt(final int row, final int column) {
    boolean hsign= false;
    boolean dir= false;
    boolean per= false;
    boolean dept= false;
    boolean vel= false;
    boolean force= false;
    boolean transp= false;
    boolean dirspr= false;
    boolean dissip= false;
    boolean leak= false;
    boolean qb= false;
    boolean xp= false;
    boolean yp= false;
    boolean distan= false;
    boolean ubottom= false;
    boolean steepn= false;
    boolean wlength= false;
    Object r= null;
    // pour les correpondances , voir plus haut
    for (int i= 0; i < resultatsValides_.length; i++) {
      if (resultatsValides_[i].equals("HSIGN")) {
        hsign= true;
        //System.err.println("hsign");
      }
      if (resultatsValides_[i].equals("DIR")) {
        dir= true;
        //System.err.println("dir");
      }
      if (resultatsValides_[i].equals("PERIOD")) {
        per= true;
        //System.err.println("per");
      }
      if (resultatsValides_[i].equals("DEPTH")) {
        dept= true;
        //System.err.println("dept");
      }
      if (resultatsValides_[i].equals("VEL")) {
        vel= true;
        //System.err.println("vel");
      }
      if (resultatsValides_[i].equals("FORCE")) {
        force= true;
        //System.err.println("force");
      }
      if (resultatsValides_[i].equals("TRANSP")) {
        transp= true;
        //System.err.println("transp");
      }
      if (resultatsValides_[i].equals("DSPR")) {
        dirspr= true;
        //System.err.println("dirspr");
      }
      if (resultatsValides_[i].equals("DISSIP")) {
        dissip= true;
        //System.err.println("dissip");
      }
      if (resultatsValides_[i].equals("LEAK")) {
        leak= true;
        //System.err.println("leak");
      }
      if (resultatsValides_[i].equals("QB")) {
        qb= true;
        //System.err.println("qb");
      }
      if (resultatsValides_[i].equals("XP")) {
        xp= true;
        //System.err.println("xp");
      }
      if (resultatsValides_[i].equals("YP")) {
        yp= true;
        //System.err.println("yp");
      }
      if (resultatsValides_[i].equals("DIST")) {
        distan= true;
        //System.err.println("distan");
      }
      if (resultatsValides_[i].equals("UBOT")) {
        ubottom= true;
        //System.err.println("ubottom");
      }
      if (resultatsValides_[i].equals("STEEPN")) {
        steepn= true;
        //System.err.println("steepn");
      }
      if (resultatsValides_[i].equals("WLENGTH")) {
        wlength= true;
        //System.err.println("wlength");
      }
    }
    switch (column) {
      case 0 :
        if (hsign) {
          r= new Double(resultats_.lignes[row].hsign);
        } else {
          r= "";
        }
        break;
      case 1 :
        if (dir) {
          r= new Double(resultats_.lignes[row].dir);
        } else {
          r= "";
        }
        break;
      case 2 :
        if (per) {
          r= new Double(resultats_.lignes[row].period);
        } else {
          r= "";
        }
        break;
      case 3 :
        if (dept) {
          r= new Double(resultats_.lignes[row].depth);
        } else {
          r= "";
        }
        break;
      case 4 :
        if (vel) {
          r= new Double(resultats_.lignes[row].velX);
        } else {
          r= "";
        }
        break;
      case 5 :
        if (vel) {
          r= new Double(resultats_.lignes[row].velY);
        } else {
          r= "";
        }
        break;
      case 6 :
        if (force) {
          r= new Double(resultats_.lignes[row].forceX);
        } else {
          r= "";
        }
        break;
      case 7 :
        if (force) {
          r= new Double(resultats_.lignes[row].forceY);
        } else {
          r= "";
        }
        break;
      case 8 :
        if (transp) {
          r= new Double(resultats_.lignes[row].transpX);
        } else {
          r= "";
        }
        break;
      case 9 :
        if (transp) {
          r= new Double(resultats_.lignes[row].transpY);
        } else {
          r= "";
        }
        break;
      case 10 :
        if (dirspr) {
          r= new Double(resultats_.lignes[row].dspr);
        } else {
          r= "";
        }
        break;
      case 11 :
        if (dissip) {
          r= new Double(resultats_.lignes[row].dissip);
        } else {
          r= "";
        }
        break;
      case 12 :
        if (leak) {
          r= new Double(resultats_.lignes[row].leak);
        } else {
          r= "";
        }
        break;
      case 13 :
        if (qb) {
          r= new Double(resultats_.lignes[row].qb);
        } else {
          r= "";
        }
        break;
      case 14 :
        if (xp) {
          r= new Double(resultats_.lignes[row].xp);
        } else {
          r= "";
        }
        break;
      case 15 :
        if (yp) {
          r= new Double(resultats_.lignes[row].yp);
        } else {
          r= "";
        }
        break;
      case 16 :
        if (distan) {
          r= new Double(resultats_.lignes[row].dist);
        } else {
          r= "";
        }
        break;
      case 17 :
        if (ubottom) {
          r= new Double(resultats_.lignes[row].ubot);
        } else {
          r= "";
        }
        break;
      case 18 :
        if (steepn) {
          r= new Double(resultats_.lignes[row].steepn);
        } else {
          r= "";
        }
        break;
      case 19 :
        if (wlength) {
          r= new Double(resultats_.lignes[row].wlength);
        } else {
          r= "";
        }
        break;
    }
    return r;
  }
  public boolean isCellEditable(final int row, final int column) {
    return false;
  }
  public void setValueAt(final Object value, final int row, final int column) {}
  public void addTableModelListener(final TableModelListener _l) {
    listeners_.addElement(_l);
  }
  public void removeTableModelListener(final TableModelListener _l) {
    listeners_.removeElement(_l);
  }
}
