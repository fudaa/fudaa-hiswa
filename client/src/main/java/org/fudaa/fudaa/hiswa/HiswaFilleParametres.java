/*
 * @file         HiswaFilleParametres.java
 * @creation     1999-02-02
 * @modification $Date: 2006-09-19 15:02:04 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hiswa;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuResource;

import org.fudaa.dodico.corba.hiswa.SParametresHiswaFichier;
import org.fudaa.dodico.corba.hiswa.SParametresHiswaProject;
import org.fudaa.dodico.corba.hiswa.SParametresHiswaSet;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
/**
 * Une fenetre fille pour entrer les parametres.
 *
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:02:04 $ by $Author: deniger $
 * @author       Axel Guerrand
 */
public class HiswaFilleParametres extends JInternalFrame {
  // JTextFields General (SET)
  JTextField tf_level;
  JTextField tf_cfae;
  JTextField tf_depmin;
  JTextField tf_negmes;
  JComboBox cb_interp;
  JComboBox cb_maxerr;
  JTextField tf_grav;
  JTextField tf_rho;
  JComboBox cb_inrhog;
  JTextField tf_leakf;
  JTextField tf_umin;
  /*// JComponents 02
  JLabel lb_import02;
  JButton bt_import02;*/
  JComponent content_;
  BuCommonInterface appli_;
  public HiswaFilleParametres(final BuCommonInterface _appli) {
    super("", false, true, false, true);
    appli_= _appli;
    int n;
    //  Parametres generaux
    final JPanel jpset= new JPanel();
    final BuGridLayout glset= new BuGridLayout();
    glset.setColumns(3);
    glset.setHgap(5);
    glset.setVgap(5);
    jpset.setLayout(glset);
    jpset.setBorder(new EmptyBorder(5, 5, 5, 5));
    n= 0;
    tf_level= new JTextField("0");
    tf_level.setName("PANELSET_LEVEL");
    tf_cfae= new JTextField("1.09");
    tf_cfae.setName("PANELSET_CFAE");
    tf_depmin= new JTextField("0.05");
    tf_depmin.setName("PANELSET_DEPMIN");
    tf_negmes= new JTextField("200");
    tf_negmes.setName("PANELSET_NEGMES");
    cb_interp= new JComboBox();
    cb_interp.setEditable(false);
    cb_interp.addItem("Bilin�aire");
    cb_interp.addItem("Spline");
    cb_maxerr= new JComboBox();
    cb_maxerr.setEditable(false);
    cb_maxerr.addItem("Avertissements");
    cb_maxerr.addItem("Erreurs");
    cb_maxerr.addItem("Erreurs graves");
    tf_grav= new JTextField("9.81");
    tf_grav.setName("PANELSET_GRAV");
    tf_rho= new JTextField("1025");
    tf_rho.setName("PANELSET_RHO");
    cb_inrhog= new JComboBox();
    cb_inrhog.setEditable(false);
    cb_inrhog.addItem("True energy");
    cb_inrhog.addItem("Variance");
    tf_leakf= new JTextField("1");
    tf_leakf.setName("PANELSET_LEAKF");
    tf_umin= new JTextField("1");
    tf_umin.setName("UMIN");
    //tf_level.setColumns(2);
    jpset.add(new JLabel("Niveau de l'eau:", SwingConstants.RIGHT), n++);
    jpset.add(tf_level, n++);
    jpset.add(new JLabel("(r�el)", SwingConstants.LEFT), n++);
    jpset.add(new JLabel("cfae:", SwingConstants.RIGHT), n++);
    jpset.add(tf_cfae, n++);
    jpset.add(new JLabel("(r�el)", SwingConstants.LEFT), n++);
    jpset.add(new JLabel("Seuil de profondeur [depmin]:", SwingConstants.RIGHT), n++);
    jpset.add(tf_depmin, n++);
    jpset.add(new JLabel("(r�el)", SwingConstants.LEFT), n++);
    jpset.add(new JLabel("Nb max de messages d'erreur:", SwingConstants.RIGHT), n++);
    jpset.add(tf_negmes, n++);
    jpset.add(new JLabel("(entier)", SwingConstants.LEFT), n++);
    jpset.add(new JLabel("Type d'interpolation:", SwingConstants.RIGHT), n++);
    jpset.add(cb_interp, n++);
    jpset.add(new JLabel("", SwingConstants.LEFT), n++);
    jpset.add(new JLabel("Blocage du calcul sur:", SwingConstants.RIGHT), n++);
    jpset.add(cb_maxerr, n++);
    jpset.add(new JLabel("", SwingConstants.LEFT), n++);
    jpset.add(new JLabel("Accel�ration gravitationnelle:", SwingConstants.RIGHT), n++);
    jpset.add(tf_grav, n++);
    jpset.add(new JLabel("(m/s�)", SwingConstants.LEFT), n++);
    jpset.add(new JLabel("Densit� de l'eau:", SwingConstants.RIGHT), n++);
    jpset.add(tf_rho, n++);
    jpset.add(new JLabel("(kg/m\u00b3)", SwingConstants.LEFT), n++);
    jpset.add(new JLabel("Sortie bas�e sur:", SwingConstants.RIGHT), n++);
    jpset.add(cb_inrhog, n++);
    jpset.add(new JLabel("", SwingConstants.LEFT), n++);
    jpset.add(new JLabel("Facteur de perte:", SwingConstants.RIGHT), n++);
    jpset.add(tf_leakf, n++);
    jpset.add(new JLabel("0<n<1", SwingConstants.LEFT), n++);
    jpset.add(
      new JLabel("Valeur mini de la v�locit� du vent:", SwingConstants.RIGHT),
      n++);
    jpset.add(tf_umin, n++);
    jpset.add(new JLabel("(m/s)", SwingConstants.LEFT), n++);
    // Mise en place du panneau
    final JTabbedPane tpMain= new JTabbedPane();
    final JPanel panSet= new JPanel();
    panSet.setLayout(new BuBorderLayout());
    //tf_level.setName("PANELSET");
    panSet.add("North", jpset);
    tpMain.addTab(" G�n�raux ", null, panSet, "Param�tres G�n�raux");
    content_= (JComponent)getContentPane();
    content_.setBorder(new EmptyBorder(5, 5, 5, 5));
    content_.add("Center", tpMain);
    setTitle("Param�tres de calcul");
    setFrameIcon(BuResource.BU.getIcon("parametre"));
    setLocation(40, 40);
    pack();
  }
  // Methodes publiques
  public void updatePanels(final FudaaProjet project_) {
    final SParametresHiswaFichier paramsFichier=
      (SParametresHiswaFichier)project_.getParam(HiswaResource.HISWAFichier);
    final SParametresHiswaProject paramsProject=
      (SParametresHiswaProject)project_.getParam(HiswaResource.HISWAProject);
    if (paramsFichier != null) {
      String fichier= project_.getFichier();
      fichier= fichier.substring(0, (fichier.length()) - 4);
      paramsFichier.filename= fichier;
    }
    if (paramsProject != null) {
      paramsProject.ecriture= true;
      paramsProject.name= project_.getInformations().titre;
      paramsProject.nr= project_.getFichier();
      //			paramsProject.title1 = project_.getComment();
      paramsProject.title1= project_.getInformations().commentaire;
    }
    setTextes((SParametresHiswaSet)project_.getParam(HiswaResource.HISWASet));
  }
  private void setTextes(final SParametresHiswaSet _paramsSet) {
    if (_paramsSet != null) {
      tf_level.setText("" + _paramsSet.level);
      tf_cfae.setText("" + _paramsSet.cfae);
      tf_depmin.setText("" + _paramsSet.depmin);
      tf_negmes.setText("" + _paramsSet.negmes);
      final int interp= _paramsSet.interp;
      if (interp == 1) {
        cb_interp.setSelectedItem("Bilin�aire");
      } else {
        cb_interp.setSelectedItem("Spline");
      }
      switch (_paramsSet.maxerr) {
        case 1 :
          cb_interp.setSelectedItem("Avertissements");
          break;
        case 2 :
          cb_interp.setSelectedItem("Erreurs");
          break;
        case 3 :
          cb_interp.setSelectedItem("Erreurs graves");
          break;
      }
      tf_grav.setText("" + _paramsSet.grav);
      tf_rho.setText("" + _paramsSet.rho);
      final int inrhog= _paramsSet.inrhog;
      if (inrhog == 0) {
        cb_interp.setSelectedItem("Variance");
      } else {
        cb_interp.setSelectedItem("True energy");
      }
      tf_leakf.setText("" + _paramsSet.leakf);
      tf_umin.setText("" + _paramsSet.umin);
    }
  }
  public SParametresHiswaSet getTextesSet() {
    SParametresHiswaSet paramsSet= new SParametresHiswaSet();
    try {
      paramsSet.ecriture= true;
      paramsSet.level= Double.valueOf(tf_level.getText()).doubleValue();
      paramsSet.cfae= Double.valueOf(tf_cfae.getText()).doubleValue();
      paramsSet.depmin= Double.valueOf(tf_depmin.getText()).doubleValue();
      paramsSet.negmes= Integer.valueOf(tf_negmes.getText()).intValue();
      final String interp= (String)cb_interp.getSelectedItem();
      if (interp.equals("Spline")) {
        paramsSet.interp= 2;
      } else {
        paramsSet.interp= 1;
      }
      final String maxerr= (String)cb_maxerr.getSelectedItem();
      if (maxerr.equals("Erreurs graves")) {
        paramsSet.interp= 3;
      } else if (maxerr.equals("Erreurs")) {
        paramsSet.interp= 2;
      } else {
        paramsSet.interp= 1;
      }
      paramsSet.grav= Double.valueOf(tf_grav.getText()).doubleValue();
      paramsSet.rho= Double.valueOf(tf_rho.getText()).doubleValue();
      final String inrhog= (String)cb_inrhog.getSelectedItem();
      if (inrhog.equals("Variance")) {
        paramsSet.interp= 0;
      } else {
        paramsSet.interp= 1;
      }
      paramsSet.leakf= Double.valueOf(tf_leakf.getText()).doubleValue();
      paramsSet.umin= Double.valueOf(tf_umin.getText()).doubleValue();
    } catch (final NumberFormatException e) {
      paramsSet= null;
      System.err.println("ParametresSet : NumberFormatException");
    }
    return paramsSet;
  }
}
