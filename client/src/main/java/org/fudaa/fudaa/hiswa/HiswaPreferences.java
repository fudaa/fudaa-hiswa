/*
 * @file         HiswaPreferences.java
 * @creation     1999-01-26
 * @modification $Date: 2006-09-19 15:02:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hiswa;
import com.memoire.bu.BuPreferences;
/**
 * Preferences pour Hiswa.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:02:03 $ by $Author: deniger $
 * @author       Axel Guerrand
 */
public class HiswaPreferences extends BuPreferences {
  public final static HiswaPreferences HISWA= new HiswaPreferences();
  public void applyOn(final Object _o) {
    if (!(_o instanceof HiswaImplementation)) {
      throw new RuntimeException("" + _o + " is not a HiswaImplementation.");
    //    HiswaImplementation _appli=(HiswaImplementation)_o;
    }
  }
}
