/*
 * @file         HiswaFilleCalques.java
 * @creation     1999-12-22
 * @modification $Date: 2006-11-14 09:08:08 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hiswa;
import javax.swing.JComponent;

import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuPopupButton;

import org.fudaa.ebli.calque.BArbreCalque;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.EbliFilleCalques;
import org.fudaa.ebli.controle.BSelecteurReduitParametresGouraud;
import org.fudaa.ebli.palette.BPaletteProprietesSurface;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Une fenetre fille pour les calques inspiree de CurviFilleCalques.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-11-14 09:08:08 $ by $Author: deniger $
 * @author       Guillaume Desnoix , Claudio Branco
 */
public class HiswaFilleCalques extends EbliFilleCalques {
  JComponent[] tools_;
  BuPopupButton pbProprietes_;
  BuPopupButton pbGouraud_;
  public HiswaFilleCalques(final BCalque _cq, final BArbreCalque _arbre) {
    super(_cq);
    //super(_vc/**, _arbre*/);
    addInternalFrameListener(_arbre);
    tools_= null;
    final BSelecteurReduitParametresGouraud srpg=
      new BSelecteurReduitParametresGouraud();
    srpg.addPropertyChangeListener(getArbreCalqueModel());
    srpg.setPropertyName("parametresGouraud");
    pbGouraud_= new BuPopupButton("Lissage", srpg);
    pbGouraud_.setToolTipText("Parametres de Lissage ( Gouraud )");
    pbGouraud_.setIcon(EbliResource.EBLI.getIcon("lissage"));
    final BPaletteProprietesSurface ccs= new BPaletteProprietesSurface();
    ccs.addPropertyChangeListener(getArbreCalqueModel());
    pbProprietes_= new BuPopupButton("Parametres de Surface", ccs);
    pbProprietes_.setToolTipText("Propriétés d'affichage du maillage");
    pbProprietes_.setIcon(EbliResource.EBLI.getIcon("surface"));
  }
  public JComponent[] getSpecificTools() {
    pbProprietes_.setDesktop((BuDesktop)getDesktopPane());
    pbGouraud_.setDesktop((BuDesktop)getDesktopPane());
    final JComponent[] extraTools= new JComponent[4];
    extraTools[0]= null;
    extraTools[1]= pbProprietes_;
    extraTools[2]= null;
    extraTools[3]= pbGouraud_;
    final JComponent[] superTools= super.getSpecificTools();
    tools_= new JComponent[extraTools.length + superTools.length];
    for (int i= 0; i < superTools.length; i++) {
      tools_[i]= superTools[i];
    }
    for (int i= 0; i < extraTools.length; i++) {
      tools_[i + superTools.length]= extraTools[i];
    }
    return tools_;
  }
}
