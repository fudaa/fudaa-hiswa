/*
 * @file         HiswaApplication.java
 * @creation     1999-01-18
 * @modification $Date: 2005-08-16 13:28:13 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.hiswa;
import com.memoire.bu.BuApplication;
/**
 * L'application cliente Hiswa.
 *
 * @version      $Revision: 1.5 $ $Date: 2005-08-16 13:28:13 $ by $Author: deniger $
 * @author       Axel Guerrand 
 */
public class HiswaApplication extends BuApplication {
  public static java.awt.Frame FRAME= null;
  public HiswaApplication() {
    super();
    FRAME= this;
    setImplementation(new HiswaImplementation());
  }
}
