
package com.memoire.jedit;
import javax.swing.text.Segment;

/**
 * Batch file token marker.
 *
 * @author Slava Pestov
 * @version $Id: JEditBatchFileTokenMarker.java,v 1.2 2006-09-19 14:35:03 deniger Exp $
 */
public class JEditBatchFileTokenMarker extends JEditTokenMarker
{
  public byte markTokensImpl(byte token, Segment line, int lineIndex)
  {
    char[] array = line.array;
    int offset = line.offset;
    int lastOffset = offset;
    int length = line.count + offset;

    if(JEditSyntaxUtilities.regionMatches(true,line,offset,"rem"))
    {
      addToken(line.count,JEditToken.COMMENT1);
      return JEditToken.NULL;
    }

loop:		for(int i = offset; i < length; i++)
    {
      int i1 = (i+1);

      switch(token)
      {
      case JEditToken.NULL:
        switch(array[i])
        {
        case '%':
          addToken(i - lastOffset,token);
          lastOffset = i;
          if(length - i <= 3 || array[i+2] == ' ')
          {
            addToken(2,JEditToken.KEYWORD2);
            i += 2;
            lastOffset = i;
          }
          else
            token = JEditToken.KEYWORD2;
          break;
        case '"':
          addToken(i - lastOffset,token);
          token = JEditToken.LITERAL1;
          lastOffset = i;
          break;
        case ':':
          if(i == offset)
          {
            addToken(line.count,JEditToken.LABEL);
            lastOffset = length;
            break loop;
          }
          break;
        case ' ':
          if(lastOffset == offset)
          {
            addToken(i - lastOffset,JEditToken.KEYWORD1);
            lastOffset = i;
          }
          break;
        }
        break;
      case JEditToken.KEYWORD2:
        if(array[i] == '%')
        {
          addToken(i1 - lastOffset,token);
          token = JEditToken.NULL;
          lastOffset = i1;
        }
        break;
      case JEditToken.LITERAL1:
        if(array[i] == '"')
        {
          addToken(i1 - lastOffset,token);
          token = JEditToken.NULL;
          lastOffset = i1;
        }
        break;
      default:
        throw new InternalError("Invalid state: " + token);
      }
    }

    if(lastOffset != length)
    {
      if(token != JEditToken.NULL)
        token = JEditToken.INVALID;
      else if(lastOffset == offset)
        token = JEditToken.KEYWORD1;
      addToken(length - lastOffset,token);
    }
    return JEditToken.NULL;
  }

  public boolean supportsMultilineTokens()
  {
    return false;
  }
}

/*
 * ChangeLog:
 * $Log: not supported by cvs2svn $
 * Revision 1.1  2003/01/29 10:50:38  deniger
 * version initiale
 *
 * Revision 1.4  2002/05/22 16:44:39  deniger
 * correction erreurs de jedit
 *
 * Revision 1.3  2001/07/25 17:39:05  desnoix
 * *** empty log message ***
 *
 * Revision 1.2  2001/06/21 18:50:04  desnoix
 * *** empty log message ***
 *
 * Revision 1.1  2001/06/13 19:48:18  desnoix
 * *** empty log message ***
 *
 * Revision 1.19  1999/07/16 23:45:49  sp
 * 1.7pre6 BugFree version
 *
 * Revision 1.18  1999/07/05 04:38:39  sp
 * Massive batch of changes... bug fixes, also new text component is in place.
 * Have fun
 *
 * Revision 1.17  1999/06/20 02:15:45  sp
 * Syntax coloring optimizations
 *
 * Revision 1.16  1999/06/05 00:22:58  sp
 * LGPL'd syntax package
 *
 * Revision 1.15  1999/06/03 08:24:13  sp
 * Fixing broken CVS
 *
 * Revision 1.16  1999/05/31 08:11:10  sp
 * Syntax coloring updates, expand abbrev bug fix
 *
 * Revision 1.15  1999/05/31 04:38:51  sp
 * Syntax optimizations, HyperSearch for Selection added (Mike Dillon)
 *
 * Revision 1.14  1999/04/19 05:38:20  sp
 * Syntax API changes
 *
 * Revision 1.13  1999/03/26 05:13:04  sp
 * Enhanced menu item updates
 *
 * Revision 1.12  1999/03/13 08:50:39  sp
 * Syntax colorizing updates and cleanups, general code reorganizations
 *
 * Revision 1.11  1999/03/13 00:09:07  sp
 * Console updates, uncomment removed cos it's too buggy, cvs log tags added
 *
 * Revision 1.10  1999/03/12 23:51:00  sp
 * Console updates, uncomment removed cos it's too buggy, cvs log tags added
 *
 */
