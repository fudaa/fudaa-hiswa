
package com.memoire.jedit;
import javax.swing.text.Segment;

/**
 * Eiffel token Marker.
 *
 * @author Artur Biesiadowski
 */
public class JEditEiffelTokenMarker extends JEditTokenMarker
{

  public JEditEiffelTokenMarker()
  {
    this.keywords = getKeywords();
  }

  public byte markTokensImpl(byte token, Segment line, int lineIndex)
  {
    char[] array = line.array;
    int offset = line.offset;
    lastOffset = offset;
    lastKeyword = offset;
    int length = line.count + offset;
    boolean backslash = false;

loop:		for(int i = offset; i < length; i++)
    {
      int i1 = (i+1);

      char c = array[i];
      if(c == '%')
      {
        backslash = !backslash;
        continue;
      }

      switch(token)
      {
      case JEditToken.NULL:
        switch(c)
        {
        case '"':
          doKeyword(line,i,c);
          if(backslash)
            backslash = false;
          else
          {
            addToken(i - lastOffset,token);
            token = JEditToken.LITERAL1;
            lastOffset = lastKeyword = i;
          }
          break;
        case '\'':
          doKeyword(line,i,c);
          if(backslash)
            backslash = false;
          else
          {
            addToken(i - lastOffset,token);
            token = JEditToken.LITERAL2;
            lastOffset = lastKeyword = i;
          }
          break;
        case ':':
          if(lastKeyword == offset)
          {
            if(doKeyword(line,i,c))
              break;
            backslash = false;
            addToken(i1 - lastOffset,JEditToken.LABEL);
            lastOffset = lastKeyword = i1;
          }
          else if(doKeyword(line,i,c))
            break;
          break;
        case '-':
          backslash = false;
          doKeyword(line,i,c);
          if(length - i > 1)
          {
            switch(array[i1])
            {
            case '-':
              addToken(i - lastOffset,token);
              addToken(length - i,JEditToken.COMMENT1);
              lastOffset = lastKeyword = length;
              break loop;
            }
          }
          break;
        default:
          backslash = false;
          if(!Character.isLetterOrDigit(c)
            && c != '_')
            doKeyword(line,i,c);
          break;
        }
        break;
      case JEditToken.COMMENT1:
      case JEditToken.COMMENT2:
        throw new RuntimeException("Wrong eiffel parser state");
      case JEditToken.LITERAL1:
        if(backslash)
          backslash = false;
        else if(c == '"')
        {
          addToken(i1 - lastOffset,token);
          token = JEditToken.NULL;
          lastOffset = lastKeyword = i1;
        }
        break;
      case JEditToken.LITERAL2:
        if(backslash)
          backslash = false;
        else if(c == '\'')
        {
          addToken(i1 - lastOffset,JEditToken.LITERAL1);
          token = JEditToken.NULL;
          lastOffset = lastKeyword = i1;
        }
        break;
      default:
        throw new InternalError("Invalid state: "
          + token);
      }
    }

    if(token == JEditToken.NULL)
      doKeyword(line,length,'\0');

    switch(token)
    {
    case JEditToken.LITERAL1:
    case JEditToken.LITERAL2:
      addToken(length - lastOffset,JEditToken.INVALID);
      token = JEditToken.NULL;
      break;
    case JEditToken.KEYWORD2:
      addToken(length - lastOffset,token);
      if(!backslash)
        token = JEditToken.NULL;
    default:
      addToken(length - lastOffset,token);
      break;
    }

    return token;
  }

  public static JEditKeywordMap getKeywords()
  {
    if(eiffelKeywords == null)
    {
      eiffelKeywords = new JEditKeywordMap(true);
      eiffelKeywords.add("alias", JEditToken.KEYWORD1);
      eiffelKeywords.add("all", JEditToken.KEYWORD1);
      eiffelKeywords.add("and", JEditToken.KEYWORD1);
      eiffelKeywords.add("as", JEditToken.KEYWORD1);
      eiffelKeywords.add("check", JEditToken.KEYWORD1);
      eiffelKeywords.add("class", JEditToken.KEYWORD1);
      eiffelKeywords.add("creation", JEditToken.KEYWORD1);
      eiffelKeywords.add("debug", JEditToken.KEYWORD1);
      eiffelKeywords.add("deferred", JEditToken.KEYWORD1);
      eiffelKeywords.add("do", JEditToken.KEYWORD1);
      eiffelKeywords.add("else",JEditToken.KEYWORD1);
      eiffelKeywords.add("elseif", JEditToken.KEYWORD1);
      eiffelKeywords.add("end", JEditToken.KEYWORD1);
      eiffelKeywords.add("ensure", JEditToken.KEYWORD1);
      eiffelKeywords.add("expanded", JEditToken.KEYWORD1);
      eiffelKeywords.add("export", JEditToken.KEYWORD1);
      eiffelKeywords.add("external", JEditToken.KEYWORD1);
      eiffelKeywords.add("feature", JEditToken.KEYWORD1);
      eiffelKeywords.add("from", JEditToken.KEYWORD1);
      eiffelKeywords.add("frozen", JEditToken.KEYWORD1);
      eiffelKeywords.add("if", JEditToken.KEYWORD1);
      eiffelKeywords.add("implies",JEditToken.KEYWORD1);
      eiffelKeywords.add("indexing", JEditToken.KEYWORD1);
      eiffelKeywords.add("infix", JEditToken.KEYWORD1);
      eiffelKeywords.add("inherit", JEditToken.KEYWORD1);
      eiffelKeywords.add("inspect", JEditToken.KEYWORD1);
      eiffelKeywords.add("invariant", JEditToken.KEYWORD1);
      eiffelKeywords.add("is", JEditToken.KEYWORD1);
      eiffelKeywords.add("like", JEditToken.KEYWORD1);
      eiffelKeywords.add("local", JEditToken.KEYWORD1);
      eiffelKeywords.add("loop", JEditToken.KEYWORD1);
      eiffelKeywords.add("not", JEditToken.KEYWORD1);
      eiffelKeywords.add("obsolete", JEditToken.KEYWORD1);
      eiffelKeywords.add("old",JEditToken.KEYWORD1);
      eiffelKeywords.add("once", JEditToken.KEYWORD1);
      eiffelKeywords.add("or", JEditToken.KEYWORD1);
      eiffelKeywords.add("prefix", JEditToken.KEYWORD1);
      eiffelKeywords.add("redefine", JEditToken.KEYWORD1);
      eiffelKeywords.add("rename", JEditToken.KEYWORD1);
      eiffelKeywords.add("require", JEditToken.KEYWORD1);
      eiffelKeywords.add("rescue", JEditToken.KEYWORD1);
      eiffelKeywords.add("retry", JEditToken.KEYWORD1);
      eiffelKeywords.add("select", JEditToken.KEYWORD1);
      eiffelKeywords.add("separate", JEditToken.KEYWORD1);
      eiffelKeywords.add("then",JEditToken.KEYWORD1);
      eiffelKeywords.add("undefine", JEditToken.KEYWORD1);
      eiffelKeywords.add("until", JEditToken.KEYWORD1);
      eiffelKeywords.add("variant", JEditToken.KEYWORD1);
      eiffelKeywords.add("when", JEditToken.KEYWORD1);
      eiffelKeywords.add("xor", JEditToken.KEYWORD1);

      eiffelKeywords.add("current",JEditToken.LITERAL2);
      eiffelKeywords.add("false",JEditToken.LITERAL2);
      eiffelKeywords.add("precursor",JEditToken.LITERAL2);
      eiffelKeywords.add("result",JEditToken.LITERAL2);
      eiffelKeywords.add("strip",JEditToken.LITERAL2);
      eiffelKeywords.add("true",JEditToken.LITERAL2);
      eiffelKeywords.add("unique",JEditToken.LITERAL2);
      eiffelKeywords.add("void",JEditToken.LITERAL2);

    }
    return eiffelKeywords;
  }

  // private members
  private static JEditKeywordMap eiffelKeywords;

  private boolean cpp;
  private JEditKeywordMap keywords;
  private int lastOffset;
  private int lastKeyword;

  private boolean doKeyword(Segment line, int i, char c)
  {
    int i1 = i+1;
    boolean klassname = false;

    int len = i - lastKeyword;
    byte id = keywords.lookup(line,lastKeyword,len);
    if ( id == JEditToken.NULL )
    {
      klassname = true;
      for ( int at = lastKeyword; at < lastKeyword + len; at++ )
      {
        char ch = line.array[at];
        if ( ch != '_' && !Character.isUpperCase(ch) )
        {
          klassname = false;
          break;
        }
      }
      if ( klassname )
        id = JEditToken.KEYWORD3;
    }

    if(id != JEditToken.NULL)
    {
      if(lastKeyword != lastOffset)
        addToken(lastKeyword - lastOffset,JEditToken.NULL);
      addToken(len,id);
      lastOffset = i;
    }
    lastKeyword = i1;
    return false;
  }
}


