
package com.memoire.jedit;
import javax.swing.text.Segment;

/**
 * C token marker.
 *
 * @author Slava Pestov
 * @version $Id: JEditCTokenMarker.java,v 1.2 2006-09-19 14:35:04 deniger Exp $
 */
public class JEditCTokenMarker extends JEditTokenMarker
{
  public JEditCTokenMarker()
  {
    this(true,getKeywords());
  }

  public JEditCTokenMarker(boolean cpp, JEditKeywordMap keywords)
  {
    this.cpp = cpp;
    this.keywords = keywords;
  }

  public byte markTokensImpl(byte token, Segment line, int lineIndex)
  {
    char[] array = line.array;
    int offset = line.offset;
    lastOffset = offset;
    lastKeyword = offset;
    int length = line.count + offset;
    boolean backslash = false;

loop:		for(int i = offset; i < length; i++)
    {
      int i1 = (i+1);

      char c = array[i];
      if(c == '\\')
      {
        backslash = !backslash;
        continue;
      }

      switch(token)
      {
      case JEditToken.NULL:
        switch(c)
        {
        case '#':
          if(backslash)
            backslash = false;
          else if(cpp)
          {
            if(doKeyword(line,i,c))
              break;
            addToken(i - lastOffset,token);
            addToken(length - i,JEditToken.KEYWORD2);
            lastOffset = lastKeyword = length;
            break loop;
          }
          break;
        case '"':
          doKeyword(line,i,c);
          if(backslash)
            backslash = false;
          else
          {
            addToken(i - lastOffset,token);
            token = JEditToken.LITERAL1;
            lastOffset = lastKeyword = i;
          }
          break;
        case '\'':
          doKeyword(line,i,c);
          if(backslash)
            backslash = false;
          else
          {
            addToken(i - lastOffset,token);
            token = JEditToken.LITERAL2;
            lastOffset = lastKeyword = i;
          }
          break;
        case ':':
          if(lastKeyword == offset)
          {
            if(doKeyword(line,i,c))
              break;
            backslash = false;
            addToken(i1 - lastOffset,JEditToken.LABEL);
            lastOffset = lastKeyword = i1;
          }
          else if(doKeyword(line,i,c))
            break;
          break;
        case '/':
          backslash = false;
          doKeyword(line,i,c);
          if(length - i > 1)
          {
            switch(array[i1])
            {
            case '*':
              addToken(i - lastOffset,token);
              lastOffset = lastKeyword = i;
              if(length - i > 2 && array[i+2] == '*')
                token = JEditToken.COMMENT2;
              else
                token = JEditToken.COMMENT1;
              break;
            case '/':
              addToken(i - lastOffset,token);
              addToken(length - i,JEditToken.COMMENT1);
              lastOffset = lastKeyword = length;
              break loop;
            }
          }
          break;
        default:
          backslash = false;
          if(!Character.isLetterOrDigit(c)
            && c != '_')
            doKeyword(line,i,c);
          break;
        }
        break;
      case JEditToken.COMMENT1:
      case JEditToken.COMMENT2:
        backslash = false;
        if(c == '*' && length - i > 1)
        {
          if(array[i1] == '/')
          {
            i++;
            addToken((i+1) - lastOffset,token);
            token = JEditToken.NULL;
            lastOffset = lastKeyword = i+1;
          }
        }
        break;
      case JEditToken.LITERAL1:
        if(backslash)
          backslash = false;
        else if(c == '"')
        {
          addToken(i1 - lastOffset,token);
          token = JEditToken.NULL;
          lastOffset = lastKeyword = i1;
        }
        break;
      case JEditToken.LITERAL2:
        if(backslash)
          backslash = false;
        else if(c == '\'')
        {
          addToken(i1 - lastOffset,JEditToken.LITERAL1);
          token = JEditToken.NULL;
          lastOffset = lastKeyword = i1;
        }
        break;
      default:
        throw new InternalError("Invalid state: "
          + token);
      }
    }

    if(token == JEditToken.NULL)
      doKeyword(line,length,'\0');

    switch(token)
    {
    case JEditToken.LITERAL1:
    case JEditToken.LITERAL2:
      addToken(length - lastOffset,JEditToken.INVALID);
      token = JEditToken.NULL;
      break;
    case JEditToken.KEYWORD2:
      addToken(length - lastOffset,token);
      if(!backslash)
        token = JEditToken.NULL;
    default:
      addToken(length - lastOffset,token);
      break;
    }

    return token;
  }

  public static JEditKeywordMap getKeywords()
  {
    if(cKeywords == null)
    {
      cKeywords = new JEditKeywordMap(false);
      cKeywords.add("char",JEditToken.KEYWORD3);
      cKeywords.add("double",JEditToken.KEYWORD3);
      cKeywords.add("enum",JEditToken.KEYWORD3);
      cKeywords.add("float",JEditToken.KEYWORD3);
      cKeywords.add("int",JEditToken.KEYWORD3);
      cKeywords.add("long",JEditToken.KEYWORD3);
      cKeywords.add("short",JEditToken.KEYWORD3);
      cKeywords.add("signed",JEditToken.KEYWORD3);
      cKeywords.add("struct",JEditToken.KEYWORD3);
      cKeywords.add("typedef",JEditToken.KEYWORD3);
      cKeywords.add("union",JEditToken.KEYWORD3);
      cKeywords.add("unsigned",JEditToken.KEYWORD3);
      cKeywords.add("void",JEditToken.KEYWORD3);
      cKeywords.add("auto",JEditToken.KEYWORD1);
      cKeywords.add("const",JEditToken.KEYWORD1);
      cKeywords.add("extern",JEditToken.KEYWORD1);
      cKeywords.add("register",JEditToken.KEYWORD1);
      cKeywords.add("static",JEditToken.KEYWORD1);
      cKeywords.add("volatile",JEditToken.KEYWORD1);
      cKeywords.add("break",JEditToken.KEYWORD1);
      cKeywords.add("case",JEditToken.KEYWORD1);
      cKeywords.add("continue",JEditToken.KEYWORD1);
      cKeywords.add("default",JEditToken.KEYWORD1);
      cKeywords.add("do",JEditToken.KEYWORD1);
      cKeywords.add("else",JEditToken.KEYWORD1);
      cKeywords.add("for",JEditToken.KEYWORD1);
      cKeywords.add("goto",JEditToken.KEYWORD1);
      cKeywords.add("if",JEditToken.KEYWORD1);
      cKeywords.add("return",JEditToken.KEYWORD1);
      cKeywords.add("sizeof",JEditToken.KEYWORD1);
      cKeywords.add("switch",JEditToken.KEYWORD1);
      cKeywords.add("while",JEditToken.KEYWORD1);
      cKeywords.add("asm",JEditToken.KEYWORD2);
      cKeywords.add("asmlinkage",JEditToken.KEYWORD2);
      cKeywords.add("far",JEditToken.KEYWORD2);
      cKeywords.add("huge",JEditToken.KEYWORD2);
      cKeywords.add("inline",JEditToken.KEYWORD2);
      cKeywords.add("near",JEditToken.KEYWORD2);
      cKeywords.add("pascal",JEditToken.KEYWORD2);
      cKeywords.add("true",JEditToken.LITERAL2);
      cKeywords.add("false",JEditToken.LITERAL2);
      cKeywords.add("NULL",JEditToken.LITERAL2);
    }
    return cKeywords;
  }

  // private members
  private static JEditKeywordMap cKeywords;

  private boolean cpp;
  private JEditKeywordMap keywords;
  private int lastOffset;
  private int lastKeyword;

  private boolean doKeyword(Segment line, int i, char c)
  {
    int i1 = i+1;

    int len = i - lastKeyword;
    byte id = keywords.lookup(line,lastKeyword,len);
    if(id != JEditToken.NULL)
    {
      if(lastKeyword != lastOffset)
        addToken(lastKeyword - lastOffset,JEditToken.NULL);
      addToken(len,id);
      lastOffset = i;
    }
    lastKeyword = i1;
    return false;
  }
}

/*
 * ChangeLog:
 * $Log: not supported by cvs2svn $
 * Revision 1.1  2003/01/29 10:50:39  deniger
 * version initiale
 *
 * Revision 1.4  2002/05/22 16:44:39  deniger
 * correction erreurs de jedit
 *
 * Revision 1.3  2001/07/25 17:39:05  desnoix
 * *** empty log message ***
 *
 * Revision 1.2  2001/06/21 18:50:04  desnoix
 * *** empty log message ***
 *
 * Revision 1.1  2001/06/13 19:48:18  desnoix
 * *** empty log message ***
 *
 * Revision 1.33  1999/10/31 07:15:34  sp
 * New logging API, splash screen updates, bug fixes
 *
 * Revision 1.32  1999/09/30 12:21:04  sp
 * No net access for a month... so here's one big jEdit 2.1pre1
 *
 * Revision 1.31  1999/08/21 01:48:18  sp
 * jEdit 2.0pre8
 *
 * Revision 1.30  1999/06/05 00:22:58  sp
 * LGPL'd syntax package
 *
 * Revision 1.29  1999/06/03 08:24:13  sp
 * Fixing broken CVS
 *
 * Revision 1.30  1999/05/31 08:11:10  sp
 * Syntax coloring updates, expand abbrev bug fix
 *
 * Revision 1.29  1999/05/31 04:38:51  sp
 * Syntax optimizations, HyperSearch for Selection added (Mike Dillon)
 *
 * Revision 1.28  1999/05/29 03:46:53  sp
 * JEditCTokenMarker bug fix, new splash screen
 *
 * Revision 1.27  1999/05/14 04:56:15  sp
 * Docs updated, default: fix in C/C++/Java mode, full path in title bar toggle
 *
 * Revision 1.26  1999/05/11 09:05:10  sp
 * New version1.6.html file, some other stuff perhaps
 *
 * Revision 1.25  1999/04/22 06:03:26  sp
 * Syntax colorizing change
 *
 * Revision 1.24  1999/04/19 05:38:20  sp
 * Syntax API changes
 *
 * Revision 1.23  1999/03/13 08:50:39  sp
 * Syntax colorizing updates and cleanups, general code reorganizations
 *
 * Revision 1.22  1999/03/13 00:09:07  sp
 * Console updates, uncomment removed cos it's too buggy, cvs log tags added
 *
 * Revision 1.21  1999/03/12 23:51:00  sp
 * Console updates, uncomment removed cos it's too buggy, cvs log tags added
 *
 */
