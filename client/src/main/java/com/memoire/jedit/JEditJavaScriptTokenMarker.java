
package com.memoire.jedit;

/**
 * JavaScript token marker.
 *
 * @author Slava Pestov
 * @version $Id: JEditJavaScriptTokenMarker.java,v 1.2 2006-09-19 14:35:03 deniger Exp $
 */
public class JEditJavaScriptTokenMarker extends JEditCTokenMarker
{
	public JEditJavaScriptTokenMarker()
	{
		super(false,getKeywords());
	}

	public static JEditKeywordMap getKeywords()
	{
		if(javaScriptKeywords == null)
		{
			javaScriptKeywords = new JEditKeywordMap(false);
			javaScriptKeywords.add("function",JEditToken.KEYWORD3);
			javaScriptKeywords.add("var",JEditToken.KEYWORD3);
			javaScriptKeywords.add("else",JEditToken.KEYWORD1);
			javaScriptKeywords.add("for",JEditToken.KEYWORD1);
			javaScriptKeywords.add("if",JEditToken.KEYWORD1);
			javaScriptKeywords.add("in",JEditToken.KEYWORD1);
			javaScriptKeywords.add("new",JEditToken.KEYWORD1);
			javaScriptKeywords.add("return",JEditToken.KEYWORD1);
			javaScriptKeywords.add("while",JEditToken.KEYWORD1);
			javaScriptKeywords.add("with",JEditToken.KEYWORD1);
			javaScriptKeywords.add("break",JEditToken.KEYWORD1);
			javaScriptKeywords.add("case",JEditToken.KEYWORD1);
			javaScriptKeywords.add("continue",JEditToken.KEYWORD1);
			javaScriptKeywords.add("default",JEditToken.KEYWORD1);
			javaScriptKeywords.add("false",JEditToken.LABEL);
			javaScriptKeywords.add("this",JEditToken.LABEL);
			javaScriptKeywords.add("true",JEditToken.LABEL);
		}
		return javaScriptKeywords;
	}

	// private members
	private static JEditKeywordMap javaScriptKeywords;
}

/*
 * ChangeLog:
 * $Log: not supported by cvs2svn $
 * Revision 1.1  2003/01/29 10:50:39  deniger
 * version initiale
 *
 * Revision 1.3  2001/07/25 17:39:05  desnoix
 * *** empty log message ***
 *
 * Revision 1.2  2001/06/21 18:50:04  desnoix
 * *** empty log message ***
 *
 * Revision 1.1  2001/06/13 19:48:18  desnoix
 * *** empty log message ***
 *
 * Revision 1.2  1999/06/05 00:22:58  sp
 * LGPL'd syntax package
 *
 * Revision 1.1  1999/03/13 09:11:46  sp
 * Syntax code updates, code cleanups
 *
 */
