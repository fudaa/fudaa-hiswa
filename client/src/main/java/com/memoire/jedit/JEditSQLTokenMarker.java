
package com.memoire.jedit;
import javax.swing.text.Segment;

/**
 * SQL token marker.
 *
 * @author mike dillon
 * @version $Id: JEditSQLTokenMarker.java,v 1.3 2007-05-04 13:42:00 deniger Exp $
 */
public class JEditSQLTokenMarker extends JEditTokenMarker
{
  private int offset, lastOffset, lastKeyword, length;

  // public members
  public JEditSQLTokenMarker(JEditKeywordMap k)
  {
    this(k, false);
  }

  public JEditSQLTokenMarker(JEditKeywordMap k, boolean tsql)
  {
    keywords = k;
    isTSQL = tsql;
  }

  public byte markTokensImpl(byte token, Segment line, int lineIndex)
  {
    offset = lastOffset = lastKeyword = line.offset;
    length = line.count + offset;

loop:
    for(int i = offset; i < length; i++)
    {
      switch(line.array[i])
      {
      case '*':
        if(token == JEditToken.COMMENT1 && length - i >= 1 && line.array[i+1] == '/')
        {
          token = JEditToken.NULL;
          i++;
          addToken((i + 1) - lastOffset,JEditToken.COMMENT1);
          lastOffset = i + 1;
        }
        else if (token == JEditToken.NULL)
        {
          searchBack(line, i);
          addToken(1,JEditToken.OPERATOR);
          lastOffset = i + 1;
        }
        break;
      case '[':
        if(token == JEditToken.NULL)
        {
          searchBack(line, i);
          token = JEditToken.LITERAL1;
          literalChar = '[';
          lastOffset = i;
        }
        break;
      case ']':
        if(token == JEditToken.LITERAL1 && literalChar == '[')
        {
          token = JEditToken.NULL;
          literalChar = 0;
          addToken((i + 1) - lastOffset,JEditToken.LITERAL1);
          lastOffset = i + 1;
        }
        break;
      case '.': case ',': case '(': case ')':
        if (token == JEditToken.NULL) {
          searchBack(line, i);
          addToken(1, JEditToken.NULL);
          lastOffset = i + 1;
        }
        break;
      case '+': case '%': case '&': case '|': case '^':
      case '~': case '<': case '>': case '=':
        if (token == JEditToken.NULL) {
          searchBack(line, i);
          addToken(1,JEditToken.OPERATOR);
          lastOffset = i + 1;
        }
        break;
      case ' ': case '\t':
        if (token == JEditToken.NULL) {
          searchBack(line, i, false);
        }
        break;
      case ':':
        if(token == JEditToken.NULL)
        {
          addToken((i+1) - lastOffset,JEditToken.LABEL);
          lastOffset = i + 1;
        }
        break;
      case '/':
        if(token == JEditToken.NULL)
        {
          if (length - i >= 2 && line.array[i + 1] == '*')
          {
            searchBack(line, i);
            token = JEditToken.COMMENT1;
            lastOffset = i;
            i++;
          }
          else
          {
            searchBack(line, i);
            addToken(1,JEditToken.OPERATOR);
            lastOffset = i + 1;
          }
        }
        break;
      case '-':
        if(token == JEditToken.NULL)
        {
          if (length - i >= 2 && line.array[i+1] == '-')
          {
            searchBack(line, i);
            addToken(length - i,JEditToken.COMMENT1);
            lastOffset = length;
            break loop;
          }
          searchBack(line, i);
          addToken(1,JEditToken.OPERATOR);
          lastOffset = i + 1;
        }
        break;
      case '!':
        if(isTSQL && token == JEditToken.NULL && length - i >= 2 &&
        (line.array[i+1] == '=' || line.array[i+1] == '<' || line.array[i+1] == '>'))
        {
          searchBack(line, i);
          addToken(1,JEditToken.OPERATOR);
          lastOffset = i + 1;
        }
        break;
      case '"': case '\'':
        if(token == JEditToken.NULL)
        {
          token = JEditToken.LITERAL1;
          literalChar = line.array[i];
          addToken(i - lastOffset,JEditToken.NULL);
          lastOffset = i;
        }
        else if(token == JEditToken.LITERAL1 && literalChar == line.array[i])
        {
          token = JEditToken.NULL;
          literalChar = 0;
          addToken((i + 1) - lastOffset,JEditToken.LITERAL1);
          lastOffset = i + 1;
        }
        break;
      default:
        break;
      }
    }
    if(token == JEditToken.NULL)
      searchBack(line, length, false);
    if(lastOffset != length)
      addToken(length - lastOffset,token);
    return token;
  }

  // protected members
  protected boolean isTSQL ;

  // private members
  private JEditKeywordMap keywords;
  private char literalChar ;

  private void searchBack(Segment line, int pos)
  {
    searchBack(line, pos, true);
  }

  private void searchBack(Segment line, int pos, boolean padNull)
  {
    int len = pos - lastKeyword;
    byte id = keywords.lookup(line,lastKeyword,len);
    if(id != JEditToken.NULL)
    {
      if(lastKeyword != lastOffset)
        addToken(lastKeyword - lastOffset,JEditToken.NULL);
      addToken(len,id);
      lastOffset = pos;
    }
    lastKeyword = pos + 1;
    if (padNull && lastOffset < pos)
      addToken(pos - lastOffset, JEditToken.NULL);
  }
}

/*
 * ChangeLog:
 * $Log: not supported by cvs2svn $
 * Revision 1.2  2006/09/19 14:35:03  deniger
 * Maj syntaxe +audit findbugs,PMD, Lint4j
 *
 * Revision 1.1  2003/01/29 10:50:39  deniger
 * version initiale
 *
 * Revision 1.4  2002/05/22 16:44:39  deniger
 * correction erreurs de jedit
 *
 * Revision 1.3  2001/07/25 17:39:05  desnoix
 * *** empty log message ***
 *
 * Revision 1.2  2001/06/21 18:50:04  desnoix
 * *** empty log message ***
 *
 * Revision 1.1  2001/06/13 19:48:18  desnoix
 * *** empty log message ***
 *
 * Revision 1.6  1999/04/19 05:38:20  sp
 * Syntax API changes
 *
 * Revision 1.5  1999/03/15 03:40:23  sp
 * Search and replace updates, TSQL mode/token marker updates
 *
 * Revision 1.4  1999/03/13 00:09:07  sp
 * Console updates, uncomment removed cos it's too buggy, cvs log tags added
 *
 * Revision 1.3  1999/03/12 23:51:00  sp
 * Console updates, uncomment removed cos it's too buggy, cvs log tags added
 *
 */
