
package com.memoire.jedit;
import javax.swing.text.Segment;

/**
 * Java properties/DOS INI token marker.
 *
 * @author Slava Pestov
 * @version $Id: JEditPropsTokenMarker.java,v 1.2 2006-09-19 14:35:04 deniger Exp $
 */
public class JEditPropsTokenMarker extends JEditTokenMarker
{
  public static final byte VALUE = JEditToken.INTERNAL_FIRST;

  public byte markTokensImpl(byte token, Segment line, int lineIndex)
  {
    char[] array = line.array;
    int offset = line.offset;
    int lastOffset = offset;
    int length = line.count + offset;
loop:		for(int i = offset; i < length; i++)
    {
      int i1 = (i+1);

      switch(token)
      {
      case JEditToken.NULL:
        switch(array[i])
        {
        case '#': case ';':
          if(i == offset)
          {
            addToken(line.count,JEditToken.COMMENT1);
            lastOffset = length;
            break loop;
          }
          break;
        case '[':
          if(i == offset)
          {
            addToken(i - lastOffset,token);
            token = JEditToken.KEYWORD2;
            lastOffset = i;
          }
          break;
        case '=':
          addToken(i - lastOffset,JEditToken.KEYWORD1);
          token = VALUE;
          lastOffset = i;
          break;
        }
        break;
      case JEditToken.KEYWORD2:
        if(array[i] == ']')
        {
          addToken(i1 - lastOffset,token);
          token = JEditToken.NULL;
          lastOffset = i1;
        }
        break;
      case VALUE:
        break;
      default:
        throw new InternalError("Invalid state: "
          + token);
      }
    }
    if(lastOffset != length)
      addToken(length - lastOffset,JEditToken.NULL);
    return JEditToken.NULL;
  }

  public boolean supportsMultilineTokens()
  {
    return false;
  }
}

/*
 * ChangeLog:
 * $Log: not supported by cvs2svn $
 * Revision 1.1  2003/01/29 10:50:39  deniger
 * version initiale
 *
 * Revision 1.4  2002/05/22 16:44:39  deniger
 * correction erreurs de jedit
 *
 * Revision 1.3  2001/07/25 17:39:05  desnoix
 * *** empty log message ***
 *
 * Revision 1.2  2001/06/21 18:50:04  desnoix
 * *** empty log message ***
 *
 * Revision 1.1  2001/06/13 19:48:18  desnoix
 * *** empty log message ***
 *
 * Revision 1.8  1999/10/23 03:48:22  sp
 * Mode system overhaul, close all dialog box, misc other stuff
 *
 * Revision 1.7  1999/07/05 04:38:39  sp
 * Massive batch of changes... bug fixes, also new text component is in place.
 * Have fun
 *
 * Revision 1.6  1999/06/20 02:15:45  sp
 * Syntax coloring optimizations
 *
 * Revision 1.5  1999/06/05 00:22:58  sp
 * LGPL'd syntax package
 *
 * Revision 1.4  1999/06/03 08:24:14  sp
 * Fixing broken CVS
 *
 * Revision 1.5  1999/05/31 08:11:10  sp
 * Syntax coloring updates, expand abbrev bug fix
 *
 * Revision 1.4  1999/05/31 04:38:51  sp
 * Syntax optimizations, HyperSearch for Selection added (Mike Dillon)
 *
 * Revision 1.3  1999/04/19 05:38:20  sp
 * Syntax API changes
 *
 * Revision 1.2  1999/03/12 23:51:00  sp
 * Console updates, uncomment removed cos it's too buggy, cvs log tags added
 *
 */
