package com.memoire.jedit;

import javax.swing.text.Segment;

/**
 * Perl token marker.
 * 
 * @author Slava Pestov
 * @version $Id: JEditPerlTokenMarker.java,v 1.2 2006-09-19 14:35:04 deniger Exp $
 */
public class JEditPerlTokenMarker extends JEditTokenMarker {
  // public members
  public static final byte S_ONE = JEditToken.INTERNAL_FIRST;
  public static final byte S_TWO = (byte) (JEditToken.INTERNAL_FIRST + 1);
  public static final byte S_END = (byte) (JEditToken.INTERNAL_FIRST + 2);

  public JEditPerlTokenMarker() {
    this(getKeywords());
  }

  public JEditPerlTokenMarker(JEditKeywordMap _keywords) {
    this.keywords = _keywords;
  }

  public byte markTokensImpl(byte _token, Segment line, int lineIndex) {
    char[] array = line.array;
    int offset = line.offset;
    token = _token;
    lastOffset = offset;
    lastKeyword = offset;
    matchChar = '\0';
    matchCharBracket = false;
    matchSpacesAllowed = false;
    int localLength = line.count + offset;

    if (token == JEditToken.LITERAL1 && lineIndex != 0 && lineInfo[lineIndex - 1].obj != null) {
      String str = (String) lineInfo[lineIndex - 1].obj;
      if (str != null && str.length() == line.count && JEditSyntaxUtilities.regionMatches(false, line, offset, str)) {
        addToken(line.count, token);
        return JEditToken.NULL;
      }
      addToken(line.count, token);
      lineInfo[lineIndex].obj = str;
      return token;
    }

    boolean backslash = false;
    loop: for (int i = offset; i < localLength; i++) {
      int i1 = (i + 1);

      char c = array[i];
      if (c == '\\') {
        backslash = !backslash;
        continue;
      }

      switch (token) {
      case JEditToken.NULL:
        switch (c) {
        case '#':
          if (doKeyword(line, i, c)) break;
          if (backslash) backslash = false;
          else {
            addToken(i - lastOffset, token);
            addToken(localLength - i, JEditToken.COMMENT1);
            lastOffset = lastKeyword = localLength;
            break loop;
          }
          break;
        case '=':
          backslash = false;
          if (i == offset) {
            token = JEditToken.COMMENT2;
            addToken(localLength - i, token);
            lastOffset = lastKeyword = localLength;
            break loop;
          }
          doKeyword(line, i, c);
          break;
        case '$':
        case '&':
        case '%':
        case '@':
          backslash = false;
          if (doKeyword(line, i, c)) break;
          if (localLength - i > 1) {
            if (c == '&' && (array[i1] == '&' || Character.isWhitespace(array[i1]))) i++;
            else {
              addToken(i - lastOffset, token);
              lastOffset = lastKeyword = i;
              token = JEditToken.KEYWORD2;
            }
          }
          break;
        case '"':
          if (doKeyword(line, i, c)) break;
          if (backslash) backslash = false;
          else {
            addToken(i - lastOffset, token);
            token = JEditToken.LITERAL1;
            lineInfo[lineIndex].obj = null;
            lastOffset = lastKeyword = i;
          }
          break;
        case '\'':
          if (backslash) backslash = false;
          else {
            int oldLastKeyword = lastKeyword;
            if (doKeyword(line, i, c)) break;
            if (i != oldLastKeyword) break;
            addToken(i - lastOffset, token);
            token = JEditToken.LITERAL2;
            lastOffset = lastKeyword = i;
          }
          break;
        case '`':
          if (doKeyword(line, i, c)) break;
          if (backslash) backslash = false;
          else {
            addToken(i - lastOffset, token);
            token = JEditToken.OPERATOR;
            lastOffset = lastKeyword = i;
          }
          break;
        case '<':
          if (doKeyword(line, i, c)) break;
          if (backslash) backslash = false;
          else {
            if (localLength - i > 2 && array[i1] == '<' && !Character.isWhitespace(array[i + 2])) {
              addToken(i - lastOffset, token);
              lastOffset = lastKeyword = i;
              token = JEditToken.LITERAL1;
              int len = localLength - (i + 2);
              if (array[localLength - 1] == ';') len--;
              lineInfo[lineIndex].obj = createReadinString(array, i + 2, len);
            }
          }
          break;
        case ':':
          backslash = false;
          if (doKeyword(line, i, c)) break;
          // Doesn't pick up all labels,
          // but at least doesn't mess up
          // XXX::YYY
          if (lastKeyword != 0) break;
          addToken(i1 - lastOffset, JEditToken.LABEL);
          lastOffset = lastKeyword = i1;
          break;
        case '-':
          backslash = false;
          if (doKeyword(line, i, c)) break;
          if (i != lastKeyword || localLength - i <= 1) break;
          switch (array[i1]) {
          case 'r':
          case 'w':
          case 'x':
          case 'o':
          case 'R':
          case 'W':
          case 'X':
          case 'O':
          case 'e':
          case 'z':
          case 's':
          case 'f':
          case 'd':
          case 'l':
          case 'p':
          case 'S':
          case 'b':
          case 'c':
          case 't':
          case 'u':
          case 'g':
          case 'k':
          case 'T':
          case 'B':
          case 'M':
          case 'A':
          case 'C':
            addToken(i - lastOffset, token);
            addToken(2, JEditToken.KEYWORD3);
            lastOffset = lastKeyword = i + 2;
            i++;
          }
          break;
        case '/':
        case '?':
          if (doKeyword(line, i, c)) break;
          if (localLength - i > 1) {
            backslash = false;
            char ch = array[i1];
            if (Character.isWhitespace(ch)) break;
            matchChar = c;
            matchSpacesAllowed = false;
            addToken(i - lastOffset, token);
            token = S_ONE;
            lastOffset = lastKeyword = i;
          }
          break;
        default:
          backslash = false;
          if (!Character.isLetterOrDigit(c) && c != '_') doKeyword(line, i, c);
          break;
        }
        break;
      case JEditToken.KEYWORD2:
        backslash = false;
        // This test checks for an end-of-variable
        // condition
        if (!Character.isLetterOrDigit(c) && c != '_' && c != '#' && c != '\'' && c != ':' && c != '&') {
          // If this is the first character
          // of the variable name ($'aaa)
          // ignore it
          if (i != offset && array[i - 1] == '$') {
            addToken(i1 - lastOffset, token);
            lastOffset = lastKeyword = i1;
          }
          // Otherwise, end of variable...
          else {
            addToken(i - lastOffset, token);
            lastOffset = lastKeyword = i;
            // Wind back so that stuff
            // like $hello$fred is picked
            // up
            i--;
            token = JEditToken.NULL;
          }
        }
        break;
      case S_ONE:
      case S_TWO:
        if (backslash) backslash = false;
        else {
          if (matchChar == '\0') {
            if (Character.isWhitespace(matchChar) && !matchSpacesAllowed) break;
              matchChar = c;
          } else {
            switch (matchChar) {
            case '(':
              matchChar = ')';
              matchCharBracket = true;
              break;
            case '[':
              matchChar = ']';
              matchCharBracket = true;
              break;
            case '{':
              matchChar = '}';
              matchCharBracket = true;
              break;
            case '<':
              matchChar = '>';
              matchCharBracket = true;
              break;
            default:
              matchCharBracket = false;
              break;
            }
            if (c != matchChar) break;
            if (token == S_TWO) {
              token = S_ONE;
              if (matchCharBracket) matchChar = '\0';
            } else {
              token = S_END;
              addToken(i1 - lastOffset, JEditToken.LITERAL2);
              lastOffset = lastKeyword = i1;
            }
          }
        }
        break;
      case S_END:
        backslash = false;
        if (!Character.isLetterOrDigit(c) && c != '_') doKeyword(line, i, c);
        break;
      case JEditToken.COMMENT2:
        backslash = false;
        if (i == offset) {
          addToken(line.count, token);
          if (localLength - i > 3 && JEditSyntaxUtilities.regionMatches(false, line, offset, "=cut")) token = JEditToken.NULL;
          lastOffset = lastKeyword = localLength;
          break loop;
        }
        break;
      case JEditToken.LITERAL1:
        if (backslash) backslash = false;
        /*
         * else if(c == '$') backslash = true;
         */
        else if (c == '"') {
          addToken(i1 - lastOffset, token);
          token = JEditToken.NULL;
          lastOffset = lastKeyword = i1;
        }
        break;
      case JEditToken.LITERAL2:
        if (backslash) backslash = false;
        /*
         * else if(c == '$') backslash = true;
         */
        else if (c == '\'') {
          addToken(i1 - lastOffset, JEditToken.LITERAL1);
          token = JEditToken.NULL;
          lastOffset = lastKeyword = i1;
        }
        break;
      case JEditToken.OPERATOR:
        if (backslash) backslash = false;
        else if (c == '`') {
          addToken(i1 - lastOffset, token);
          token = JEditToken.NULL;
          lastOffset = lastKeyword = i1;
        }
        break;
      default:
        throw new InternalError("Invalid state: " + token);
      }
    }

    if (token == JEditToken.NULL) doKeyword(line, localLength, '\0');

    switch (token) {
    case JEditToken.KEYWORD2:
      addToken(localLength - lastOffset, token);
      token = JEditToken.NULL;
      break;
    case JEditToken.LITERAL2:
      addToken(localLength - lastOffset, JEditToken.LITERAL1);
      break;
    case S_END:
      addToken(localLength - lastOffset, JEditToken.LITERAL2);
      token = JEditToken.NULL;
      break;
    case S_ONE:
    case S_TWO:
      addToken(localLength - lastOffset, JEditToken.INVALID); // XXX
      token = JEditToken.NULL;
      break;
    default:
      addToken(localLength - lastOffset, token);
      break;
    }
    return token;
  }

  // private members
  private JEditKeywordMap keywords;
  private byte token;
  private int lastOffset;
  private int lastKeyword;
  private char matchChar;
  private boolean matchCharBracket;
  private boolean matchSpacesAllowed;

  private boolean doKeyword(Segment line, int i, char c) {
    int i1 = i + 1;

    if (token == S_END) {
      addToken(i - lastOffset, JEditToken.LITERAL2);
      token = JEditToken.NULL;
      lastOffset = i;
      lastKeyword = i1;
      return false;
    }

    int len = i - lastKeyword;
    byte id = keywords.lookup(line, lastKeyword, len);
    if (id == S_ONE || id == S_TWO) {
      if (lastKeyword != lastOffset) addToken(lastKeyword - lastOffset, JEditToken.NULL);
      addToken(len, JEditToken.LITERAL2);
      lastOffset = i;
      lastKeyword = i1;
      if (Character.isWhitespace(c)) matchChar = '\0';
      else
        matchChar = c;
      matchSpacesAllowed = true;
      token = id;
      return true;
    } else if (id != JEditToken.NULL) {
      if (lastKeyword != lastOffset) addToken(lastKeyword - lastOffset, JEditToken.NULL);
      addToken(len, id);
      lastOffset = i;
    }
    lastKeyword = i1;
    return false;
  }

  // Converts < EOF >, < 'EOF' >, etc to <EOF>
  private String createReadinString(char[] array, int start, int len) {
    int off = 0;

    while ((off < len) && (!Character.isLetterOrDigit(array[start + off])))
      off++;

    while ((off < len) && (!Character.isLetterOrDigit(array[start + len - 1])))
      len--;

    return new String(array, start + off, len);
  }

  private static JEditKeywordMap perlKeywords;

  private static JEditKeywordMap getKeywords() {
    if (perlKeywords == null) {
      perlKeywords = new JEditKeywordMap(false);
      perlKeywords.add("my", JEditToken.KEYWORD1);
      perlKeywords.add("local", JEditToken.KEYWORD1);
      perlKeywords.add("new", JEditToken.KEYWORD1);
      perlKeywords.add("if", JEditToken.KEYWORD1);
      perlKeywords.add("until", JEditToken.KEYWORD1);
      perlKeywords.add("while", JEditToken.KEYWORD1);
      perlKeywords.add("elsif", JEditToken.KEYWORD1);
      perlKeywords.add("else", JEditToken.KEYWORD1);
      perlKeywords.add("eval", JEditToken.KEYWORD1);
      perlKeywords.add("unless", JEditToken.KEYWORD1);
      perlKeywords.add("foreach", JEditToken.KEYWORD1);
      perlKeywords.add("continue", JEditToken.KEYWORD1);
      perlKeywords.add("exit", JEditToken.KEYWORD1);
      perlKeywords.add("die", JEditToken.KEYWORD1);
      perlKeywords.add("last", JEditToken.KEYWORD1);
      perlKeywords.add("goto", JEditToken.KEYWORD1);
      perlKeywords.add("next", JEditToken.KEYWORD1);
      perlKeywords.add("redo", JEditToken.KEYWORD1);
      perlKeywords.add("goto", JEditToken.KEYWORD1);
      perlKeywords.add("return", JEditToken.KEYWORD1);
      perlKeywords.add("do", JEditToken.KEYWORD1);
      perlKeywords.add("sub", JEditToken.KEYWORD1);
      perlKeywords.add("use", JEditToken.KEYWORD1);
      perlKeywords.add("require", JEditToken.KEYWORD1);
      perlKeywords.add("package", JEditToken.KEYWORD1);
      perlKeywords.add("BEGIN", JEditToken.KEYWORD1);
      perlKeywords.add("END", JEditToken.KEYWORD1);
      perlKeywords.add("eq", JEditToken.OPERATOR);
      perlKeywords.add("ne", JEditToken.OPERATOR);
      perlKeywords.add("not", JEditToken.OPERATOR);
      perlKeywords.add("and", JEditToken.OPERATOR);
      perlKeywords.add("or", JEditToken.OPERATOR);

      perlKeywords.add("abs", JEditToken.KEYWORD3);
      perlKeywords.add("accept", JEditToken.KEYWORD3);
      perlKeywords.add("alarm", JEditToken.KEYWORD3);
      perlKeywords.add("atan2", JEditToken.KEYWORD3);
      perlKeywords.add("bind", JEditToken.KEYWORD3);
      perlKeywords.add("binmode", JEditToken.KEYWORD3);
      perlKeywords.add("bless", JEditToken.KEYWORD3);
      perlKeywords.add("caller", JEditToken.KEYWORD3);
      perlKeywords.add("chdir", JEditToken.KEYWORD3);
      perlKeywords.add("chmod", JEditToken.KEYWORD3);
      perlKeywords.add("chomp", JEditToken.KEYWORD3);
      perlKeywords.add("chr", JEditToken.KEYWORD3);
      perlKeywords.add("chroot", JEditToken.KEYWORD3);
      perlKeywords.add("chown", JEditToken.KEYWORD3);
      perlKeywords.add("closedir", JEditToken.KEYWORD3);
      perlKeywords.add("close", JEditToken.KEYWORD3);
      perlKeywords.add("connect", JEditToken.KEYWORD3);
      perlKeywords.add("cos", JEditToken.KEYWORD3);
      perlKeywords.add("crypt", JEditToken.KEYWORD3);
      perlKeywords.add("dbmclose", JEditToken.KEYWORD3);
      perlKeywords.add("dbmopen", JEditToken.KEYWORD3);
      perlKeywords.add("defined", JEditToken.KEYWORD3);
      perlKeywords.add("delete", JEditToken.KEYWORD3);
      perlKeywords.add("die", JEditToken.KEYWORD3);
      perlKeywords.add("dump", JEditToken.KEYWORD3);
      perlKeywords.add("each", JEditToken.KEYWORD3);
      perlKeywords.add("endgrent", JEditToken.KEYWORD3);
      perlKeywords.add("endhostent", JEditToken.KEYWORD3);
      perlKeywords.add("endnetent", JEditToken.KEYWORD3);
      perlKeywords.add("endprotoent", JEditToken.KEYWORD3);
      perlKeywords.add("endpwent", JEditToken.KEYWORD3);
      perlKeywords.add("endservent", JEditToken.KEYWORD3);
      perlKeywords.add("eof", JEditToken.KEYWORD3);
      perlKeywords.add("exec", JEditToken.KEYWORD3);
      perlKeywords.add("exists", JEditToken.KEYWORD3);
      perlKeywords.add("exp", JEditToken.KEYWORD3);
      perlKeywords.add("fctnl", JEditToken.KEYWORD3);
      perlKeywords.add("fileno", JEditToken.KEYWORD3);
      perlKeywords.add("flock", JEditToken.KEYWORD3);
      perlKeywords.add("fork", JEditToken.KEYWORD3);
      perlKeywords.add("format", JEditToken.KEYWORD3);
      perlKeywords.add("formline", JEditToken.KEYWORD3);
      perlKeywords.add("getc", JEditToken.KEYWORD3);
      perlKeywords.add("getgrent", JEditToken.KEYWORD3);
      perlKeywords.add("getgrgid", JEditToken.KEYWORD3);
      perlKeywords.add("getgrnam", JEditToken.KEYWORD3);
      perlKeywords.add("gethostbyaddr", JEditToken.KEYWORD3);
      perlKeywords.add("gethostbyname", JEditToken.KEYWORD3);
      perlKeywords.add("gethostent", JEditToken.KEYWORD3);
      perlKeywords.add("getlogin", JEditToken.KEYWORD3);
      perlKeywords.add("getnetbyaddr", JEditToken.KEYWORD3);
      perlKeywords.add("getnetbyname", JEditToken.KEYWORD3);
      perlKeywords.add("getnetent", JEditToken.KEYWORD3);
      perlKeywords.add("getpeername", JEditToken.KEYWORD3);
      perlKeywords.add("getpgrp", JEditToken.KEYWORD3);
      perlKeywords.add("getppid", JEditToken.KEYWORD3);
      perlKeywords.add("getpriority", JEditToken.KEYWORD3);
      perlKeywords.add("getprotobyname", JEditToken.KEYWORD3);
      perlKeywords.add("getprotobynumber", JEditToken.KEYWORD3);
      perlKeywords.add("getprotoent", JEditToken.KEYWORD3);
      perlKeywords.add("getpwent", JEditToken.KEYWORD3);
      perlKeywords.add("getpwnam", JEditToken.KEYWORD3);
      perlKeywords.add("getpwuid", JEditToken.KEYWORD3);
      perlKeywords.add("getservbyname", JEditToken.KEYWORD3);
      perlKeywords.add("getservbyport", JEditToken.KEYWORD3);
      perlKeywords.add("getservent", JEditToken.KEYWORD3);
      perlKeywords.add("getsockname", JEditToken.KEYWORD3);
      perlKeywords.add("getsockopt", JEditToken.KEYWORD3);
      perlKeywords.add("glob", JEditToken.KEYWORD3);
      perlKeywords.add("gmtime", JEditToken.KEYWORD3);
      perlKeywords.add("grep", JEditToken.KEYWORD3);
      perlKeywords.add("hex", JEditToken.KEYWORD3);
      perlKeywords.add("import", JEditToken.KEYWORD3);
      perlKeywords.add("index", JEditToken.KEYWORD3);
      perlKeywords.add("int", JEditToken.KEYWORD3);
      perlKeywords.add("ioctl", JEditToken.KEYWORD3);
      perlKeywords.add("join", JEditToken.KEYWORD3);
      perlKeywords.add("keys", JEditToken.KEYWORD3);
      perlKeywords.add("kill", JEditToken.KEYWORD3);
      perlKeywords.add("lcfirst", JEditToken.KEYWORD3);
      perlKeywords.add("lc", JEditToken.KEYWORD3);
      perlKeywords.add("length", JEditToken.KEYWORD3);
      perlKeywords.add("link", JEditToken.KEYWORD3);
      perlKeywords.add("listen", JEditToken.KEYWORD3);
      perlKeywords.add("log", JEditToken.KEYWORD3);
      perlKeywords.add("localtime", JEditToken.KEYWORD3);
      perlKeywords.add("lstat", JEditToken.KEYWORD3);
      perlKeywords.add("map", JEditToken.KEYWORD3);
      perlKeywords.add("mkdir", JEditToken.KEYWORD3);
      perlKeywords.add("msgctl", JEditToken.KEYWORD3);
      perlKeywords.add("msgget", JEditToken.KEYWORD3);
      perlKeywords.add("msgrcv", JEditToken.KEYWORD3);
      perlKeywords.add("no", JEditToken.KEYWORD3);
      perlKeywords.add("oct", JEditToken.KEYWORD3);
      perlKeywords.add("opendir", JEditToken.KEYWORD3);
      perlKeywords.add("open", JEditToken.KEYWORD3);
      perlKeywords.add("ord", JEditToken.KEYWORD3);
      perlKeywords.add("pack", JEditToken.KEYWORD3);
      perlKeywords.add("pipe", JEditToken.KEYWORD3);
      perlKeywords.add("pop", JEditToken.KEYWORD3);
      perlKeywords.add("pos", JEditToken.KEYWORD3);
      perlKeywords.add("printf", JEditToken.KEYWORD3);
      perlKeywords.add("print", JEditToken.KEYWORD3);
      perlKeywords.add("push", JEditToken.KEYWORD3);
      perlKeywords.add("quotemeta", JEditToken.KEYWORD3);
      perlKeywords.add("rand", JEditToken.KEYWORD3);
      perlKeywords.add("readdir", JEditToken.KEYWORD3);
      perlKeywords.add("read", JEditToken.KEYWORD3);
      perlKeywords.add("readlink", JEditToken.KEYWORD3);
      perlKeywords.add("recv", JEditToken.KEYWORD3);
      perlKeywords.add("ref", JEditToken.KEYWORD3);
      perlKeywords.add("rename", JEditToken.KEYWORD3);
      perlKeywords.add("reset", JEditToken.KEYWORD3);
      perlKeywords.add("reverse", JEditToken.KEYWORD3);
      perlKeywords.add("rewinddir", JEditToken.KEYWORD3);
      perlKeywords.add("rindex", JEditToken.KEYWORD3);
      perlKeywords.add("rmdir", JEditToken.KEYWORD3);
      perlKeywords.add("scalar", JEditToken.KEYWORD3);
      perlKeywords.add("seekdir", JEditToken.KEYWORD3);
      perlKeywords.add("seek", JEditToken.KEYWORD3);
      perlKeywords.add("select", JEditToken.KEYWORD3);
      perlKeywords.add("semctl", JEditToken.KEYWORD3);
      perlKeywords.add("semget", JEditToken.KEYWORD3);
      perlKeywords.add("semop", JEditToken.KEYWORD3);
      perlKeywords.add("send", JEditToken.KEYWORD3);
      perlKeywords.add("setgrent", JEditToken.KEYWORD3);
      perlKeywords.add("sethostent", JEditToken.KEYWORD3);
      perlKeywords.add("setnetent", JEditToken.KEYWORD3);
      perlKeywords.add("setpgrp", JEditToken.KEYWORD3);
      perlKeywords.add("setpriority", JEditToken.KEYWORD3);
      perlKeywords.add("setprotoent", JEditToken.KEYWORD3);
      perlKeywords.add("setpwent", JEditToken.KEYWORD3);
      perlKeywords.add("setsockopt", JEditToken.KEYWORD3);
      perlKeywords.add("shift", JEditToken.KEYWORD3);
      perlKeywords.add("shmctl", JEditToken.KEYWORD3);
      perlKeywords.add("shmget", JEditToken.KEYWORD3);
      perlKeywords.add("shmread", JEditToken.KEYWORD3);
      perlKeywords.add("shmwrite", JEditToken.KEYWORD3);
      perlKeywords.add("shutdown", JEditToken.KEYWORD3);
      perlKeywords.add("sin", JEditToken.KEYWORD3);
      perlKeywords.add("sleep", JEditToken.KEYWORD3);
      perlKeywords.add("socket", JEditToken.KEYWORD3);
      perlKeywords.add("socketpair", JEditToken.KEYWORD3);
      perlKeywords.add("sort", JEditToken.KEYWORD3);
      perlKeywords.add("splice", JEditToken.KEYWORD3);
      perlKeywords.add("split", JEditToken.KEYWORD3);
      perlKeywords.add("sprintf", JEditToken.KEYWORD3);
      perlKeywords.add("sqrt", JEditToken.KEYWORD3);
      perlKeywords.add("srand", JEditToken.KEYWORD3);
      perlKeywords.add("stat", JEditToken.KEYWORD3);
      perlKeywords.add("study", JEditToken.KEYWORD3);
      perlKeywords.add("substr", JEditToken.KEYWORD3);
      perlKeywords.add("symlink", JEditToken.KEYWORD3);
      perlKeywords.add("syscall", JEditToken.KEYWORD3);
      perlKeywords.add("sysopen", JEditToken.KEYWORD3);
      perlKeywords.add("sysread", JEditToken.KEYWORD3);
      perlKeywords.add("syswrite", JEditToken.KEYWORD3);
      perlKeywords.add("telldir", JEditToken.KEYWORD3);
      perlKeywords.add("tell", JEditToken.KEYWORD3);
      perlKeywords.add("tie", JEditToken.KEYWORD3);
      perlKeywords.add("tied", JEditToken.KEYWORD3);
      perlKeywords.add("time", JEditToken.KEYWORD3);
      perlKeywords.add("times", JEditToken.KEYWORD3);
      perlKeywords.add("truncate", JEditToken.KEYWORD3);
      perlKeywords.add("uc", JEditToken.KEYWORD3);
      perlKeywords.add("ucfirst", JEditToken.KEYWORD3);
      perlKeywords.add("umask", JEditToken.KEYWORD3);
      perlKeywords.add("undef", JEditToken.KEYWORD3);
      perlKeywords.add("unlink", JEditToken.KEYWORD3);
      perlKeywords.add("unpack", JEditToken.KEYWORD3);
      perlKeywords.add("unshift", JEditToken.KEYWORD3);
      perlKeywords.add("untie", JEditToken.KEYWORD3);
      perlKeywords.add("utime", JEditToken.KEYWORD3);
      perlKeywords.add("values", JEditToken.KEYWORD3);
      perlKeywords.add("vec", JEditToken.KEYWORD3);
      perlKeywords.add("wait", JEditToken.KEYWORD3);
      perlKeywords.add("waitpid", JEditToken.KEYWORD3);
      perlKeywords.add("wantarray", JEditToken.KEYWORD3);
      perlKeywords.add("warn", JEditToken.KEYWORD3);
      perlKeywords.add("write", JEditToken.KEYWORD3);

      perlKeywords.add("m", S_ONE);
      perlKeywords.add("q", S_ONE);
      perlKeywords.add("qq", S_ONE);
      perlKeywords.add("qw", S_ONE);
      perlKeywords.add("qx", S_ONE);
      perlKeywords.add("s", S_TWO);
      perlKeywords.add("tr", S_TWO);
      perlKeywords.add("y", S_TWO);
    }
    return perlKeywords;
  }
}

/**
 * ChangeLog: $Log: not supported by cvs2svn $ Revision 1.1 2003/01/29 10:50:38 deniger version initiale Revision 1.4
 * 2002/05/22 16:44:39 deniger correction erreurs de jedit Revision 1.3 2001/07/25 17:39:05 desnoix *** empty log
 * message *** Revision 1.2 2001/06/21 18:50:04 desnoix *** empty log message *** Revision 1.1 2001/06/13 19:48:18
 * desnoix *** empty log message *** Revision 1.9 1999/09/30 12:21:05 sp No net access for a month... so here's one big
 * jEdit 2.1pre1 Revision 1.8 1999/06/28 09:17:20 sp Perl mode javac compile fix, text area hacking Revision 1.7
 * 1999/06/09 05:22:11 sp Find next now supports multi-file searching, minor Perl mode tweak Revision 1.6 1999/06/07
 * 06:36:32 sp Syntax `styling' (bold/italic tokens) added, plugin options dialog for plugin option panes Revision 1.5
 * 1999/06/07 03:26:33 sp Major Perl token marker updates Revision 1.4 1999/06/06 05:05:25 sp Search and replace tweaks,
 * Perl/Shell Script mode updates Revision 1.3 1999/06/05 00:22:58 sp LGPL'd syntax package Revision 1.2 1999/06/03
 * 08:24:14 sp Fixing broken CVS Revision 1.3 1999/05/31 08:11:10 sp Syntax coloring updates, expand abbrev bug fix
 * Revision 1.2 1999/05/31 04:38:51 sp Syntax optimizations, HyperSearch for Selection added (Mike Dillon) Revision 1.1
 * 1999/05/30 04:57:15 sp Perl mode started
 */
