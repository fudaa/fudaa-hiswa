
package com.memoire.jedit;

/**
 * IDL token marker.
 *
 * @author Slava Pestov
 * @author Juha Lindfors
 * @version $Id: JEditIDLTokenMarker.java,v 1.2 2006-09-19 14:35:03 deniger Exp $
 */
public class JEditIDLTokenMarker extends JEditCTokenMarker
{
	public JEditIDLTokenMarker()
	{
		super(true,getKeywords());
	}

	public static JEditKeywordMap getKeywords()
	{
		if(idlKeywords == null)
		{
			idlKeywords = new JEditKeywordMap(false);

            idlKeywords.add("any",      JEditToken.KEYWORD3);
            idlKeywords.add("attribute",JEditToken.KEYWORD1);
            idlKeywords.add("boolean",  JEditToken.KEYWORD3);
            idlKeywords.add("case",     JEditToken.KEYWORD1);
            idlKeywords.add("char",     JEditToken.KEYWORD3);
            idlKeywords.add("const",    JEditToken.KEYWORD1);
            idlKeywords.add("context",  JEditToken.KEYWORD1);
            idlKeywords.add("default",  JEditToken.KEYWORD1);
            idlKeywords.add("double",   JEditToken.KEYWORD3);
            idlKeywords.add("enum",     JEditToken.KEYWORD3);
            idlKeywords.add("exception",JEditToken.KEYWORD1);
            idlKeywords.add("FALSE",    JEditToken.LITERAL2);
            idlKeywords.add("fixed",    JEditToken.KEYWORD1);
            idlKeywords.add("float",    JEditToken.KEYWORD3);
            idlKeywords.add("in",       JEditToken.KEYWORD1);
            idlKeywords.add("inout",    JEditToken.KEYWORD1);
            idlKeywords.add("interface",JEditToken.KEYWORD1);
            idlKeywords.add("long",     JEditToken.KEYWORD3);
            idlKeywords.add("module",   JEditToken.KEYWORD1);
            idlKeywords.add("Object",   JEditToken.KEYWORD3);
            idlKeywords.add("octet",    JEditToken.KEYWORD3);
            idlKeywords.add("oneway",   JEditToken.KEYWORD1);
            idlKeywords.add("out",      JEditToken.KEYWORD1);
            idlKeywords.add("raises",   JEditToken.KEYWORD1);
            idlKeywords.add("readonly", JEditToken.KEYWORD1);
            idlKeywords.add("sequence", JEditToken.KEYWORD3);
            idlKeywords.add("short",    JEditToken.KEYWORD3);
            idlKeywords.add("string",   JEditToken.KEYWORD3);
            idlKeywords.add("struct",   JEditToken.KEYWORD3);
            idlKeywords.add("switch",   JEditToken.KEYWORD1);
            idlKeywords.add("TRUE",     JEditToken.LITERAL2);
            idlKeywords.add("typedef",  JEditToken.KEYWORD3);
            idlKeywords.add("unsigned", JEditToken.KEYWORD3);
            idlKeywords.add("union",    JEditToken.KEYWORD3);
            idlKeywords.add("void",     JEditToken.KEYWORD3);
            idlKeywords.add("wchar",    JEditToken.KEYWORD3);
            idlKeywords.add("wstring",  JEditToken.KEYWORD3);
		}
		return idlKeywords;
	}

	// private members
	private static JEditKeywordMap idlKeywords;
}

/*
 * ChangeLog:
 * $Log: not supported by cvs2svn $
 * Revision 1.1  2003/01/29 10:50:39  deniger
 * version initiale
 *
 * Revision 1.3  2001/07/25 17:39:05  desnoix
 * *** empty log message ***
 *
 * Revision 1.2  2001/06/21 18:50:04  desnoix
 * *** empty log message ***
 *
 * Revision 1.1  2001/06/13 19:48:18  desnoix
 * *** empty log message ***
 *
 * Revision 1.1  1999/10/03 03:47:16  sp
 * Minor stupidity, IDL mode
 * 
 */
