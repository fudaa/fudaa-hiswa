
package com.memoire.jedit;
import javax.swing.text.Segment;

/**
 * Makefile token marker.
 *
 * @author Slava Pestov
 * @version $Id: JEditMakefileTokenMarker.java,v 1.2 2006-09-19 14:35:04 deniger Exp $
 */
public class JEditMakefileTokenMarker extends JEditTokenMarker
{
  // public members
  public byte markTokensImpl(byte token, Segment line, int lineIndex)
  {
    char[] array = line.array;
    int offset = line.offset;
    int lastOffset = offset;
    int length = line.count + offset;
    boolean backslash = false;
loop:		for(int i = offset; i < length; i++)
    {
      int i1 = (i+1);

      char c = array[i];
      if(c == '\\')
      {
        backslash = !backslash;
        continue;
      }

      switch(token)
      {
      case JEditToken.NULL:
        switch(c)
        {
        case ':': case '=': case ' ': case '\t':
          backslash = false;
          if(lastOffset == offset)
          {
            addToken(i1 - lastOffset,JEditToken.KEYWORD1);
            lastOffset = i1;
          }
          break;
        case '#':
          if(backslash)
            backslash = false;
          else
          {
            addToken(i - lastOffset,token);
            addToken(length - i,JEditToken.COMMENT1);
            lastOffset = length;
            break loop;
          }
          break;
        case '$':
          if(backslash)
            backslash = false;
          else if(lastOffset != offset)
          {
            addToken(i - lastOffset,token);
            lastOffset = i;
            if(length - i > 1)
            {
              char c1 = array[i1];
              if(c1 == '(' || c1 == '{')
                token = JEditToken.KEYWORD2;
              else
              {
                addToken(2,JEditToken.KEYWORD2);
                lastOffset += 2;
                i++;
              }
            }
          }
          break;
        case '"':
          if(backslash)
            backslash = false;
          else
          {
            addToken(i - lastOffset,token);
            token = JEditToken.LITERAL1;
            lastOffset = i;
          }
          break;
        case '\'':
          if(backslash)
            backslash = false;
          else
          {
            addToken(i - lastOffset,token);
            token = JEditToken.LITERAL2;
            lastOffset = i;
          }
          break;
        default:
          backslash = false;
          break;
        }
      case JEditToken.KEYWORD2:
        backslash = false;
        if(c == ')' || c == '}')
        {
          addToken(i1 - lastOffset,token);
          token = JEditToken.NULL;
          lastOffset = i1;
        }
        break;
      case JEditToken.LITERAL1:
        if(backslash)
          backslash = false;
        else if(c == '"')
        {
          addToken(i1 - lastOffset,token);
          token = JEditToken.NULL;
          lastOffset = i1;
        }
        else
          backslash = false;
        break;
      case JEditToken.LITERAL2:
        if(backslash)
          backslash = false;
        else if(c == '\'')
        {
          addToken(i1 - lastOffset,JEditToken.LITERAL1);
          token = JEditToken.NULL;
          lastOffset = i1;
        }
        else
          backslash = false;
        break;
      }
    }
    switch(token)
    {
    case JEditToken.KEYWORD2:
      addToken(length - lastOffset,JEditToken.INVALID);
      token = JEditToken.NULL;
      break;
    case JEditToken.LITERAL2:
      addToken(length - lastOffset,JEditToken.LITERAL1);
      break;
    default:
      addToken(length - lastOffset,token);
      break;
    }
    return token;
  }
}
/*
 * ChangeLog:
 * $Log: not supported by cvs2svn $
 * Revision 1.1  2003/01/29 10:50:39  deniger
 * version initiale
 *
 * Revision 1.4  2002/05/22 16:44:39  deniger
 * correction erreurs de jedit
 *
 * Revision 1.3  2001/07/25 17:39:05  desnoix
 * *** empty log message ***
 *
 * Revision 1.2  2001/06/21 18:50:04  desnoix
 * *** empty log message ***
 *
 * Revision 1.1  2001/06/13 19:48:18  desnoix
 * *** empty log message ***
 *
 * Revision 1.17  1999/06/20 02:15:45  sp
 * Syntax coloring optimizations
 *
 * Revision 1.16  1999/06/05 00:22:58  sp
 * LGPL'd syntax package
 *
 * Revision 1.15  1999/06/03 08:24:14  sp
 * Fixing broken CVS
 *
 * Revision 1.16  1999/05/31 08:11:10  sp
 * Syntax coloring updates, expand abbrev bug fix
 *
 * Revision 1.15  1999/05/31 04:38:51  sp
 * Syntax optimizations, HyperSearch for Selection added (Mike Dillon)
 *
 * Revision 1.14  1999/05/22 08:33:53  sp
 * FAQ updates, mode selection tweak, patch mode update, javadoc updates, JDK 1.1.8 fix
 *
 * Revision 1.13  1999/04/22 06:03:26  sp
 * Syntax colorizing change
 *
 * Revision 1.12  1999/04/19 05:38:20  sp
 * Syntax API changes
 *
 * Revision 1.11  1999/03/13 00:09:07  sp
 * Console updates, uncomment removed cos it's too buggy, cvs log tags added
 *
 * Revision 1.10  1999/03/12 23:51:00  sp
 * Console updates, uncomment removed cos it's too buggy, cvs log tags added
 *
 */
