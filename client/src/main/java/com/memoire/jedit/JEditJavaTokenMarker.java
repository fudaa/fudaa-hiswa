
package com.memoire.jedit;

/**
 * Java token marker.
 *
 * @author Slava Pestov
 * @version $Id: JEditJavaTokenMarker.java,v 1.2 2006-09-19 14:35:03 deniger Exp $
 */
public class JEditJavaTokenMarker extends JEditCTokenMarker
{
	public JEditJavaTokenMarker()
	{
		super(false,getKeywords());
	}

	public static JEditKeywordMap getKeywords()
	{
		if(javaKeywords == null)
		{
			javaKeywords = new JEditKeywordMap(false);
			javaKeywords.add("package",JEditToken.KEYWORD2);
			javaKeywords.add("import",JEditToken.KEYWORD2);
			javaKeywords.add("byte",JEditToken.KEYWORD3);
			javaKeywords.add("char",JEditToken.KEYWORD3);
			javaKeywords.add("short",JEditToken.KEYWORD3);
			javaKeywords.add("int",JEditToken.KEYWORD3);
			javaKeywords.add("long",JEditToken.KEYWORD3);
			javaKeywords.add("float",JEditToken.KEYWORD3);
			javaKeywords.add("double",JEditToken.KEYWORD3);
			javaKeywords.add("boolean",JEditToken.KEYWORD3);
			javaKeywords.add("void",JEditToken.KEYWORD3);
			javaKeywords.add("class",JEditToken.KEYWORD3);
			javaKeywords.add("interface",JEditToken.KEYWORD3);
			javaKeywords.add("abstract",JEditToken.KEYWORD1);
			javaKeywords.add("final",JEditToken.KEYWORD1);
			javaKeywords.add("private",JEditToken.KEYWORD1);
			javaKeywords.add("protected",JEditToken.KEYWORD1);
			javaKeywords.add("public",JEditToken.KEYWORD1);
			javaKeywords.add("static",JEditToken.KEYWORD1);
			javaKeywords.add("synchronized",JEditToken.KEYWORD1);
			javaKeywords.add("native",JEditToken.KEYWORD1);
			javaKeywords.add("volatile",JEditToken.KEYWORD1);
			javaKeywords.add("transient",JEditToken.KEYWORD1);
			javaKeywords.add("break",JEditToken.KEYWORD1);
			javaKeywords.add("case",JEditToken.KEYWORD1);
			javaKeywords.add("continue",JEditToken.KEYWORD1);
			javaKeywords.add("default",JEditToken.KEYWORD1);
			javaKeywords.add("do",JEditToken.KEYWORD1);
			javaKeywords.add("else",JEditToken.KEYWORD1);
			javaKeywords.add("for",JEditToken.KEYWORD1);
			javaKeywords.add("if",JEditToken.KEYWORD1);
			javaKeywords.add("instanceof",JEditToken.KEYWORD1);
			javaKeywords.add("new",JEditToken.KEYWORD1);
			javaKeywords.add("return",JEditToken.KEYWORD1);
			javaKeywords.add("switch",JEditToken.KEYWORD1);
			javaKeywords.add("while",JEditToken.KEYWORD1);
			javaKeywords.add("throw",JEditToken.KEYWORD1);
			javaKeywords.add("try",JEditToken.KEYWORD1);
			javaKeywords.add("catch",JEditToken.KEYWORD1);
			javaKeywords.add("extends",JEditToken.KEYWORD1);
			javaKeywords.add("finally",JEditToken.KEYWORD1);
			javaKeywords.add("implements",JEditToken.KEYWORD1);
			javaKeywords.add("throws",JEditToken.KEYWORD1);
			javaKeywords.add("this",JEditToken.LITERAL2);
			javaKeywords.add("null",JEditToken.LITERAL2);
			javaKeywords.add("super",JEditToken.LITERAL2);
			javaKeywords.add("true",JEditToken.LITERAL2);
			javaKeywords.add("false",JEditToken.LITERAL2);
		}
		return javaKeywords;
	}

	// private members
	private static JEditKeywordMap javaKeywords;
}

/*
 * ChangeLog:
 * $Log: not supported by cvs2svn $
 * Revision 1.1  2003/01/29 10:50:39  deniger
 * version initiale
 *
 * Revision 1.3  2001/07/25 17:39:05  desnoix
 * *** empty log message ***
 *
 * Revision 1.2  2001/06/21 18:50:04  desnoix
 * *** empty log message ***
 *
 * Revision 1.1  2001/06/13 19:48:18  desnoix
 * *** empty log message ***
 *
 * Revision 1.4  1999/11/09 10:14:34  sp
 * Macro code cleanups, menu item and tool bar clicks are recorded now, delete
 * word commands, check box menu item support
 *
 * Revision 1.3  1999/06/05 00:22:58  sp
 * LGPL'd syntax package
 *
 * Revision 1.2  1999/04/22 06:03:26  sp
 * Syntax colorizing change
 *
 * Revision 1.1  1999/03/13 09:11:46  sp
 * Syntax code updates, code cleanups
 *
 */
