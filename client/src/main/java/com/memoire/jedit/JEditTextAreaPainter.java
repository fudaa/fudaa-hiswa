
package com.memoire.jedit;

/*
 * JEditTextAreaPainter.java - Paints the text area
 * Copyright (C) 1999 Slava Pestov
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 */



import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;
import javax.swing.ToolTipManager;
import javax.swing.text.PlainDocument;
import javax.swing.text.Segment;
import javax.swing.text.TabExpander;
import javax.swing.text.Utilities;



/**
 * The text area repaint manager. It performs double buffering and paints
 * lines of text.
 * @author Slava Pestov
 * @version $Id: JEditTextAreaPainter.java,v 1.2 2006-09-19 14:35:04 deniger Exp $
 */
public class JEditTextAreaPainter extends JComponent implements TabExpander
{
  /**
   * Creates a new repaint manager. This should be not be called
   * directly.
   */
  public JEditTextAreaPainter(JEditTextArea _textArea, JEditTextAreaDefaults defaults)
  {
    this.textArea = _textArea;

    setAutoscrolls(true);
    setDoubleBuffered(true);
    setOpaque(true);

    ToolTipManager.sharedInstance().registerComponent(this);

    currentLine = new Segment();
    currentLineIndex = -1;

    setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));

    setFont(new Font("Monospaced",Font.PLAIN,14));
    setForeground(Color.black);
    setBackground(Color.white);

    blockCaret = defaults.blockCaret;
    styles = defaults.styles;
    cols = defaults.cols;
    rows = defaults.rows;
    caretColor = defaults.caretColor;
    selectionColor = defaults.selectionColor;
    lineHighlightColor = defaults.lineHighlightColor;
    lineHighlight = defaults.lineHighlight;
    bracketHighlightColor = defaults.bracketHighlightColor;
    bracketHighlight = defaults.bracketHighlight;
    eolMarkerColor = defaults.eolMarkerColor;
    eolMarkers = defaults.eolMarkers;

    eolMarkerChar=""+(char)171; // &=38 �=176 P=182 <<=171
    eosMarkerChar=""+(char)216; // �=176 ~=126 O/=216
  }

  /**
   * Returns if this component can be traversed by pressing the
   * Tab key. This returns false.
   */
  public final boolean isManagingFocus()
  {
    return false;
  }

  /**
   * Returns the syntax styles used to paint colorized text. Entry <i>n</i>
   * will be used to paint tokens with id = <i>n</i>.
   * @see org.gjt.sp.jedit.syntax.JEditToken
   */
  public final JEditSyntaxStyle[] getStyles()
  {
    return styles;
  }

  /**
   * Sets the syntax styles used to paint colorized text. Entry <i>n</i>
   * will be used to paint tokens with id = <i>n</i>.
   * @param _styles The syntax styles
   * @see org.gjt.sp.jedit.syntax.JEditToken
   */
  public final void setStyles(JEditSyntaxStyle[] _styles)
  {
    this.styles = _styles;
    repaint();
  }

  /**
   * Returns the caret color.
   */
  public final Color getCaretColor()
  {
    return caretColor;
  }

  /**
   * Sets the caret color.
   * @param _caretColor The caret color
   */
  public final void setCaretColor(Color _caretColor)
  {
    this.caretColor = _caretColor;
    invalidateSelectedLines();
  }

  /**
   * Returns the selection color.
   */
  public final Color getSelectionColor()
  {
    return selectionColor;
  }

  /**
   * Sets the selection color.
   * @param _selectionColor The selection color
   */
  public final void setSelectionColor(Color _selectionColor)
  {
    this.selectionColor = _selectionColor;
    invalidateSelectedLines();
  }

  /**
   * Returns the line highlight color.
   */
  public final Color getLineHighlightColor()
  {
    return lineHighlightColor;
  }

  /**
   * Sets the line highlight color.
   * @param _lineHighlightColor The line highlight color
   */
  public final void setLineHighlightColor(Color _lineHighlightColor)
  {
    this.lineHighlightColor = _lineHighlightColor;
    invalidateSelectedLines();
  }

  /**
   * Returns true if line highlight is enabled, false otherwise.
   */
  public final boolean isLineHighlightEnabled()
  {
    return lineHighlight;
  }

  /**
   * Enables or disables current line highlighting.
   * @param _lineHighlight True if current line highlight should be enabled,
   * false otherwise
   */
  public final void setLineHighlightEnabled(boolean _lineHighlight)
  {
    this.lineHighlight = _lineHighlight;
    invalidateSelectedLines();
  }

  /**
   * Returns the bracket highlight color.
   */
  public final Color getBracketHighlightColor()
  {
    return bracketHighlightColor;
  }

  /**
   * Sets the bracket highlight color.
   * @param _bracketHighlightColor The bracket highlight color
   */
  public final void setBracketHighlightColor(Color _bracketHighlightColor)
  {
    this.bracketHighlightColor = _bracketHighlightColor;
    invalidateLine(textArea.getBracketLine());
  }

  /**
   * Returns true if bracket highlighting is enabled, false otherwise.
   * When bracket highlighting is enabled, the bracket matching the
   * one before the caret (if any) is highlighted.
   */
  public final boolean isBracketHighlightEnabled()
  {
    return bracketHighlight;
  }

  /**
   * Enables or disables bracket highlighting.
   * When bracket highlighting is enabled, the bracket matching the
   * one before the caret (if any) is highlighted.
   * @param _bracketHighlight True if bracket highlighting should be
   * enabled, false otherwise
   */
  public final void setBracketHighlightEnabled(boolean _bracketHighlight)
  {
    this.bracketHighlight = _bracketHighlight;
    invalidateLine(textArea.getBracketLine());
  }

  /**
   * Returns true if the caret should be drawn as a block, false otherwise.
   */
  public final boolean isBlockCaretEnabled()
  {
    return blockCaret;
  }

  /**
   * Sets if the caret should be drawn as a block, false otherwise.
   * @param _blockCaret True if the caret should be drawn as a block,
   * false otherwise.
   */
  public final void setBlockCaretEnabled(boolean _blockCaret)
  {
    this.blockCaret = _blockCaret;
    invalidateSelectedLines();
  }

  /**
   * Returns the EOL marker color.
   */
  public final Color getEOLMarkerColor()
  {
    return eolMarkerColor;
  }

  /**
   * Sets the EOL marker color.
   * @param _eolMarkerColor The EOL marker color
   */
  public final void setEOLMarkerColor(Color _eolMarkerColor)
  {
    this.eolMarkerColor = _eolMarkerColor;
    repaint();
  }

  /**
   * Returns true if EOL markers are drawn, false otherwise.
   */
  public final boolean isEOLMarkerEnabled()
  {
    return eolMarkers;
  }

  /**
   * Sets if EOL markers are to be drawn.
   * @param _eolMarkers True if EOL markers should be dranw, false otherwise
   */
  public final void setEOLMarkerEnabled(boolean _eolMarkers)
  {
    this.eolMarkers = _eolMarkers;
    repaint();
  }

  /**
   * Adds a custom highlight painter.
   * @param highlight The highlight
   */
  public void addCustomHighlight(Highlight highlight)
  {
    highlight.init(textArea,highlights);
    highlights = highlight;
  }

  /**
   * Highlight interface.
   */
  public interface Highlight
  {
    /**
     * Called after the highlight painter has been added.
     * @param textArea The text area
     * @param next The painter this one should delegate to
     */
    void init(JEditTextArea textArea, Highlight next);

    /**
     * This should paint the highlight and delgate to the
     * next highlight painter.
     * @param gfx The graphics context
     * @param line The line number
     * @param y The y co-ordinate of the line
     */
    void paintHighlight(Graphics gfx, int line, int y);

    /**
     * Returns the tool tip to display at the specified
     * location. If this highlighter doesn't know what to
     * display, it should delegate to the next highlight
     * painter.
     * @param evt The mouse event
     */
    String getToolTipText(MouseEvent evt);
  }

  /**
   * Returns the tool tip to display at the specified location.
   * @param evt The mouse event
   */
  public String getToolTipText(MouseEvent evt)
  {
    if(highlights != null)
      return highlights.getToolTipText(evt);
      return null;
  }

  /**
   * Returns the font metrics used by this component.
   */
  public FontMetrics getFontMetrics()
  {
    return fm;
  }

  /**
   * Sets the font for this component. This is overridden to update the
   * cached font metrics and to recalculate which lines are visible.
   * @param font The font
   */
  public void setFont(Font font)
  {
    super.setFont(font);
    fm = textArea./*getToolkit().*/getFontMetrics(font);
    textArea.recalculateVisibleLines();
  }

  /**
   * Repaints the text.
   * @param g The graphics context
   */
  public void paint(Graphics gfx)
  {
    tabSize = fm.charWidth('w') * ((Integer)textArea
      .getDocument().getProperty(
      PlainDocument.tabSizeAttribute)).intValue();

    Rectangle clipRect = gfx.getClipBounds();

    gfx.setColor(getBackground());
    gfx.fillRect(clipRect.x,clipRect.y,clipRect.width,clipRect.height);

    // We don't use yToLine() here because that method doesn't
    // return lines past the end of the document
    int height = fm.getHeight();
    int firstLine = textArea.getFirstLine();
    int firstInvalid = firstLine + clipRect.y / height;
    // Because the clipRect's height is usually an even multiple
    // of the font height, we subtract 1 from it, otherwise one
    // too many lines will always be painted.
    int lastInvalid = firstLine + (clipRect.y + clipRect.height - 1) / height;

    try
    {
      JEditTokenMarker tokenMarker = textArea.getDocument()
        .getTokenMarker();
      int x = textArea.getHorizontalOffset();

      for(int line = firstInvalid; line <= lastInvalid; line++)
      {
        paintLine(gfx,tokenMarker,line,x);
      }

      if(tokenMarker != null && tokenMarker.isNextLineRequested())
      {
        int h = clipRect.y + clipRect.height;
        repaint(0,h,getWidth(),getHeight() - h);
      }
    }
    catch(Exception e)
    {
      System.err.println("Error repainting line"
        + " range {" + firstInvalid + ","
        + lastInvalid + "}:");
      e.printStackTrace();
    }
  }

  /**
   * Marks a line as needing a repaint.
   * @param line The line to invalidate
   */
  public final void invalidateLine(int line)
  {
    repaint(0,textArea.lineToY(line) + fm.getMaxDescent() + fm.getLeading(),
      getWidth(),fm.getHeight());
  }

  /**
   * Marks a range of lines as needing a repaint.
   * @param firstLine The first line to invalidate
   * @param lastLine The last line to invalidate
   */
  public final void invalidateLineRange(int firstLine, int lastLine)
  {
    repaint(0,textArea.lineToY(firstLine) + fm.getMaxDescent() + fm.getLeading(),
      getWidth(),(lastLine - firstLine + 1) * fm.getHeight());
  }

  /**
   * Repaints the lines containing the selection.
   */
  public final void invalidateSelectedLines()
  {
    invalidateLineRange(textArea.getSelectionStartLine(),
      textArea.getSelectionEndLine());
  }

  /**
   * Implementation of TabExpander interface. Returns next tab stop after
   * a specified point.
   * @param x The x co-ordinate
   * @param tabOffset Ignored
   * @return The next tab stop after <i>x</i>
   */
  public float nextTabStop(float x, int tabOffset)
  {
    int offset = textArea.getHorizontalOffset();
    int ntabs = ((int)x - offset) / tabSize;
    return (ntabs + 1) * tabSize + offset;
  }

  /**
   * Returns the painter's preferred size.
   */
  public Dimension getPreferredSize()
  {
    Dimension dim = new Dimension();
    dim.width = fm.charWidth('w') * cols;
    dim.height = fm.getHeight() * rows;
    return dim;
  }


  /**
   * Returns the painter's minimum size.
   */
  public Dimension getMinimumSize()
  {
    return getPreferredSize();
  }

  // package-private members
  int currentLineIndex;
  JEditToken currentLineJEditTokens;
  Segment currentLine;

  // protected members
  protected JEditTextArea textArea;

  protected JEditSyntaxStyle[] styles;
  protected Color caretColor;
  protected Color selectionColor;
  protected Color lineHighlightColor;
  protected Color bracketHighlightColor;
  protected Color eolMarkerColor;

  protected boolean blockCaret;
  protected boolean lineHighlight;
  protected boolean bracketHighlight;
  protected boolean eolMarkers;
        protected String  eolMarkerChar;
        protected String  eosMarkerChar;
  protected int cols;
  protected int rows;

  protected int tabSize;
  protected FontMetrics fm;

  protected Highlight highlights;

  protected void paintLine(Graphics gfx, JEditTokenMarker tokenMarker,
    int line, int x)
  {
    Font defaultFont = getFont();
    Color defaultColor = getForeground();

    currentLineIndex = line;
    int y = textArea.lineToY(line);

    if(line < 0 || line >= textArea.getLineCount())
    {
            // styles[JEditToken.INVALID].setGraphicsFlags(gfx,defaultFont);
            gfx.setFont(defaultFont);
      gfx.setColor(eolMarkerColor);
      gfx.drawString(eosMarkerChar,0,y + fm.getHeight());
    }
    else if(tokenMarker == null)
    {
      paintPlainLine(gfx,line,defaultFont,defaultColor,x,y);
    }
    else
    {
      paintSyntaxLine(gfx,tokenMarker,line,defaultFont,
        defaultColor,x,y);
    }
  }

  protected void paintPlainLine(Graphics gfx, int line, Font defaultFont,
    Color defaultColor, int x, int y)
  {
    paintHighlight(gfx,line,y);
    textArea.getLineText(line,currentLine);

    gfx.setFont(defaultFont);
    gfx.setColor(defaultColor);

    y += fm.getHeight();
    x = Utilities.drawTabbedText(currentLine,x,y,gfx,this,0);

    if(eolMarkers)
    {
            gfx.setFont(defaultFont);
      gfx.setColor(eolMarkerColor);
      gfx.drawString(eolMarkerChar,x,y);
    }
  }

  protected void paintSyntaxLine(Graphics gfx, JEditTokenMarker tokenMarker,
    int line, Font defaultFont, Color defaultColor, int x, int y)
  {
    textArea.getLineText(currentLineIndex,currentLine);
    currentLineJEditTokens = tokenMarker.markTokens(currentLine,
      currentLineIndex);

    paintHighlight(gfx,line,y);

    gfx.setFont(defaultFont);
    gfx.setColor(defaultColor);
    y += fm.getHeight();
    x = JEditSyntaxUtilities.paintSyntaxLine(currentLine,
      currentLineJEditTokens,styles,this,gfx,x,y);

    if(eolMarkers)
    {
            gfx.setFont(defaultFont);
      gfx.setColor(eolMarkerColor);
      gfx.drawString(eolMarkerChar,x,y);
    }
  }

  protected void paintHighlight(Graphics gfx, int line, int y)
  {
    if(line >= textArea.getSelectionStartLine()
      && line <= textArea.getSelectionEndLine())
      paintLineHighlight(gfx,line,y);

    if(bracketHighlight && line == textArea.getBracketLine())
      paintBracketHighlight(gfx,line,y);

    if(line == textArea.getCaretLine())
      paintCaret(gfx,line,y);

    if(highlights != null)
      highlights.paintHighlight(gfx,line,y);
  }

  protected void paintLineHighlight(Graphics gfx, int line, int y)
  {
    int height = fm.getHeight();
    y += fm.getLeading() + fm.getMaxDescent();

    int selectionStart = textArea.getSelectionStart();
    int selectionEnd = textArea.getSelectionEnd();

    if(selectionStart == selectionEnd)
    {
      if(lineHighlight)
      {
        gfx.setColor(lineHighlightColor);
        gfx.fillRect(0,y,getWidth(),height);
      }
    }
    else
    {
      gfx.setColor(selectionColor);

      int selectionStartLine = textArea.getSelectionStartLine();
      int selectionEndLine = textArea.getSelectionEndLine();
      int lineStart = textArea.getLineStartOffset(line);

      int x1, x2;
      if(textArea.isSelectionRectangular())
      {
        int lineLen = textArea.getLineLength(line);
        x1 = textArea._offsetToX(line,Math.min(lineLen,
          selectionStart - textArea.getLineStartOffset(
          selectionStartLine)));
        x2 = textArea._offsetToX(line,Math.min(lineLen,
          selectionEnd - textArea.getLineStartOffset(
          selectionEndLine)));
        if(x1 == x2)
          x2++;
      }
      else if(selectionStartLine == selectionEndLine)
      {
        x1 = textArea._offsetToX(line,
          selectionStart - lineStart);
        x2 = textArea._offsetToX(line,
          selectionEnd - lineStart);
      }
      else if(line == selectionStartLine)
      {
        x1 = textArea._offsetToX(line,
          selectionStart - lineStart);
        x2 = getWidth();
      }
      else if(line == selectionEndLine)
      {
        x1 = 0;
        x2 = textArea._offsetToX(line,
          selectionEnd - lineStart);
      }
      else
      {
        x1 = 0;
        x2 = getWidth();
      }

      // "inlined" min/max()
      gfx.fillRect(x1 > x2 ? x2 : x1,y,x1 > x2 ?
        (x1 - x2) : (x2 - x1),height);
    }

  }

  protected void paintBracketHighlight(Graphics gfx, int line, int y)
  {
    int position = textArea.getBracketPosition();
    if(position == -1)
      return;
    y += fm.getLeading() + fm.getMaxDescent();
    int x = textArea._offsetToX(line,position);
    gfx.setColor(bracketHighlightColor);
    // Hack!!! Since there is no fast way to get the character
    // from the bracket matching routine, we use ( since all
    // brackets probably have the same width anyway
    // gfx.drawRect(x,y,fm.charWidth('(')-1,fm.getHeight()-1);

    gfx.fillRect(x,y-1,fm.charWidth('(')+2,fm.getHeight()+1);
  }

  protected void paintCaret(Graphics gfx, int line, int y)
  {
    if(textArea.isCaretVisible())
    {
      int offset = textArea.getCaretPosition()
        - textArea.getLineStartOffset(line);
      int caretX = textArea._offsetToX(line,offset);
      int caretWidth = ((blockCaret ||
        textArea.isOverwriteEnabled()) ?
        fm.charWidth('w') : 1);
      y += fm.getLeading() + fm.getMaxDescent();
      int height = fm.getHeight();

      gfx.setColor(caretColor);

      if(textArea.isOverwriteEnabled())
      {
        gfx.fillRect(caretX,y + height - 1,
          caretWidth,1);
      }
      else
      {
        if(textArea.hasFocus()) gfx.fillRect(caretX,y,caretWidth,height);
        else                    gfx.drawRect(caretX,y,caretWidth-1,height-1);
        // gfx.fillRect(caretX,y,caretWidth-1,height-1);
      }
    }
  }
}

/*
 * ChangeLog:
 * $Log: not supported by cvs2svn $
 * Revision 1.1  2003/01/29 10:50:39  deniger
 * version initiale
 *
 * Revision 1.4  2002/05/22 16:44:39  deniger
 * correction erreurs de jedit
 *
 * Revision 1.3  2001/07/25 17:39:05  desnoix
 * *** empty log message ***
 *
 * Revision 1.2  2001/06/21 18:50:04  desnoix
 * *** empty log message ***
 *
 * Revision 1.1  2001/06/13 19:48:18  desnoix
 * *** empty log message ***
 *
 * Revision 1.17  1999/11/06 02:06:50  sp
 * Logging updates, bug fixing, icons, various other stuff
 *
 * Revision 1.16  1999/10/06 08:39:46  sp
 * Fixes to repeating and macro features
 *
 * Revision 1.15  1999/09/30 12:21:05  sp
 * No net access for a month... so here's one big jEdit 2.1pre1
 *
 * Revision 1.14  1999/08/21 01:48:18  sp
 * jEdit 2.0pre8
 *
 * Revision 1.13  1999/07/29 08:50:21  sp
 * Misc stuff for 1.7pre7
 *
 * Revision 1.12  1999/07/16 23:45:49  sp
 * 1.7pre6 BugFree version
 *
 * Revision 1.11  1999/07/08 06:06:04  sp
 * Bug fixes and miscallaneous updates
 *
 * Revision 1.10  1999/07/05 04:38:40  sp
 * Massive batch of changes... bug fixes, also new text component is in place.
 * Have fun
 *
 * Revision 1.9  1999/06/30 07:08:02  sp
 * Text area bug fixes
 *
 * Revision 1.8  1999/06/30 05:01:55  sp
 * Lots of text area bug fixes and optimizations
 *
 * Revision 1.7  1999/06/29 09:01:24  sp
 * Text area now does bracket matching, eol markers, also xToOffset() and
 * offsetToX() now work better
 *
 * Revision 1.6  1999/06/28 09:17:20  sp
 * Perl mode javac compile fix, text area hacking
 *
 * Revision 1.5  1999/06/27 04:53:16  sp
 * Text selection implemented in text area, assorted bug fixes
 *
 * Revision 1.4  1999/06/25 06:54:08  sp
 * Text area updates
 *
 */
