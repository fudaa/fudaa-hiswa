
package com.memoire.jedit;
import javax.swing.text.Segment;

/**
 * Shell script token marker.
 *
 * @author Slava Pestov
 * @version $Id: JEditShellScriptTokenMarker.java,v 1.2 2006-09-19 14:35:03 deniger Exp $
 */
public class JEditShellScriptTokenMarker extends JEditTokenMarker
{
  // public members
  public static final byte LVARIABLE = JEditToken.INTERNAL_FIRST;

  public byte markTokensImpl(byte token, Segment line, int lineIndex)
  {
    char[] array = line.array;
    byte cmdState = 0; // 0 = space before command, 1 = inside
        // command, 2 = after command
    int offset = line.offset;
    int lastOffset = offset;
    int length = line.count + offset;

    if(token == JEditToken.LITERAL1 && lineIndex != 0
      && lineInfo[lineIndex - 1].obj != null)
    {
      String str = (String)lineInfo[lineIndex - 1].obj;
      if(str != null && str.length() == line.count
        && JEditSyntaxUtilities.regionMatches(false,line,
        offset,str))
      {
        addToken(line.count,JEditToken.LITERAL1);
        return JEditToken.NULL;
      }
      addToken(line.count,JEditToken.LITERAL1);
      lineInfo[lineIndex].obj = str;
      return JEditToken.LITERAL1;
    }

    boolean backslash = false;
loop:		for(int i = offset; i < length; i++)
    {
      int i1 = (i+1);

      char c = array[i];

      if(c == '\\')
      {
        backslash = !backslash;
        continue;
      }

      switch(token)
      {
      case JEditToken.NULL:
        switch(c)
        {
        case ' ': case '\t': case '(': case ')':
          backslash = false;
          if(cmdState == 1/*insideCmd*/)
          {
            addToken(i - lastOffset,JEditToken.KEYWORD1);
            lastOffset = i;
            cmdState = 2; /*afterCmd*/
          }
          break;
        case '=':
          backslash = false;
          if(cmdState == 1/*insideCmd*/)
          {
            addToken(i - lastOffset,token);
            lastOffset = i;
            cmdState = 2; /*afterCmd*/
          }
          break;
        case '&': case '|': case ';':
          if(backslash)
            backslash = false;
          else
            cmdState = 0; /*beforeCmd*/
          break;
        case '#':
          if(backslash)
            backslash = false;
          else
          {
            addToken(i - lastOffset,token);
            addToken(length - i,JEditToken.COMMENT1);
            lastOffset = length;
            break loop;
          }
          break;
        case '$':
          if(backslash)
            backslash = false;
          else
          {
            addToken(i - lastOffset,token);
            cmdState = 2; /*afterCmd*/
            lastOffset = i;
            if(length - i >= 2)
            {
              switch(array[i1])
              {
              case '(':
                continue;
              case '{':
                token = LVARIABLE;
                break;
              default:
                token = JEditToken.KEYWORD2;
                break;
              }
            }
            else
              token = JEditToken.KEYWORD2;
          }
          break;
        case '"':
          if(backslash)
            backslash = false;
          else
          {
            addToken(i - lastOffset,token);
            token = JEditToken.LITERAL1;
            lineInfo[lineIndex].obj = null;
            cmdState = 2; /*afterCmd*/
            lastOffset = i;
          }
          break;
        case '\'':
          if(backslash)
            backslash = false;
          else
          {
            addToken(i - lastOffset,token);
            token = JEditToken.LITERAL2;
            cmdState = 2; /*afterCmd*/
            lastOffset = i;
          }
          break;
        case '<':
          if(backslash)
            backslash = false;
          else
          {
            if(length - i > 1 && array[i1] == '<')
            {
              addToken(i - lastOffset,
                token);
              token = JEditToken.LITERAL1;
              lastOffset = i;
              lineInfo[lineIndex].obj =
                new String(array,i + 2,
                  length - (i+2));
            }
          }
          break;
        default:
          backslash = false;
          if(Character.isLetter(c))
          {
            if(cmdState == 0 /*beforeCmd*/)
            {
              addToken(i - lastOffset,token);
              lastOffset = i;
              cmdState++; /*insideCmd*/
            }
          }
          break;
        }
        break;
      case JEditToken.KEYWORD2:
        backslash = false;
        if(!Character.isLetterOrDigit(c) && c != '_')
        {
          if(i != offset && array[i-1] == '$')
          {
            addToken(i1 - lastOffset,token);
            lastOffset = i1;
            token = JEditToken.NULL;
            continue;
          }
          addToken(i - lastOffset,token);
          lastOffset = i;
          token = JEditToken.NULL;
        }
        break;
      case JEditToken.LITERAL1:
        if(backslash)
          backslash = false;
        else if(c == '"')
        {
          addToken(i1 - lastOffset,token);
          cmdState = 2; /*afterCmd*/
          lastOffset = i1;
          token = JEditToken.NULL;
        }
        else
          backslash = false;
        break;
      case JEditToken.LITERAL2:
        if(backslash)
          backslash = false;
        else if(c == '\'')
        {
          addToken(i1 - lastOffset,JEditToken.LITERAL1);
          cmdState = 2; /*afterCmd*/
          lastOffset = i1;
          token = JEditToken.NULL;
        }
        else
          backslash = false;
        break;
      case LVARIABLE:
        backslash = false;
        if(c == '}')
        {
          addToken(i1 - lastOffset,JEditToken.KEYWORD2);
          lastOffset = i1;
          token = JEditToken.NULL;
        }
        break;
      default:
        throw new InternalError("Invalid state: " + token);
      }
    }

    switch(token)
    {
    case JEditToken.NULL:
      if(cmdState == 1)
        addToken(length - lastOffset,JEditToken.KEYWORD1);
      else
        addToken(length - lastOffset,token);
      break;
    case JEditToken.LITERAL2:
      addToken(length - lastOffset,JEditToken.LITERAL1);
      break;
    case JEditToken.KEYWORD2:
      addToken(length - lastOffset,token);
      token = JEditToken.NULL;
      break;
    case LVARIABLE:
      addToken(length - lastOffset,JEditToken.INVALID);
      token = JEditToken.NULL;
      break;
    default:
      addToken(length - lastOffset,token);
      break;
    }
    return token;
  }
}

/*
 * ChangeLog:
 * $Log: not supported by cvs2svn $
 * Revision 1.1  2003/01/29 10:50:39  deniger
 * version initiale
 *
 * Revision 1.4  2002/05/22 16:44:39  deniger
 * correction erreurs de jedit
 *
 * Revision 1.3  2001/07/25 17:39:05  desnoix
 * *** empty log message ***
 *
 * Revision 1.2  2001/06/21 18:50:04  desnoix
 * *** empty log message ***
 *
 * Revision 1.1  2001/06/13 19:48:18  desnoix
 * *** empty log message ***
 *
 * Revision 1.17  1999/06/20 02:15:45  sp
 * Syntax coloring optimizations
 *
 * Revision 1.16  1999/06/06 05:05:25  sp
 * Search and replace tweaks, Perl/Shell Script mode updates
 *
 * Revision 1.15  1999/06/05 00:22:58  sp
 * LGPL'd syntax package
 *
 * Revision 1.14  1999/06/03 08:24:14  sp
 * Fixing broken CVS
 *
 * Revision 1.15  1999/05/31 08:11:10  sp
 * Syntax coloring updates, expand abbrev bug fix
 *
 * Revision 1.14  1999/05/31 04:38:51  sp
 * Syntax optimizations, HyperSearch for Selection added (Mike Dillon)
 *
 * Revision 1.13  1999/05/30 04:57:15  sp
 * Perl mode started
 *
 * Revision 1.12  1999/05/03 04:28:01  sp
 * Syntax colorizing bug fixing, console bug fix for Swing 1.1.1
 *
 * Revision 1.11  1999/04/27 06:53:38  sp
 * JARClassLoader updates, shell script token marker update, token marker compiles
 * now
 *
 * Revision 1.10  1999/04/22 06:03:26  sp
 * Syntax colorizing change
 *
 * Revision 1.9  1999/04/19 05:38:20  sp
 * Syntax API changes
 *
 * Revision 1.8  1999/03/12 23:51:00  sp
 * Console updates, uncomment removed cos it's too buggy, cvs log tags added
 *
 */
