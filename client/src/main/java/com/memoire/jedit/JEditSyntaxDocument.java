
package com.memoire.jedit;
import javax.swing.event.DocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.PlainDocument;
import javax.swing.text.Segment;
import javax.swing.undo.UndoableEdit;

/**
 * A document implementation that can be tokenized by the syntax highlighting
 * system.
 *
 * @author Slava Pestov
 * @version $Id: JEditSyntaxDocument.java,v 1.2 2006-09-19 14:35:03 deniger Exp $
 */
public class JEditSyntaxDocument extends PlainDocument
{
  /**
   * Returns the token marker that is to be used to split lines
   * of this document up into tokens. May return null if this
   * document is not to be colorized.
   */
  public JEditTokenMarker getTokenMarker()
  {
    return tokenMarker;
  }

  /**
   * Sets the token marker that is to be used to split lines of
   * this document up into tokens. May throw an exception if
   * this is not supported for this type of document.
   * @param tm The new token marker
   */
  public void setTokenMarker(JEditTokenMarker tm)
  {
    tokenMarker = tm;
    if(tm == null)
      return;
    tokenMarker.insertLines(0,getDefaultRootElement()
      .getElementCount());
    tokenizeLines();
  }

  /**
   * Reparses the document, by passing all lines to the token
   * marker. This should be called after the document is first
   * loaded.
   */
  public void tokenizeLines()
  {
    tokenizeLines(0,getDefaultRootElement().getElementCount());
  }

  /**
   * Reparses the document, by passing the specified lines to the
   * token marker. This should be called after a large quantity of
   * text is first inserted.
   * @param start The first line to parse
   * @param len The number of lines, after the first one to parse
   */
  public void tokenizeLines(int start, int len)
  {
    if(tokenMarker == null || !tokenMarker.supportsMultilineTokens())
      return;

    Segment lineSegment = new Segment();
    Element map = getDefaultRootElement();

    len += start;

    try
    {
      for(int i = start; i < len; i++)
      {
        Element lineElement = map.getElement(i);
        int lineStart = lineElement.getStartOffset();
        getText(lineStart,lineElement.getEndOffset()
          - lineStart - 1,lineSegment);
        tokenMarker.markTokens(lineSegment,i);
      }
    }
    catch(BadLocationException bl)
    {
      bl.printStackTrace();
    }
  }

  /**
   * Starts a compound edit that can be undone in one operation.
   * Subclasses that implement undo should override this method;
   * this class has no undo functionality so this method is
   * empty.
   */
  public void beginCompoundEdit() {}

  /**
   * Ends a compound edit that can be undone in one operation.
   * Subclasses that implement undo should override this method;
   * this class has no undo functionality so this method is
   * empty.
   */
  public void endCompoundEdit() {}

  /**
   * Adds an undoable edit to this document's undo list. The edit
   * should be ignored if something is currently being undone.
   * @param edit The undoable edit
   *
   * @since jEdit 2.2pre1
   */
  public void addUndoableEdit(UndoableEdit edit) {}

  // protected members
  protected JEditTokenMarker tokenMarker;

  /**
   * We overwrite this method to update the token marker
   * state immediately so that any event listeners get a
   * consistent token marker.
   */
  protected void fireInsertUpdate(DocumentEvent evt)
  {
    if(tokenMarker != null)
    {
      DocumentEvent.ElementChange ch = evt.getChange(
        getDefaultRootElement());
      if(ch != null)
      {
        tokenMarker.insertLines(ch.getIndex() + 1,
          ch.getChildrenAdded().length -
          ch.getChildrenRemoved().length);
      }
    }

    super.fireInsertUpdate(evt);
  }

  /**
   * We overwrite this method to update the token marker
   * state immediately so that any event listeners get a
   * consistent token marker.
   */
  protected void fireRemoveUpdate(DocumentEvent evt)
  {
    if(tokenMarker != null)
    {
      DocumentEvent.ElementChange ch = evt.getChange(
        getDefaultRootElement());
      if(ch != null)
      {
        tokenMarker.deleteLines(ch.getIndex() + 1,
          ch.getChildrenRemoved().length -
          ch.getChildrenAdded().length);
      }
    }

    super.fireRemoveUpdate(evt);
  }
}

/*
 * ChangeLog:
 * $Log: not supported by cvs2svn $
 * Revision 1.1  2003/01/29 10:50:39  deniger
 * version initiale
 *
 * Revision 1.4  2002/05/22 16:44:39  deniger
 * correction erreurs de jedit
 *
 * Revision 1.3  2001/07/25 17:39:05  desnoix
 * *** empty log message ***
 *
 * Revision 1.2  2001/06/21 18:50:04  desnoix
 * *** empty log message ***
 *
 * Revision 1.1  2001/06/13 19:48:18  desnoix
 * *** empty log message ***
 *
 * Revision 1.11  1999/11/07 06:51:43  sp
 * Check box menu items supported
 *
 * Revision 1.10  1999/10/24 02:06:41  sp
 * Miscallaneous pre1 stuff
 *
 * Revision 1.9  1999/09/30 12:21:05  sp
 * No net access for a month... so here's one big jEdit 2.1pre1
 *
 * Revision 1.8  1999/07/05 04:38:39  sp
 * Massive batch of changes... bug fixes, also new text component is in place.
 * Have fun
 *
 * Revision 1.7  1999/06/22 06:14:39  sp
 * RMI updates, text area updates, flag to disable geometry saving
 *
 * Revision 1.6  1999/06/07 06:36:32  sp
 * Syntax `styling' (bold/italic tokens) added,
 * plugin options dialog for plugin option panes
 *
 * Revision 1.5  1999/06/05 00:22:58  sp
 * LGPL'd syntax package
 *
 * Revision 1.4  1999/05/02 00:07:21  sp
 * Syntax system tweaks, console bugfix for Swing 1.1.1
 *
 * Revision 1.3  1999/04/19 05:38:20  sp
 * Syntax API changes
 *
 * Revision 1.2  1999/03/29 06:30:25  sp
 * Documentation updates, fixed bug in DefaultJEditSyntaxDocument, fixed bug in
 * goto-line
 *
 * Revision 1.1  1999/03/22 04:35:48  sp
 * Syntax colorizing updates
 *
 * Revision 1.1  1999/03/13 09:11:46  sp
 * Syntax code updates, code cleanups
 *
 */
