
package com.memoire.jedit;
import javax.swing.text.Segment;

/**
 * Python token marker.
 *
 * @author Jonathan Revusky
 * @version $Id: JEditPythonTokenMarker.java,v 1.2 2006-09-19 14:35:03 deniger Exp $
 */
public class JEditPythonTokenMarker extends JEditTokenMarker
{
  private static final byte TRIPLEQUOTE1 = JEditToken.INTERNAL_FIRST;
  private static final byte TRIPLEQUOTE2 = JEditToken.INTERNAL_LAST;

  public JEditPythonTokenMarker()
  {
    this.keywords = getKeywords();
  }

  public byte markTokensImpl(byte token, Segment line, int lineIndex)
  {
    char[] array = line.array;
    int offset = line.offset;
    lastOffset = offset;
    lastKeyword = offset;
    int length = line.count + offset;
    boolean backslash = false;

loop:		for(int i = offset; i < length; i++)
    {
      int i1 = (i+1);

      char c = array[i];
      if(c == '\\')
      {
        backslash = !backslash;
        continue;
      }

      switch(token)
      {
      case JEditToken.NULL:
        switch(c)
        {
        case '#':
          if(backslash)
            backslash = false;
          else
          {
            doKeyword(line,i,c);
            addToken(i - lastOffset,token);
            addToken(length - i,JEditToken.COMMENT1);
            lastOffset = lastKeyword = length;
            break loop;
          }
          break;
        case '"':
          doKeyword(line,i,c);
          if(backslash)
            backslash = false;
          else
          {
            addToken(i - lastOffset,token);
            if(JEditSyntaxUtilities.regionMatches(false,
              line,i1,"\"\""))
            {
              token = TRIPLEQUOTE1;
            }
            else
            {
              token = JEditToken.LITERAL1;
            }
            lastOffset = lastKeyword = i;
          }
          break;
        case '\'':
          doKeyword(line,i,c);
          if(backslash)
            backslash = false;
          else
          {
            addToken(i - lastOffset,token);
            if(JEditSyntaxUtilities.regionMatches(false,
              line,i1,"''"))
            {
              token = TRIPLEQUOTE2;
            }
            else
            {
              token = JEditToken.LITERAL2;
            }
            lastOffset = lastKeyword = i;
          }
          break;
        default:
          backslash = false;
          if(!Character.isLetterOrDigit(c)
            && c != '_')
            doKeyword(line,i,c);
          break;
        }
        break;
      case JEditToken.LITERAL1:
        if(backslash)
          backslash = false;
        else if(c == '"')
        {
          addToken(i1 - lastOffset,token);
          token = JEditToken.NULL;
          lastOffset = lastKeyword = i1;
        }
        break;
      case JEditToken.LITERAL2:
        if(backslash)
          backslash = false;
        else if(c == '\'')
        {
          addToken(i1 - lastOffset,JEditToken.LITERAL1);
          token = JEditToken.NULL;
          lastOffset = lastKeyword = i1;
        }
        break;
      case TRIPLEQUOTE1:
        if(backslash)
          backslash = false;
        else if(JEditSyntaxUtilities.regionMatches(false,
          line,i,"\"\"\""))
        {
          addToken((i+=4) - lastOffset,
            JEditToken.LITERAL1);
          token = JEditToken.NULL;
          lastOffset = lastKeyword = i;
        }
        break;
      case TRIPLEQUOTE2:
        if(backslash)
          backslash = false;
        else if(JEditSyntaxUtilities.regionMatches(false,
          line,i,"'''"))
        {
          addToken((i+=4) - lastOffset,
            JEditToken.LITERAL1);
          token = JEditToken.NULL;
          lastOffset = lastKeyword = i;
        }
        break;
      default:
        throw new InternalError("Invalid state: "
          + token);
      }
    }

    switch(token)
    {
      case TRIPLEQUOTE1:
      case TRIPLEQUOTE2:
        addToken(length - lastOffset,JEditToken.LITERAL1);
        break;
      case JEditToken.NULL:
        doKeyword(line,length,'\0');
      default:
        addToken(length - lastOffset,token);
        break;
    }

    return token;
  }

  public static JEditKeywordMap getKeywords()
  {
    if (pyKeywords == null)
    {
      pyKeywords = new JEditKeywordMap(false);
      pyKeywords.add("and",JEditToken.KEYWORD3);
      pyKeywords.add("not",JEditToken.KEYWORD3);
      pyKeywords.add("or",JEditToken.KEYWORD3);
      pyKeywords.add("if",JEditToken.KEYWORD1);
      pyKeywords.add("for",JEditToken.KEYWORD1);
      pyKeywords.add("assert",JEditToken.KEYWORD1);
      pyKeywords.add("break",JEditToken.KEYWORD1);
      pyKeywords.add("continue",JEditToken.KEYWORD1);
      pyKeywords.add("elif",JEditToken.KEYWORD1);
      pyKeywords.add("else",JEditToken.KEYWORD1);
      pyKeywords.add("except",JEditToken.KEYWORD1);
      pyKeywords.add("exec",JEditToken.KEYWORD1);
      pyKeywords.add("finally",JEditToken.KEYWORD1);
      pyKeywords.add("raise",JEditToken.KEYWORD1);
      pyKeywords.add("return",JEditToken.KEYWORD1);
      pyKeywords.add("try",JEditToken.KEYWORD1);
      pyKeywords.add("while",JEditToken.KEYWORD1);
      pyKeywords.add("def",JEditToken.KEYWORD2);
      pyKeywords.add("class",JEditToken.KEYWORD2);
      pyKeywords.add("del",JEditToken.KEYWORD2);
      pyKeywords.add("from",JEditToken.KEYWORD2);
      pyKeywords.add("global",JEditToken.KEYWORD2);
      pyKeywords.add("import",JEditToken.KEYWORD2);
      pyKeywords.add("in",JEditToken.KEYWORD2);
      pyKeywords.add("is",JEditToken.KEYWORD2);
      pyKeywords.add("lambda",JEditToken.KEYWORD2);
      pyKeywords.add("pass",JEditToken.KEYWORD2);
      pyKeywords.add("print",JEditToken.KEYWORD2);
    }
    return pyKeywords;
  }

  // private members
  private static JEditKeywordMap pyKeywords;

  private JEditKeywordMap keywords;
  private int lastOffset;
  private int lastKeyword;

  private boolean doKeyword(Segment line, int i, char c)
  {
    int i1 = i+1;

    int len = i - lastKeyword;
    byte id = keywords.lookup(line,lastKeyword,len);
    if(id != JEditToken.NULL)
    {
      if(lastKeyword != lastOffset)
        addToken(lastKeyword - lastOffset,JEditToken.NULL);
      addToken(len,id);
      lastOffset = i;
    }
    lastKeyword = i1;
    return false;
  }
}

/*
 * ChangeLog:
 * $Log: not supported by cvs2svn $
 * Revision 1.1  2003/01/29 10:50:39  deniger
 * version initiale
 *
 * Revision 1.4  2002/05/22 16:44:39  deniger
 * correction erreurs de jedit
 *
 * Revision 1.3  2001/07/25 17:39:05  desnoix
 * *** empty log message ***
 *
 * Revision 1.2  2001/06/21 18:50:04  desnoix
 * *** empty log message ***
 *
 * Revision 1.1  2001/06/13 19:48:18  desnoix
 * *** empty log message ***
 *
 * Revision 1.2  1999/10/31 07:15:34  sp
 * New logging API, splash screen updates, bug fixes
 *
 * Revision 1.1  1999/09/30 12:21:05  sp
 * No net access for a month... so here's one big jEdit 2.1pre1
 *
 *
 */
