
package com.memoire.jedit;
import javax.swing.text.Segment;

/**
 * HTML token marker.
 *
 * @author Slava Pestov
 * @version $Id: JEditHTMLTokenMarker.java,v 1.2 2006-09-19 14:35:03 deniger Exp $
 */
public class JEditHTMLTokenMarker extends JEditTokenMarker
{
  public static final byte JAVASCRIPT = JEditToken.INTERNAL_FIRST;

  public JEditHTMLTokenMarker()
  {
    this(true);
  }

  public JEditHTMLTokenMarker(boolean js)
  {
    this.js = js;
    keywords = JEditJavaScriptTokenMarker.getKeywords();
  }

  public byte markTokensImpl(byte token, Segment line, int lineIndex)
  {
    char[] array = line.array;
    int offset = line.offset;
    lastOffset = offset;
    lastKeyword = offset;
    int length = line.count + offset;
    boolean backslash = false;

loop:		for(int i = offset; i < length; i++)
    {
      int i1 = (i+1);

      char c = array[i];
      if(c == '\\')
      {
        backslash = !backslash;
        continue;
      }

      switch(token)
      {
      case JEditToken.NULL: // HTML text
        backslash = false;
        switch(c)
        {
        case '<':
          addToken(i - lastOffset,token);
          lastOffset = lastKeyword = i;
          if(JEditSyntaxUtilities.regionMatches(false,
            line,i1,"!--"))
          {
            i += 3;
            token = JEditToken.COMMENT1;
          }
          else if(js && JEditSyntaxUtilities.regionMatches(
            true,line,i1,"script>"))
          {
            addToken(8,JEditToken.KEYWORD1);
            lastOffset = lastKeyword = (i += 8);
            token = JAVASCRIPT;
          }
          else
          {
            token = JEditToken.KEYWORD1;
          }
          break;
        case '&':
          addToken(i - lastOffset,token);
          lastOffset = lastKeyword = i;
          token = JEditToken.KEYWORD2;
          break;
        }
        break;
      case JEditToken.KEYWORD1: // Inside a tag
        backslash = false;
        if(c == '>')
        {
          addToken(i1 - lastOffset,token);
          lastOffset = lastKeyword = i1;
          token = JEditToken.NULL;
        }
        break;
      case JEditToken.KEYWORD2: // Inside an entity
        backslash = false;
        if(c == ';')
        {
          addToken(i1 - lastOffset,token);
          lastOffset = lastKeyword = i1;
          token = JEditToken.NULL;
          break;
        }
        break;
      case JEditToken.COMMENT1: // Inside a comment
        backslash = false;
        if(JEditSyntaxUtilities.regionMatches(false,line,i,"-->"))
        {
          addToken((i + 3) - lastOffset,token);
          lastOffset = lastKeyword = i + 3;
          token = JEditToken.NULL;
        }
        break;
      case JAVASCRIPT: // Inside a JavaScript
        switch(c)
        {
        case '<':
          backslash = false;
          doKeyword(line,i,c);
          if(JEditSyntaxUtilities.regionMatches(true,
            line,i1,"/script>"))
          {
            addToken(i - lastOffset,
              JEditToken.NULL);
            addToken(9,JEditToken.KEYWORD1);
            lastOffset = lastKeyword = (i += 9);
            token = JEditToken.NULL;
          }
          break;
        case '"':
          if(backslash)
            backslash = false;
          else
          {
            doKeyword(line,i,c);
            addToken(i - lastOffset,JEditToken.NULL);
            lastOffset = lastKeyword = i;
            token = JEditToken.LITERAL1;
          }
          break;
        case '\'':
          if(backslash)
            backslash = false;
          else
          {
            doKeyword(line,i,c);
            addToken(i - lastOffset,JEditToken.NULL);
            lastOffset = lastKeyword = i;
            token = JEditToken.LITERAL2;
          }
          break;
        case '/':
          backslash = false;
          doKeyword(line,i,c);
          if(length - i > 1)
          {
            addToken(i - lastOffset,JEditToken.NULL);
            lastOffset = lastKeyword = i;
            if(array[i1] == '/')
            {
              addToken(length - i,JEditToken.COMMENT2);
              lastOffset = lastKeyword = length;
              break loop;
            }
            else if(array[i1] == '*')
            {
              token = JEditToken.COMMENT2;
            }
          }
          break;
        default:					backslash = false;
          if(!Character.isLetterOrDigit(c)
            && c != '_')
            doKeyword(line,i,c);
          break;
        }
        break;
      case JEditToken.LITERAL1: // JavaScript "..."
        if(backslash)
          backslash = false;
        else if(c == '"')
        {
          addToken(i1 - lastOffset,JEditToken.LITERAL1);
          lastOffset = lastKeyword = i1;
          token = JAVASCRIPT;
        }
        break;
      case JEditToken.LITERAL2: // JavaScript '...'
        if(backslash)
          backslash = false;
        else if(c == '\'')
        {
          addToken(i1 - lastOffset,JEditToken.LITERAL1);
          lastOffset = lastKeyword = i1;
          token = JAVASCRIPT;
        }
        break;
      case JEditToken.COMMENT2: // Inside a JavaScript comment
        backslash = false;
        if(c == '*' && length - i > 1 && array[i1] == '/')
        {
          addToken((i+=2) - lastOffset,JEditToken.COMMENT2);
          lastOffset = lastKeyword = i;
          token = JAVASCRIPT;
        }
        break;
      default:
        throw new InternalError("Invalid state: "
          + token);
      }
    }

    switch(token)
    {
    case JEditToken.LITERAL1:
    case JEditToken.LITERAL2:
      addToken(length - lastOffset,JEditToken.INVALID);
      token = JAVASCRIPT;
      break;
    case JEditToken.KEYWORD2:
      addToken(length - lastOffset,JEditToken.INVALID);
      token = JEditToken.NULL;
      break;
    case JAVASCRIPT:
      doKeyword(line,length,'\0');
      addToken(length - lastOffset,JEditToken.NULL);
      break;
    default:
      addToken(length - lastOffset,token);
      break;
    }

    return token;
  }

  // private members
  private JEditKeywordMap keywords;
  private boolean js;
  private int lastOffset;
  private int lastKeyword;

  private boolean doKeyword(Segment line, int i, char c)
  {
    int i1 = i+1;

    int len = i - lastKeyword;
    byte id = keywords.lookup(line,lastKeyword,len);
    if(id != JEditToken.NULL)
    {
      if(lastKeyword != lastOffset)
        addToken(lastKeyword - lastOffset,JEditToken.NULL);
      addToken(len,id);
      lastOffset = i;
    }
    lastKeyword = i1;
    return false;
  }
}

/*
 * ChangeLog:
 * $Log: not supported by cvs2svn $
 * Revision 1.1  2003/01/29 10:50:38  deniger
 * version initiale
 *
 * Revision 1.4  2002/05/22 16:44:39  deniger
 * correction erreurs de jedit
 *
 * Revision 1.3  2001/07/25 17:39:05  desnoix
 * *** empty log message ***
 *
 * Revision 1.2  2001/06/21 18:50:04  desnoix
 * *** empty log message ***
 *
 * Revision 1.1  2001/06/13 19:48:18  desnoix
 * *** empty log message ***
 *
 * Revision 1.33  1999/11/19 05:11:36  sp
 * Recent file list bug fix, HTML comment bug fix
 *
 * Revision 1.32  1999/11/12 09:06:01  sp
 * HTML bug fix
 *
 * Revision 1.31  1999/10/23 03:48:22  sp
 * Mode system overhaul, close all dialog box, misc other stuff
 *
 * Revision 1.30  1999/09/30 12:21:05  sp
 * No net access for a month... so here's one big jEdit 2.1pre1
 *
 * Revision 1.29  1999/07/16 23:45:49  sp
 * 1.7pre6 BugFree version
 *
 * Revision 1.28  1999/06/05 00:22:58  sp
 * LGPL'd syntax package
 *
 * Revision 1.27  1999/06/03 08:24:13  sp
 * Fixing broken CVS
 *
 * Revision 1.28  1999/05/31 08:11:10  sp
 * Syntax coloring updates, expand abbrev bug fix
 *
 * Revision 1.27  1999/05/31 04:38:51  sp
 * Syntax optimizations, HyperSearch for Selection added (Mike Dillon)
 *
 * Revision 1.26  1999/05/14 04:56:15  sp
 * Docs updated, default: fix in C/C++/Java mode, full path in title bar toggle
 *
 * Revision 1.25  1999/05/11 09:05:10  sp
 * New version1.6.html file, some other stuff perhaps
 *
 * Revision 1.24  1999/05/03 04:28:01  sp
 * Syntax colorizing bug fixing, console bug fix for Swing 1.1.1
 *
 * Revision 1.23  1999/04/22 06:03:26  sp
 * Syntax colorizing change
 *
 * Revision 1.22  1999/04/19 05:38:20  sp
 * Syntax API changes
 *
 * Revision 1.21  1999/03/13 08:50:39  sp
 * Syntax colorizing updates and cleanups, general code reorganizations
 *
 * Revision 1.20  1999/03/12 23:51:00  sp
 * Console updates, uncomment removed cos it's too buggy, cvs log tags added
 *
 */
