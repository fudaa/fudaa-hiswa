
package com.memoire.jedit;

/**
 * C++ token marker.
 *
 * @author Slava Pestov
 * @version $Id: JEditCCTokenMarker.java,v 1.2 2006-09-19 14:35:03 deniger Exp $
 */
public class JEditCCTokenMarker extends JEditCTokenMarker
{
	public JEditCCTokenMarker()
	{
		super(true,getKeywords());
	}

	public static JEditKeywordMap getKeywords()
	{
		if(ccKeywords == null)
		{
			ccKeywords = new JEditKeywordMap(false);

			ccKeywords.add("and", JEditToken.KEYWORD3);
			ccKeywords.add("and_eq", JEditToken.KEYWORD3);
			ccKeywords.add("asm", JEditToken.KEYWORD2);         //
			ccKeywords.add("auto", JEditToken.KEYWORD1);        //
			ccKeywords.add("bitand", JEditToken.KEYWORD3);
			ccKeywords.add("bitor", JEditToken.KEYWORD3);
			ccKeywords.add("bool",JEditToken.KEYWORD3);
			ccKeywords.add("break", JEditToken.KEYWORD1);	   //
			ccKeywords.add("case", JEditToken.KEYWORD1);		   //
			ccKeywords.add("catch", JEditToken.KEYWORD1);
			ccKeywords.add("char", JEditToken.KEYWORD3);		   //
			ccKeywords.add("class", JEditToken.KEYWORD3);
			ccKeywords.add("compl", JEditToken.KEYWORD3);
			ccKeywords.add("const", JEditToken.KEYWORD1);	   //
			ccKeywords.add("const_cast", JEditToken.KEYWORD3);
			ccKeywords.add("continue", JEditToken.KEYWORD1);	   //
			ccKeywords.add("default", JEditToken.KEYWORD1);	   //
			ccKeywords.add("delete", JEditToken.KEYWORD1);
			ccKeywords.add("do",JEditToken.KEYWORD1);           //
			ccKeywords.add("double" ,JEditToken.KEYWORD3);	   //
			ccKeywords.add("dynamic_cast", JEditToken.KEYWORD3);
			ccKeywords.add("else", 	JEditToken.KEYWORD1);	   //
			ccKeywords.add("enum",  JEditToken.KEYWORD3);	   //
			ccKeywords.add("explicit", JEditToken.KEYWORD1);			
			ccKeywords.add("export", JEditToken.KEYWORD2);
			ccKeywords.add("extern", JEditToken.KEYWORD2);	   //
			ccKeywords.add("false", JEditToken.LITERAL2);
			ccKeywords.add("float", JEditToken.KEYWORD3);	   //
			ccKeywords.add("for", JEditToken.KEYWORD1);		   //
			ccKeywords.add("friend", JEditToken.KEYWORD1);			
			ccKeywords.add("goto", JEditToken.KEYWORD1);        //
			ccKeywords.add("if", JEditToken.KEYWORD1);		   //
			ccKeywords.add("inline", JEditToken.KEYWORD1);
			ccKeywords.add("int", JEditToken.KEYWORD3);		   //
			ccKeywords.add("long", JEditToken.KEYWORD3);		   //
			ccKeywords.add("mutable", JEditToken.KEYWORD3);
			ccKeywords.add("namespace", JEditToken.KEYWORD2);
			ccKeywords.add("new", JEditToken.KEYWORD1);
			ccKeywords.add("not", JEditToken.KEYWORD3);
			ccKeywords.add("not_eq", JEditToken.KEYWORD3);
			ccKeywords.add("operator", JEditToken.KEYWORD3);
			ccKeywords.add("or", JEditToken.KEYWORD3);
			ccKeywords.add("or_eq", JEditToken.KEYWORD3);
			ccKeywords.add("private", JEditToken.KEYWORD1);
			ccKeywords.add("protected", JEditToken.KEYWORD1);
			ccKeywords.add("public", JEditToken.KEYWORD1);
			ccKeywords.add("register", JEditToken.KEYWORD1);
			ccKeywords.add("reinterpret_cast", JEditToken.KEYWORD3);
			ccKeywords.add("return", JEditToken.KEYWORD1);      //
			ccKeywords.add("short", JEditToken.KEYWORD3);	   //
			ccKeywords.add("signed", JEditToken.KEYWORD3);	   //
			ccKeywords.add("sizeof", JEditToken.KEYWORD1);	   //
			ccKeywords.add("static", JEditToken.KEYWORD1);	   //
			ccKeywords.add("static_cast", JEditToken.KEYWORD3);
			ccKeywords.add("struct", JEditToken.KEYWORD3);	   //
			ccKeywords.add("switch", JEditToken.KEYWORD1);	   //
			ccKeywords.add("template", JEditToken.KEYWORD3);
			ccKeywords.add("this", JEditToken.LITERAL2);
			ccKeywords.add("throw", JEditToken.KEYWORD1);
			ccKeywords.add("true", JEditToken.LITERAL2);
			ccKeywords.add("try", JEditToken.KEYWORD1);
			ccKeywords.add("typedef", JEditToken.KEYWORD3);	   //
			ccKeywords.add("typeid", JEditToken.KEYWORD3);
			ccKeywords.add("typename", JEditToken.KEYWORD3);
			ccKeywords.add("union", JEditToken.KEYWORD3);	   //
			ccKeywords.add("unsigned", JEditToken.KEYWORD3);	   //
			ccKeywords.add("using", JEditToken.KEYWORD2);
			ccKeywords.add("virtual", JEditToken.KEYWORD1);
			ccKeywords.add("void", JEditToken.KEYWORD1);		   //
			ccKeywords.add("volatile", JEditToken.KEYWORD1);	   //
			ccKeywords.add("wchar_t", JEditToken.KEYWORD3);
			ccKeywords.add("while", JEditToken.KEYWORD1);	   //
			ccKeywords.add("xor", JEditToken.KEYWORD3);
			ccKeywords.add("xor_eq", JEditToken.KEYWORD3);            

			// non ANSI keywords
			ccKeywords.add("NULL", JEditToken.LITERAL2);
		}
		return ccKeywords;
	}

	// private members
	private static JEditKeywordMap ccKeywords;
}

/*
 * ChangeLog:
 * $Log: not supported by cvs2svn $
 * Revision 1.1  2003/01/29 10:50:39  deniger
 * version initiale
 *
 * Revision 1.3  2001/07/25 17:39:05  desnoix
 * *** empty log message ***
 *
 * Revision 1.2  2001/06/21 18:50:04  desnoix
 * *** empty log message ***
 *
 * Revision 1.1  2001/06/13 19:48:18  desnoix
 * *** empty log message ***
 *
 * Revision 1.5  1999/06/05 00:22:58  sp
 * LGPL'd syntax package
 *
 * Revision 1.4  1999/05/14 04:56:15  sp
 * Docs updated, default: fix in C/C++/Java mode, full path in title bar toggle
 *
 * Revision 1.3  1999/04/22 06:03:26  sp
 * Syntax colorizing change
 *
 * Revision 1.2  1999/04/21 05:00:46  sp
 * New splash screen!!!!!!!!!!! Also, Juha has sent in a new keyword set for C++
 *
 * Revision 1.1  1999/03/13 09:11:46  sp
 * Syntax code updates, code cleanups
 *
 */
