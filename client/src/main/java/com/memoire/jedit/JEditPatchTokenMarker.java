
package com.memoire.jedit;
import javax.swing.text.Segment;

/**
 * Patch/diff token marker.
 *
 * @author Slava Pestov
 * @version $Id: JEditPatchTokenMarker.java,v 1.2 2006-09-19 14:35:03 deniger Exp $
 */
public class JEditPatchTokenMarker extends JEditTokenMarker
{
  public byte markTokensImpl(byte token, Segment line, int lineIndex)
  {
    if(line.count == 0)
      return JEditToken.NULL;
    switch(line.array[line.offset])
    {
    case '+': case '>':
      addToken(line.count,JEditToken.KEYWORD1);
      break;
    case '-': case '<':
      addToken(line.count,JEditToken.KEYWORD2);
      break;
    case '@': case '*':
      addToken(line.count,JEditToken.KEYWORD3);
      break;
          default:
      addToken(line.count,JEditToken.NULL);
      break;
    }
    return JEditToken.NULL;
  }

  public boolean supportsMultilineTokens()
  {
    return false;
  }
}

/*
 * ChangeLog:
 * $Log: not supported by cvs2svn $
 * Revision 1.1  2003/01/29 10:50:39  deniger
 * version initiale
 *
 * Revision 1.4  2002/05/22 16:44:39  deniger
 * correction erreurs de jedit
 *
 * Revision 1.3  2001/07/25 17:39:05  desnoix
 * *** empty log message ***
 *
 * Revision 1.2  2001/06/21 18:50:04  desnoix
 * *** empty log message ***
 *
 * Revision 1.1  2001/06/13 19:48:18  desnoix
 * *** empty log message ***
 *
 * Revision 1.6  1999/07/05 04:38:39  sp
 * Massive batch of changes... bug fixes, also new text component is in place.
 * Have fun
 *
 * Revision 1.5  1999/06/05 00:22:58  sp
 * LGPL'd syntax package
 *
 * Revision 1.4  1999/05/22 08:33:53  sp
 * FAQ updates, mode selection tweak, patch mode update, javadoc updates, JDK 1.1.8 fix
 *
 * Revision 1.3  1999/04/19 05:38:20  sp
 * Syntax API changes
 *
 * Revision 1.2  1999/03/12 23:51:00  sp
 * Console updates, uncomment removed cos it's too buggy, cvs log tags added
 *
 */
